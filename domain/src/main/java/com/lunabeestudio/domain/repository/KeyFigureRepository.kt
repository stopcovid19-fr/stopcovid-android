/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.domain.repository

import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.model.TacResult
import kotlinx.coroutines.flow.Flow

interface KeyFigureRepository {
    val keyFiguresWithDepartmentFlow: Flow<TacResult<List<KeyFigure>>>
    fun keyFigureByLabelWithDepartmentFlow(label: String): Flow<TacResult<KeyFigure?>>
    fun allKeyFiguresWithFavoritesFlow(): Flow<List<KeyFigure>>
    suspend fun allKeyFigures(): TacResult<List<KeyFigure>>
    suspend fun keyFigureByLabel(label: String): TacResult<KeyFigure>
    suspend fun keyFigureWithDepartmentByLabel(label: String): TacResult<KeyFigure>
    suspend fun onAppForeground()
    suspend fun initialize()
    suspend fun toggleFavorite(figure: KeyFigure)
    suspend fun saveFavorite(figure: KeyFigure)
    suspend fun restoreDefaultFavorites()
}
