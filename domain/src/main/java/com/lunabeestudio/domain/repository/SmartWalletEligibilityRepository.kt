/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.domain.repository

import com.lunabeestudio.domain.model.smartwallet.SmartWalletEligibilityPivot

interface SmartWalletEligibilityRepository {
    val smartWalletEligibilityPivot: List<SmartWalletEligibilityPivot>
    suspend fun initialize()
    suspend fun onAppForeground()
}
