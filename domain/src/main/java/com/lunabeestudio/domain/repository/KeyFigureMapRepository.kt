/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.domain.repository

import com.lunabeestudio.domain.model.KeyFigureMap
import com.lunabeestudio.domain.model.TacResult
import kotlinx.coroutines.flow.Flow

interface KeyFigureMapRepository {
    suspend fun initialize()
    suspend fun reload()
    suspend fun fetchNewData()
    fun availableKeyFiguresFlow(): Flow<List<String>>
    suspend fun keyFigureMapByLabel(label: String): TacResult<KeyFigureMap>
}
