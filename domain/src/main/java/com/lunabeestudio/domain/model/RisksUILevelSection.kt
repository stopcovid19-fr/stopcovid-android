/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.domain.model

data class RisksUILevelSection(
    val section: String,
    val description: String,
    val link: RisksUILevelSectionLink?,
)

data class RisksUILevelSectionLink(
    val label: String,
    val action: String,
    val type: LinkType,
)

@Suppress("EnumEntryName")
enum class LinkType {
    web,
    ctrl,
}
