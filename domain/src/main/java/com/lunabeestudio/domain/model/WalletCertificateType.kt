/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.domain.model

enum class WalletCertificateType(val code: String, val format: Format) {
    SANITARY("B2", Format.WALLET_2D),
    VACCINATION("L1", Format.WALLET_2D),
    SANITARY_EUROPE("test", Format.WALLET_DCC),
    VACCINATION_EUROPE("vaccine", Format.WALLET_DCC),
    RECOVERY_EUROPE("recovery", Format.WALLET_DCC),
    EXEMPTION("exemption", Format.WALLET_DCC),
    MULTI_PASS("multiPass", Format.WALLET_DCC),
    ;

    enum class Format(val values: List<String>) {
        WALLET_2D(listOf("wallet2d", "wallet")), WALLET_DCC(listOf("walletdcc"));

        companion object {
            fun fromValue(value: String): Format? = values().firstOrNull { it.values.contains(value) }
        }
    }
}
