/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/5/17 - for the TOUS-ANTI-COVID project
 */

object EnvConfig {
    const val ENV_VERSION_CODE: String = "ANDROID_VERSION_CODE"
    const val ENV_VERSION_NAME: String = "ANDROID_VERSION_NAME"
}
