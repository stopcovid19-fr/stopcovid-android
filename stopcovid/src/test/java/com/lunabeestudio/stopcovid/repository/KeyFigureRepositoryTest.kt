/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2021/7/10 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.repository

import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.repository.KeyFigureRepository
import com.lunabeestudio.repository.keyfigure.KeyFigureLocalDataSource
import com.lunabeestudio.repository.keyfigure.KeyFigureRemoteDataSource
import com.lunabeestudio.repository.keyfigure.KeyFigureRepositoryImpl
import com.lunabeestudio.stopcovid.utils.KeyFiguresHelper
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.slot
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class KeyFigureRepositoryTest {
    private lateinit var repository: KeyFigureRepository

    private val localKeyFigureDatasource = mockk<KeyFigureLocalDataSource>(relaxed = true)
    private val remoteKeyFigureDatasource = mockk<KeyFigureRemoteDataSource>(relaxed = true)

    @Before
    fun init() {
        repository = KeyFigureRepositoryImpl(
            localKeyFigureDatasource,
            remoteKeyFigureDatasource,
        )
    }

    @Test
    fun only_favorites() {
        val list = listOf(
            KeyFiguresHelper.defaultKeyFigureForFav(0, true, 0f),
            KeyFiguresHelper.defaultKeyFigureForFav(1, true, 2f),
            KeyFiguresHelper.defaultKeyFigureForFav(2, true, 1f),
        )
        coEvery { localKeyFigureDatasource.allKeyFigures() } returns list

        val labelKeySlot = slot<String>()
        val isFavoriteSlot = slot<Boolean>()
        val favoriteOrderSlot = slot<Float>()
        coEvery {
            localKeyFigureDatasource.setFavorite(
                capture(labelKeySlot),
                capture(isFavoriteSlot),
                capture(favoriteOrderSlot),
            )
        } returns mockk()

        runBlocking {
            repository.toggleFavorite(KeyFiguresHelper.defaultKeyFigureForFav(3, false, 0f))
        }

        coVerify {
            localKeyFigureDatasource.setFavorite(any(), any(), any())
        }
        assertEquals("labelKey", labelKeySlot.captured)
        assertTrue(isFavoriteSlot.captured)
        assertEquals(3f, favoriteOrderSlot.captured)
    }

    @Test
    fun only_non_favorites() {
        val list = listOf(
            KeyFiguresHelper.defaultKeyFigureForFav(0, false, 0f),
            KeyFiguresHelper.defaultKeyFigureForFav(1, false, 2f),
            KeyFiguresHelper.defaultKeyFigureForFav(2, false, 1f),
        )
        coEvery { localKeyFigureDatasource.allKeyFigures() } returns list

        val labelKeySlot = slot<String>()
        val isFavoriteSlot = slot<Boolean>()
        val favoriteOrderSlot = slot<Float>()
        coEvery {
            localKeyFigureDatasource.setFavorite(
                capture(labelKeySlot),
                capture(isFavoriteSlot),
                capture(favoriteOrderSlot),
            )
        } returns mockk()

        runBlocking {
            repository.toggleFavorite(KeyFiguresHelper.defaultKeyFigureForFav(3, false, 0f))
        }

        coVerify {
            localKeyFigureDatasource.setFavorite(any(), any(), any())
        }
        assertEquals("labelKey", labelKeySlot.captured)
        assertTrue(isFavoriteSlot.captured)
        assertEquals(0f, favoriteOrderSlot.captured)
    }

    @Test
    fun mixed() {
        val list = listOf(
            KeyFiguresHelper.defaultKeyFigureForFav(0, true, 0f),
            KeyFiguresHelper.defaultKeyFigureForFav(1, true, 2f),
            KeyFiguresHelper.defaultKeyFigureForFav(2, false, 0.5f),
            KeyFiguresHelper.defaultKeyFigureForFav(3, true, 1f),
        )
        coEvery { localKeyFigureDatasource.allKeyFigures() } returns list

        val labelKeySlot = slot<String>()
        val isFavoriteSlot = slot<Boolean>()
        val favoriteOrderSlot = slot<Float>()
        coEvery {
            localKeyFigureDatasource.setFavorite(
                capture(labelKeySlot),
                capture(isFavoriteSlot),
                capture(favoriteOrderSlot),
            )
        } returns mockk()

        runBlocking {
            repository.toggleFavorite(KeyFiguresHelper.defaultKeyFigureForFav(3, false, 0f))
        }

        coVerify {
            localKeyFigureDatasource.setFavorite(any(), any(), any())
        }
        assertEquals("labelKey", labelKeySlot.captured)
        assertTrue(isFavoriteSlot.captured)
        assertEquals(3f, favoriteOrderSlot.captured)
    }

    @Test
    fun empty() {
        val list = emptyList<KeyFigure>()
        coEvery { localKeyFigureDatasource.allKeyFigures() } returns list

        val labelKeySlot = slot<String>()
        val isFavoriteSlot = slot<Boolean>()
        val favoriteOrderSlot = slot<Float>()
        coEvery {
            localKeyFigureDatasource.setFavorite(
                capture(labelKeySlot),
                capture(isFavoriteSlot),
                capture(favoriteOrderSlot),
            )
        } returns mockk()

        runBlocking {
            repository.toggleFavorite(KeyFiguresHelper.defaultKeyFigureForFav(3, false, 0f))
        }

        coVerify {
            localKeyFigureDatasource.setFavorite(any(), any(), any())
        }
        assertEquals("labelKey", labelKeySlot.captured)
        assertTrue(isFavoriteSlot.captured)
        assertEquals(0f, favoriteOrderSlot.captured)
    }
}
