/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/2/11 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.usecase

import com.lunabeestudio.domain.repository.EUTagsRepository
import com.lunabeestudio.stopcovid.extension.isAntigen
import com.lunabeestudio.stopcovid.extension.manufacturer
import com.lunabeestudio.stopcovid.extension.testType
import com.lunabeestudio.stopcovid.model.EuropeanCertificate
import dgca.verifier.app.decoder.model.GreenCertificate
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class IsCertificateEUValidUseCaseTest {

    private lateinit var isCertificateEUValidUseCase: IsCertificateEUValidUseCase

    private val euTagsManager = mockk<EUTagsRepository>(relaxed = true)

    @Before
    fun init() {
        MockKAnnotations.init(this)

        mockkStatic(GreenCertificate::testType)

        isCertificateEUValidUseCase = IsCertificateEUValidUseCase(
            euTagsManager,
        )
    }

    @After
    internal fun tearDown() {
        clearAllMocks()
    }

    @Test
    fun `not antigen with manufacturer not in eu list`() {
        val manufacturer = "manufacturer"
        every { euTagsManager.isEUTag(manufacturer) } returns false

        val certificate = mockk<EuropeanCertificate>(relaxed = true).also {
            every { it.greenCertificate.isAntigen } returns false
            every { it.greenCertificate.manufacturer } returns manufacturer
        }

        assertTrue(isCertificateEUValidUseCase(certificate))
    }

    @Test
    fun `not antigen with manufacturer in eu list`() {
        val manufacturer = "manufacturer"
        every { euTagsManager.isEUTag(manufacturer) } returns true

        val certificate = mockk<EuropeanCertificate>(relaxed = true).also {
            every { it.greenCertificate.isAntigen } returns false
            every { it.greenCertificate.manufacturer } returns manufacturer
        }

        assertTrue(isCertificateEUValidUseCase(certificate))
    }

    @Test
    fun `antigen with manufacturer not in eu list`() {
        val manufacturer = "manufacturer"
        every { euTagsManager.isEUTag(manufacturer) } returns false

        val certificate = mockk<EuropeanCertificate>(relaxed = true).also {
            every { it.greenCertificate.isAntigen } returns true
            every { it.greenCertificate.manufacturer } returns manufacturer
        }

        assertFalse(isCertificateEUValidUseCase(certificate))
    }

    @Test
    fun `antigen with manufacturer in eu list`() {
        val manufacturer = "manufacturer"
        every { euTagsManager.isEUTag(manufacturer) } returns true

        val certificate = mockk<EuropeanCertificate>(relaxed = true).also {
            every { it.greenCertificate.isAntigen } returns true
            every { it.greenCertificate.manufacturer } returns manufacturer
        }

        assertTrue(isCertificateEUValidUseCase(certificate))
    }
}
