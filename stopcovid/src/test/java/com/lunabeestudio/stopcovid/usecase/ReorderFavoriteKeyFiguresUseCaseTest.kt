/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2021/7/10 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.usecase

import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.repository.KeyFigureRepository
import com.lunabeestudio.stopcovid.utils.KeyFiguresHelper
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.slot
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

@OptIn(ExperimentalCoroutinesApi::class)
class ReorderFavoriteKeyFiguresUseCaseTest {
    private lateinit var useCase: ReorderFavoriteKeyFiguresUseCase

    private val keyFiguresRepository = mockk<KeyFigureRepository>(relaxed = true)

    @Before
    fun init() {
        useCase = ReorderFavoriteKeyFiguresUseCase(
            keyFiguresRepository,
        )
    }

    @Test
    fun reorder_first() {
        val list = listOf(
            KeyFiguresHelper.defaultKeyFigureForFav(0, true, 0f),
            KeyFiguresHelper.defaultKeyFigureForFav(1, true, 1f),
        )
        val slot = slot<KeyFigure>()
        coEvery { keyFiguresRepository.saveFavorite(capture(slot)) } returns mockk()

        runTest {
            useCase.invoke(list, 1, 0)
        }

        coVerify {
            keyFiguresRepository.saveFavorite(any())
        }
        assertEquals(1, slot.captured.index)
        assertEquals(-0.5f, slot.captured.favoriteOrder)
    }

    @Test
    fun reorder_last() {
        val list = listOf(
            KeyFiguresHelper.defaultKeyFigureForFav(0, true, 0f),
            KeyFiguresHelper.defaultKeyFigureForFav(1, true, 1f),
        )
        val slot = slot<KeyFigure>()
        coEvery { keyFiguresRepository.saveFavorite(capture(slot)) } returns mockk()

        runTest {
            useCase.invoke(list, 0, 1)
        }

        coVerify {
            keyFiguresRepository.saveFavorite(any())
        }
        assertEquals(0, slot.captured.index)
        assertEquals(1.5f, slot.captured.favoriteOrder)
    }

    @Test
    fun reorder_middle() {
        val list = listOf(
            KeyFiguresHelper.defaultKeyFigureForFav(0, true, 0f),
            KeyFiguresHelper.defaultKeyFigureForFav(1, true, 1f),
            KeyFiguresHelper.defaultKeyFigureForFav(2, true, 2f),
        )
        val slot = slot<KeyFigure>()
        coEvery { keyFiguresRepository.saveFavorite(capture(slot)) } returns mockk()

        runTest {
            useCase.invoke(list, 0, 1)
        }

        coVerify {
            keyFiguresRepository.saveFavorite(any())
        }
        assertEquals(0, slot.captured.index)
        assertEquals(1.5f, slot.captured.favoriteOrder)

        runTest {
            useCase.invoke(list, 2, 1)
        }

        coVerify {
            keyFiguresRepository.saveFavorite(any())
        }
        assertEquals(2, slot.captured.index)
        assertEquals(0.5f, slot.captured.favoriteOrder)
    }
}
