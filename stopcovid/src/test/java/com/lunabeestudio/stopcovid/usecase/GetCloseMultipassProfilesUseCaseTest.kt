/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/1/24 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.usecase

import com.lunabeestudio.stopcovid.extension.rawBirthDate
import com.lunabeestudio.stopcovid.model.MultipassProfile
import io.mockk.every
import io.mockk.mockk
import org.junit.Before
import org.junit.Test
import kotlin.test.assertContentEquals

class GetCloseMultipassProfilesUseCaseTest {
    private lateinit var useCase: GetCloseMultipassProfilesUseCase

    @Before
    fun init() {
        useCase = GetCloseMultipassProfilesUseCase()
    }

    @Test
    fun main_case_test() {
        // Different name
        val profileA1 = mockk<MultipassProfile>(relaxed = true)
        every { profileA1.id } returns "A1"
        every { profileA1.certificates.firstOrNull()?.firstName } returns "a1"
        every { profileA1.certificates.firstOrNull()?.name } returns "a1"
        every { profileA1.certificates.firstOrNull()?.rawBirthDate() } returns "dob"

        val profileA2 = mockk<MultipassProfile>(relaxed = true)
        every { profileA2.id } returns "A2"
        every { profileA2.certificates.firstOrNull()?.firstName } returns "a1"
        every { profileA2.certificates.firstOrNull()?.name } returns "a2"
        every { profileA2.certificates.firstOrNull()?.rawBirthDate() } returns "dob"

        // Same
        val profileB1 = mockk<MultipassProfile>(relaxed = true)
        every { profileB1.id } returns "B1"
        every { profileB1.certificates.firstOrNull()?.firstName } returns "b1"
        every { profileB1.certificates.firstOrNull()?.name } returns "b1"
        every { profileB1.certificates.firstOrNull()?.rawBirthDate() } returns "dob2"

        val profileB2 = mockk<MultipassProfile>(relaxed = true)
        every { profileB2.id } returns "B2"
        every { profileB2.certificates.firstOrNull()?.firstName } returns "b1"
        every { profileB2.certificates.firstOrNull()?.name } returns "b1"
        every { profileB2.certificates.firstOrNull()?.rawBirthDate() } returns "dob2"

        // Different firstName
        val profileC1 = mockk<MultipassProfile>(relaxed = true)
        every { profileC1.id } returns "C1"
        every { profileC1.certificates.firstOrNull()?.firstName } returns "c1"
        every { profileC1.certificates.firstOrNull()?.name } returns "c1"
        every { profileC1.certificates.firstOrNull()?.rawBirthDate() } returns "dob3"

        val profileC2 = mockk<MultipassProfile>(relaxed = true)
        every { profileC2.id } returns "C2"
        every { profileC2.certificates.firstOrNull()?.firstName } returns "c2"
        every { profileC2.certificates.firstOrNull()?.name } returns "c1"
        every { profileC2.certificates.firstOrNull()?.rawBirthDate() } returns "dob3"

        // Different DoB
        val profileD1 = mockk<MultipassProfile>(relaxed = true)
        every { profileD1.id } returns "D1"
        every { profileD1.certificates.firstOrNull()?.firstName } returns "d"
        every { profileD1.certificates.firstOrNull()?.name } returns "d"
        every { profileD1.certificates.firstOrNull()?.rawBirthDate() } returns "dob4"

        val profileD2 = mockk<MultipassProfile>(relaxed = true)
        every { profileD2.id } returns "D2"
        every { profileD2.certificates.firstOrNull()?.firstName } returns "d"
        every { profileD2.certificates.firstOrNull()?.name } returns "d"
        every { profileD2.certificates.firstOrNull()?.rawBirthDate() } returns "dob5"

        val expectedIds = listOf("C1", "C2", "A1", "A2", "D1", "D2")
        val expectedProfilesMismatchLastnameIds = listOf("A1", "A2")
        val expectedProfilesMismatchFirstnameIds = listOf("C1", "C2")
        val expectedProfilesMismatchBirthdateIds = listOf("D1", "D2")

        val actualCloseMultipassProfiles = useCase(
            listOf(
                profileA1,
                profileA2,
                profileB1,
                profileB2,
                profileC1,
                profileC2,
                profileD1,
                profileD2,
            ),
        )

        val actualIds = actualCloseMultipassProfiles.profiles.map { it.id }
        val actualProfilesMismatchLastnameIds = actualCloseMultipassProfiles.profilesMismatchLastname.map { it.id }
        val actualProfilesMismatchFirstnameIds = actualCloseMultipassProfiles.profilesMismatchFirstname.map { it.id }
        val actualProfilesMismatchBirthdateIds = actualCloseMultipassProfiles.profilesMismatchDateOfBirth.map { it.id }

        assertContentEquals(expectedIds, actualIds)
        assertContentEquals(expectedProfilesMismatchLastnameIds, actualProfilesMismatchLastnameIds)
        assertContentEquals(expectedProfilesMismatchFirstnameIds, actualProfilesMismatchFirstnameIds)
        assertContentEquals(expectedProfilesMismatchBirthdateIds, actualProfilesMismatchBirthdateIds)
    }
}
