/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/12/16 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.utils

import com.lunabeestudio.domain.model.FigureType
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.model.KeyFigureChartType

object KeyFiguresHelper {

    fun defaultKeyFigureForFav(index: Int, isFavorite: Boolean, favoriteOrder: Float): KeyFigure = KeyFigure(
        index = index,
        category = "",
        labelKey = "labelKey",
        valueGlobalToDisplay = "",
        valueGlobal = 0.0,
        isFeatured = false,
        isHighlighted = false,
        extractDateS = 0L,
        valuesDepartments = null,
        displayOnSameChart = false,
        limitLine = null,
        chartType = KeyFigureChartType.LINES,
        series = null,
        avgSeries = null,
        magnitude = 0,
        isFavorite = isFavorite,
        favoriteOrder = favoriteOrder,
        figureType = FigureType.FLOAT,
    )
}
