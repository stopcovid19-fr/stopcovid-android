/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/2/16 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.model

data class CloseMultipassProfiles(
    val profilesMismatchFirstname: List<MultipassProfile>,
    val profilesMismatchLastname: List<MultipassProfile>,
    val profilesMismatchDateOfBirth: List<MultipassProfile>,
) {
    val profiles: List<MultipassProfile>
        get() = (profilesMismatchFirstname + profilesMismatchLastname + profilesMismatchDateOfBirth).distinct()

    fun isNotEmpty(): Boolean = profilesMismatchFirstname.isNotEmpty() ||
        profilesMismatchLastname.isNotEmpty() ||
        profilesMismatchDateOfBirth.isNotEmpty()

    fun joinToString(transform: ((MultipassProfile) -> CharSequence)): String {
        return profiles.joinToString(transform = transform)
    }
}
