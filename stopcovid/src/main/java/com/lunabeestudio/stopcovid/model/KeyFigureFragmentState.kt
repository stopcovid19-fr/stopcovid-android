/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/2/2 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.model

enum class KeyFigureFragmentState(val pagerPosition: Int) {
    FAVORITE(0),
    ALL(1),
    ;

    companion object {
        fun getByPosition(pagerPosition: Int): KeyFigureFragmentState? {
            return values().firstOrNull { it.pagerPosition == pagerPosition }
        }
    }
}
