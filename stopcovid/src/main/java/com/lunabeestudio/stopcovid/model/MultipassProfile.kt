/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/1/25 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.model

import com.lunabeestudio.stopcovid.extension.birthDate
import com.lunabeestudio.stopcovid.extension.rawBirthDate
import timber.log.Timber
import java.text.DateFormat

class MultipassProfile(
    val id: String,
    certificateInfo: EuropeanCertificate?,
    val certificates: List<EuropeanCertificate>,
    dateFormat: DateFormat,
) {

    val firstname: String? = certificateInfo?.firstName
    val lastname: String = certificateInfo?.name ?: certificateInfo?.greenCertificate?.person?.standardisedFamilyName ?: id
    val formattedBirthdate: String? = certificateInfo?.birthDate()?.let(dateFormat::format) ?: certificateInfo?.rawBirthDate()

    constructor(id: String, certificates: List<EuropeanCertificate>, dateFormat: DateFormat) : this(
        id,
        certificates.firstOrNull(),
        certificates,
        dateFormat,
    ) {
        if (certificates.isEmpty()) {
            Timber.e("Unexpected empty certificate list")
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MultipassProfile

        if (id != other.id) return false
        if (certificates != other.certificates) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + certificates.hashCode()
        return result
    }
}
