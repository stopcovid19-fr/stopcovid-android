/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.model

import com.lunabeestudio.domain.model.Configuration
import com.lunabeestudio.stopcovid.extension.isExpired
import com.lunabeestudio.stopcovid.extension.isNegativeTestValid
import com.lunabeestudio.stopcovid.usecase.ComputeDccValidityUseCase

enum class WalletSection(val index: Int, val titleKey: String) {
    FAVORITE(0, ""),
    VALID(1, "walletController.valid.title"),
    VALID_SOON(2, "walletController.validSoon.title"),
    NEGATIVE_TEST_VALID(3, "walletController.negTests.title"),
    OTHER(4, "walletController.others.title"),
    EXPIRED(5, "walletController.expired.title"),
    ;

    companion object {
        fun getSection(
            certificate: WalletCertificate,
            computeDccValidityUseCase: ComputeDccValidityUseCase,
            configuration: Configuration,
        ): WalletSection {
            return if (certificate is EuropeanCertificate) {
                val validity = computeDccValidityUseCase(certificate)
                val nowTime = System.currentTimeMillis()

                val isValid = validity?.start?.time?.let {
                    val start = validity.start?.time ?: Long.MAX_VALUE
                    val end = validity.end?.time ?: Long.MAX_VALUE
                    nowTime in start until end
                } ?: false

                val isValidSoon = validity?.start?.time?.let {
                    it > nowTime
                } ?: false

                when {
                    certificate.isFavorite -> FAVORITE
                    certificate.isExpired(configuration, validity) -> EXPIRED
                    isValid -> VALID
                    isValidSoon -> VALID_SOON
                    certificate.isNegativeTestValid(configuration) -> NEGATIVE_TEST_VALID
                    else -> OTHER
                }
            } else {
                OTHER
            }
        }
    }
}
