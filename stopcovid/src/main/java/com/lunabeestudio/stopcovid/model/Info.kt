/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.model

import android.content.Context
import com.lunabeestudio.common.extension.getApplicationLanguage
import org.json.JSONObject

/**
 * Abstract class to manage multi language for message and buttonTitle
 */
@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class Info(jsonObject: JSONObject) {

    var isActive: Boolean? = null
    private var _mode: String
    var minForcedBuildNumber: Int = 0
    var minRequiredBuildNumber: Int = 0
    var minInfoBuildNumber: Int? = null
    var appAvailability: Boolean = true
    private var titleMap: HashMap<String, String> = hashMapOf()
    private var messageMap: HashMap<String, String> = hashMapOf()
    var shouldPopConfettis: Boolean = false

    fun getTitle(context: Context): String? = getValueForDefaultLanguageOrNull(titleMap, context)
    fun getMessage(context: Context): String? = getValueForDefaultLanguageOrNull(messageMap, context)
    fun getButtonTitle(context: Context): String? = getValueForDefaultLanguageOrNull(buttonTitleMap, context)
    fun getButtonUrl(context: Context): String? = getValueForDefaultLanguageOrNull(buttonUrlMap, context)

    private var buttonTitleMap: HashMap<String, String> = hashMapOf()
    private var buttonUrlMap: HashMap<String, String> = hashMapOf()

    private fun getValueForDefaultLanguageOrNull(map: HashMap<String, String>, context: Context): String? {
        val result = map[context.getApplicationLanguage()] ?: map["en"]
        return if (result.isNullOrEmpty()) null else result
    }

    private fun mapStrings(jsonObject: JSONObject?, map: HashMap<String, String>) {
        jsonObject?.let {
            for (key in it.keys()) {
                map[key] = it.getString(key)
            }
        }
    }

    val mode: Mode?
        get() = Mode.getMode(_mode, appAvailability)

    init {
        val androidInfoJSONObject = jsonObject.optJSONObject("Android")
        isActive = androidInfoJSONObject.optBoolean("isActive")
        _mode = androidInfoJSONObject.optString("mode")
        minForcedBuildNumber = androidInfoJSONObject.optInt("minForcedBuildNumber", 0)
        minRequiredBuildNumber = androidInfoJSONObject.optInt("minRequiredBuildNumber", 0)
        minInfoBuildNumber = androidInfoJSONObject.optInt("minInfoBuild")
        appAvailability = androidInfoJSONObject.optBoolean("appAvailability", true)
        mapStrings(androidInfoJSONObject.optJSONObject("title"), titleMap)
        mapStrings(androidInfoJSONObject.optJSONObject("message"), messageMap)
        mapStrings(androidInfoJSONObject.optJSONObject("buttonTitle"), buttonTitleMap)
        mapStrings(androidInfoJSONObject.optJSONObject("buttonURL"), buttonUrlMap)
        shouldPopConfettis = androidInfoJSONObject.optBoolean("shouldPopConfettis", false)
    }

    enum class Mode {
        MAINTENANCE, UPGRADE, DISABLED;

        companion object {
            fun getMode(mode: String, appAvailability: Boolean): Mode? {
                return when {
                    !appAvailability -> DISABLED
                    mode == "maintenance" -> MAINTENANCE
                    mode == "upgrade" -> UPGRADE
                    else -> null
                }
            }
        }
    }
}
