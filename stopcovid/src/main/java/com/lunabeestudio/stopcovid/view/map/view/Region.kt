/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/12/21 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.view.map.view

data class Region(
    val xmlName: String,
    val name: String?,
    val backgroundColor: Int?,
    val colorSelected: Int?,
    val value: Float?,
    val date: String?,
    val formattedValue: String,
)
