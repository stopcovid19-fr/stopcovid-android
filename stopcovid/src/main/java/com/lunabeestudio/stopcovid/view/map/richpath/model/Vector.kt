/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by tarek360, Modified by Lunabee Studio / Date - 2022/12/21 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.view.map.richpath.model

import android.content.Context
import android.content.res.XmlResourceParser
import com.lunabeestudio.stopcovid.view.map.richpath.RichPath
import com.lunabeestudio.stopcovid.view.map.richpath.util.XmlParser

class Vector {
    companion object {
        const val TAG_NAME = "vector"
    }

    var height: Float = 0f
        private set
    var width: Float = 0f
        private set

    var viewportWidth: Float = 0f
        private set
    var viewportHeight: Float = 0f
        private set
    var currentWidth: Float = 0f
    var currentHeight: Float = 0f

    var paths: ArrayList<RichPath> = ArrayList()

    fun inflate(xpp: XmlResourceParser, context: Context) {
        width = XmlParser.getAttributeDimen(context, xpp, "width", width)
        height = XmlParser.getAttributeDimen(context, xpp, "height", height)
        viewportWidth = XmlParser.getAttributeFloat(xpp, "viewportWidth", viewportWidth)
        viewportHeight = XmlParser.getAttributeFloat(xpp, "viewportHeight", viewportHeight)
        currentWidth = viewportWidth
        currentHeight = viewportHeight
    }
}
