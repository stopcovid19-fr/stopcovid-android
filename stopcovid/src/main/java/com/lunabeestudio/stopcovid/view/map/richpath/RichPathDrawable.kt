/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by tarek360, Modified by Lunabee Studio / Date - 2022/12/21 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.view.map.richpath

import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.Matrix
import android.graphics.PixelFormat
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.view.MotionEvent
import android.widget.ImageView.ScaleType
import androidx.annotation.IntRange
import com.lunabeestudio.stopcovid.view.map.richpath.listener.OnRichPathUpdatedListener
import com.lunabeestudio.stopcovid.view.map.richpath.model.Vector
import com.lunabeestudio.stopcovid.view.map.richpath.util.PathUtils
import kotlin.math.min

class RichPathDrawable(
    private val vector: Vector?, private val scaleType: ScaleType,
) : Drawable() {

    private var width: Int = 0
    private var height: Int = 0

    init {
        listenToPathsUpdates()
    }

    override fun onBoundsChange(bounds: Rect) {
        super.onBoundsChange(bounds)
        bounds.let {
            if (it.width() > 0 && it.height() > 0) {
                width = it.width()
                height = it.height()
                mapPaths()
            }
        }
    }

    private fun mapPaths() {
        val vector = vector ?: return

        val centerX = width / 2f
        val centerY = height / 2f

        val matrix = Matrix()

        matrix.postTranslate(
            centerX - vector.currentWidth / 2,
            centerY - vector.currentHeight / 2,
        )

        val widthRatio = width / vector.currentWidth
        val heightRatio = height / vector.currentHeight

        if (scaleType == ScaleType.FIT_XY) {
            matrix.postScale(widthRatio, heightRatio, centerX, centerY)
        } else {
            val ratio: Float = if (width < height) {
                widthRatio
            } else {
                heightRatio
            }
            matrix.postScale(ratio, ratio, centerX, centerY)
        }

        val absWidthRatio = width / vector.viewportWidth
        val absHeightRatio = height / vector.viewportHeight
        val absRatio = min(absWidthRatio, absHeightRatio)

        vector.paths.forEach {
            it.mapToMatrix(matrix)
            it.scaleStrokeWidth(absRatio)
        }

        vector.currentWidth = width.toFloat()
        vector.currentHeight = height.toFloat()
    }

    fun findAllRichPaths(): Array<RichPath> {
        return vector?.paths?.toTypedArray() ?: arrayOf()
    }

    fun findRichPathByName(name: String): RichPath? {
        return vector?.paths?.firstOrNull {
            it.xmlName == name
        }
    }

    private fun listenToPathsUpdates() {
        vector?.paths?.forEach {
            it.onRichPathUpdatedListener = object : OnRichPathUpdatedListener {
                override fun onPathUpdated() {
                    invalidateSelf()
                }
            }
        }
    }

    fun getTouchedPath(event: MotionEvent?): RichPath? {
        if (event?.action == MotionEvent.ACTION_UP) {
            vector?.paths?.forEach {
                if (PathUtils.isTouched(it, event.x, event.y)) {
                    return it
                }
            }
        }
        return null
    }

    override fun draw(canvas: Canvas) {
        vector?.paths?.forEach {
            it.draw(canvas)
        }
    }

    override fun setAlpha(@IntRange(from = 0, to = 255) alpha: Int) {}

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {}
}
