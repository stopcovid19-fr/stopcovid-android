/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by tarek360, Modified by Lunabee Studio / Date - 2022/12/21 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.view.map.richpath

import android.content.Context
import android.content.res.XmlResourceParser
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PathMeasure
import com.lunabeestudio.stopcovid.view.map.richpath.listener.OnRichPathUpdatedListener
import com.lunabeestudio.stopcovid.view.map.richpath.model.Group
import com.lunabeestudio.stopcovid.view.map.richpath.pathparser.PathDataNode
import com.lunabeestudio.stopcovid.view.map.richpath.pathparser.PathParser
import com.lunabeestudio.stopcovid.view.map.richpath.pathparser.PathParserCompat
import com.lunabeestudio.stopcovid.view.map.richpath.util.PathUtils
import com.lunabeestudio.stopcovid.view.map.richpath.util.XmlParser

class RichPath(private val src: Path) : Path(src) {

    companion object {
        const val TAG_NAME = "path"
    }

    var fillColor: Int = Color.TRANSPARENT
        set(value) {
            field = value
            onPathUpdated()
        }
    var strokeColor: Int = Color.TRANSPARENT
        set(value) {
            field = value
            onPathUpdated()
        }
    var strokeWidth: Float = 0f
        set(value) {
            field = value
            onPathUpdated()
        }
    private var trimPathStart: Float = 0f
        set(value) {
            field = value
            trim()
            onPathUpdated()
        }
    private var trimPathEnd: Float = 1f
        set(value) {
            field = value
            trim()
            onPathUpdated()
        }
    private var trimPathOffset: Float = 0f
        set(value) {
            field = value
            trim()
            onPathUpdated()
        }

    private var strokeLineCap: Paint.Cap = Paint.Cap.BUTT
        set(value) {
            field = value
            onPathUpdated()
        }
    private var strokeLineJoin: Paint.Join = Paint.Join.MITER
        set(value) {
            field = value
            onPathUpdated()
        }

    private var strokeMiterLimit: Float = 4f
        set(value) {
            field = value
            onPathUpdated()
        }

    var xmlName: String? = null
    private lateinit var paint: Paint

    private var originalWidth: Float = 0f
        private set
    private var originalHeight: Float = 0f
        private set

    var onRichPathUpdatedListener: OnRichPathUpdatedListener? = null
        internal set

    private var pathMeasure: PathMeasure? = null

    private var pathDataNodes: Array<PathDataNode>? = null
        set(value) {
            value ?: return
            PathUtils.setPathDataNodes(this, value)
            field = value
            for (matrix in matrices) {
                transform(matrix)
            }
            onPathUpdated()
        }

    private lateinit var matrices: ArrayList<Matrix>

    internal var onPathClickListener: OnPathClickListener? = null

    constructor(pathData: String) : this(PathParser.createPathFromPathData(pathData))

    init {
        init()
    }

    private fun init() {
        paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            style = Paint.Style.STROKE
        }
        matrices = arrayListOf()
        updateOriginalDimens()
    }

    internal fun draw(canvas: Canvas) {
        paint.run {
            color = fillColor
            style = Paint.Style.FILL
            canvas.drawPath(this@RichPath, this)

            color = strokeColor
            style = Paint.Style.STROKE
            canvas.drawPath(this@RichPath, this)
        }
    }

    fun applyGroup(group: Group) {
        mapToMatrix(group.matrix())
    }

    internal fun mapToMatrix(matrix: Matrix) {
        matrices.add(matrix)
        transform(matrix)
        src.transform(matrix)
        updateOriginalDimens()
    }

    internal fun scaleStrokeWidth(scale: Float) {
        paint.strokeWidth = strokeWidth * scale
    }

    fun inflate(context: Context, xpp: XmlResourceParser) {
        val pathData = XmlParser.getAttributeString(context, xpp, "pathData", xmlName)
        pathDataNodes = PathParserCompat.createNodesFromPathData(pathData)
        xmlName = XmlParser.getAttributeString(context, xpp, "name", xmlName)
        fillColor = XmlParser.getAttributeColor(context, xpp, "fillColor", fillColor)
        updatePaint()

        trim()
    }

    private fun updateOriginalDimens() {
        originalWidth = PathUtils.getPathWidth(this)
        originalHeight = PathUtils.getPathHeight(this)
    }

    private fun trim() {
        if (trimPathStart != 0.0f || trimPathEnd != 1.0f) {
            var start = (trimPathStart + trimPathOffset) % 1.0f
            var end = (trimPathEnd + trimPathOffset) % 1.0f
            val pathMeasure = pathMeasure ?: PathMeasure()
            pathMeasure.setPath(src, false)
            val len = pathMeasure.length
            start *= len
            end *= len
            reset()
            if (start > end) {
                pathMeasure.getSegment(start, len, this, true)
                pathMeasure.getSegment(0f, end, this, true)
            } else {
                pathMeasure.getSegment(start, end, this, true)
            }
            rLineTo(0f, 0f) // fix bug in measure
        }
    }

    private fun updatePaint() {
        paint.strokeCap = strokeLineCap
        paint.strokeJoin = strokeLineJoin
        paint.strokeMiter = strokeMiterLimit
        paint.strokeWidth = strokeWidth
    }

    private fun onPathUpdated() {
        onRichPathUpdatedListener?.onPathUpdated()
    }

    interface OnPathClickListener {
        fun onClick(richPath: RichPath)
    }
}
