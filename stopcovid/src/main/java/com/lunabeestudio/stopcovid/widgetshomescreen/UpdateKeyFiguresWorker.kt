/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.widgetshomescreen

import android.content.Context
import androidx.work.Constraints
import androidx.work.CoroutineWorker
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.NetworkType
import androidx.work.PeriodicWorkRequest
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import com.lunabeestudio.stopcovid.Constants
import com.lunabeestudio.stopcovid.extension.keyFigureRepository
import java.util.concurrent.TimeUnit

class UpdateKeyFiguresWorker(context: Context, params: WorkerParameters) : CoroutineWorker(context, params) {
    override suspend fun doWork(): Result {
        applicationContext.keyFigureRepository().onAppForeground()
        KeyFiguresWidget.updateWidget(applicationContext)
        return Result.success()
    }

    companion object {

        fun scheduleWorker(context: Context) {
            val constraints: Constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

            val periodicWorkRequest: PeriodicWorkRequest =
                PeriodicWorkRequestBuilder<UpdateKeyFiguresWorker>(
                    Constants.HomeScreenWidget.WORKER_UPDATE_FIGURES_PERIODIC_REFRESH_HOURS,
                    TimeUnit.HOURS,
                )
                    .setConstraints(constraints)
                    .setInitialDelay(
                        Constants.HomeScreenWidget.WORKER_UPDATE_FIGURES_PERIODIC_REFRESH_HOURS,
                        TimeUnit.HOURS,
                    )
                    .build()
            WorkManager
                .getInstance(context)
                .enqueueUniquePeriodicWork(
                    Constants.HomeScreenWidget.WORKER_UPDATE_FIGURES_NAME,
                    ExistingPeriodicWorkPolicy.KEEP,
                    periodicWorkRequest,
                )
        }
    }
}
