/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.domain.repository.KeyFigureRepository
import com.lunabeestudio.stopcovid.core.manager.LocalizedStrings
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class KeyFigureDetailsFragmentViewModel(
    private val strings: LocalizedStrings,
    private val keyFigureRepository: KeyFigureRepository,
) : ViewModel(), KeyFigureDetailCompareViewModelDelegate by KeyFigureDetailCompareViewModelDelegateImpl() {

    fun keyFigure(label: String): Flow<TacResult<KeyFigure?>> = keyFigureRepository.keyFigureByLabelWithDepartmentFlow(
        label,
    )
        .map { result ->
            result.data?.let { generateComparisonDataFromKeyFigure(it, strings) }
            result
        }

    fun toggleFavorite(keyFigure: KeyFigure) {
        viewModelScope.launch {
            keyFigureRepository.toggleFavorite(keyFigure)
        }
    }
}

class KeyFigureDetailsFragmentViewModelFactory(
    val strings: LocalizedStrings,
    private val keyFigureRepository: KeyFigureRepository,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return KeyFigureDetailsFragmentViewModel(
            strings,
            keyFigureRepository,
        ) as T
    }
}
