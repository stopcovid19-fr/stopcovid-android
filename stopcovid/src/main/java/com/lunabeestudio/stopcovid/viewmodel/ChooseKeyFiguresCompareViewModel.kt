/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.lunabeestudio.domain.model.Configuration
import com.lunabeestudio.domain.repository.KeyFigureRepository
import com.lunabeestudio.stopcovid.core.manager.LocalizedStrings
import com.lunabeestudio.stopcovid.extension.getDefaultFigureLabel1
import com.lunabeestudio.stopcovid.extension.getDefaultFigureLabel2
import com.lunabeestudio.stopcovid.extension.keyFigureCompare1
import com.lunabeestudio.stopcovid.extension.keyFigureCompare2
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch

class ChooseKeyFiguresCompareViewModel(
    val strings: LocalizedStrings,
    val sharedPreferences: SharedPreferences,
    val configuration: Configuration,
    private val keyFigureRepository: KeyFigureRepository,
) : ViewModel() {

    private val _state: MutableStateFlow<ChooseKeyFiguresCompareState> = MutableStateFlow(
        ChooseKeyFiguresCompareState(
            labelKey1 = sharedPreferences.keyFigureCompare1 ?: configuration.getDefaultFigureLabel1(),
            labelKey2 = sharedPreferences.keyFigureCompare2 ?: configuration.getDefaultFigureLabel2(),
        ),
    )
    val state: StateFlow<ChooseKeyFiguresCompareState> = _state

    init {
        viewModelScope.launch {
            combine(flow = state, flow2 = keyFigureRepository.allKeyFiguresWithFavoritesFlow()) { _, _ ->
                verifyIfKeyFigureExist()
            }.collect()
        }
    }

    fun setNewLabelKey(newLabelKey1: String?, newLabelKey2: String?) {
        _state.value = _state.value.copy(labelKey1 = newLabelKey1, labelKey2 = newLabelKey2)
    }

    private suspend fun verifyIfKeyFigureExist() {
        val keys = listOf(
            1 to _state.value.labelKey1,
            2 to _state.value.labelKey2,
        )
        keys.forEach { (keyFigureIndex, labelKey) ->
            val keyString = strings["$labelKey.shortLabel"]
            val keyFigure = labelKey?.let { keyFigureRepository.keyFigureByLabel(it) }
            if (keyString == null || keyFigure?.data == null) {
                if (keyFigureIndex == 1) {
                    _state.emit(_state.value.copy(labelKey1 = null))
                } else {
                    _state.emit(_state.value.copy(labelKey2 = null))
                }
            }
        }
    }

    fun getLabelActionChoiceKey(keyFigure: Int, labelKey: String?): String? {
        return if (labelKey == null) {
            strings["keyfigures.comparison.keyfiguresList.screen.title$keyFigure"]
        } else {
            strings["$labelKey.shortLabel"]
        }
    }
}

data class ChooseKeyFiguresCompareState(
    val labelKey1: String?,
    val labelKey2: String?,
) {
    val isBothKeyFigureSelected: Boolean
        get() = labelKey1 != null && labelKey2 != null
}

class ChooseKeyFiguresCompareViewModelFactory(
    val strings: LocalizedStrings,
    val sharedPreferences: SharedPreferences,
    val configuration: Configuration,
    private val keyFigureRepository: KeyFigureRepository,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return ChooseKeyFiguresCompareViewModel(
            strings,
            sharedPreferences,
            configuration,
            keyFigureRepository,
        ) as T
    }
}
