/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.stopcovid.core.utils.Event
import com.lunabeestudio.stopcovid.core.utils.SingleLiveEvent
import com.lunabeestudio.stopcovid.manager.ConfigurationManager
import com.lunabeestudio.stopcovid.model.EuropeanCertificate
import com.lunabeestudio.stopcovid.model.SmartWalletMap
import com.lunabeestudio.stopcovid.model.WalletCertificate
import com.lunabeestudio.stopcovid.model.WalletSection
import com.lunabeestudio.stopcovid.repository.WalletRepository
import com.lunabeestudio.stopcovid.usecase.ComputeDccValidityUseCase
import com.lunabeestudio.stopcovid.usecase.GetSmartWalletMapUseCase
import com.lunabeestudio.stopcovid.usecase.IsCertificateEUValidUseCase
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

class WalletCertificateListViewModel(
    private val configurationManager: ConfigurationManager,
    private val walletRepository: WalletRepository,
    getSmartWalletMapUseCase: GetSmartWalletMapUseCase,
    private val computeDccValidityUseCase: ComputeDccValidityUseCase,
    private val isCertificateEUValidUseCase: IsCertificateEUValidUseCase,
) : ViewModel() {

    val error: SingleLiveEvent<Exception> = SingleLiveEvent()
    val loading: SingleLiveEvent<Boolean> = SingleLiveEvent()

    private val _scrollEvent: MutableLiveData<Event<WalletCertificate>> = MutableLiveData()
    val scrollEvent: LiveData<Event<WalletCertificate>>
        get() = _scrollEvent

    private var previousCertificatesId = emptyList<String>()

    val certificates: StateFlow<TacResult<List<WalletCertificate>>> = walletRepository.walletCertificateFlow
        .map { certificates ->
            if (previousCertificatesId.isNotEmpty()) {
                certificates
                    .data
                    ?.find { it.id !in previousCertificatesId }
                    ?.let { _scrollEvent.value = Event(it) }
            }
            previousCertificatesId = certificates.data?.map { it.id }.orEmpty()
            loading.postValue(false)
            certificates
        }.stateIn(viewModelScope, SharingStarted.WhileSubscribed(), TacResult.Loading(emptyList()))

    val groupedCertificates: TacResult<Map<WalletSection, List<WalletCertificate>>>
        get() = certificates.value.mapData { certificates ->
            certificates
                ?.sortedByDescending { it.timestamp }
                ?.groupBy { certificate ->
                    WalletSection.getSection(certificate, computeDccValidityUseCase, configurationManager.configuration)
                }?.toSortedMap(compareBy { it.index })
        }

    val smartWalletProfiles: StateFlow<SmartWalletMap?> = getSmartWalletMapUseCase()
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(), emptyMap())

    fun removeCertificate(certificate: WalletCertificate) {
        viewModelScope.launch {
            loading.postValue(true)
            walletRepository.deleteCertificate(certificate)
        }
    }

    fun toggleFavorite(
        walletCertificate: EuropeanCertificate,
    ) {
        viewModelScope.launch {
            try {
                loading.postValue(true)
                walletRepository.toggleFavorite(walletCertificate)
            } catch (e: Exception) {
                loading.postValue(false)
                error.postValue(e)
            }
        }
    }

    fun isCertificateEUValid(certificate: EuropeanCertificate): Boolean = isCertificateEUValidUseCase(certificate)
}

class WalletCertificateListViewModelFactory(
    private val configurationManager: ConfigurationManager,
    private val walletRepository: WalletRepository,
    private val getSmartWalletMapUseCase: GetSmartWalletMapUseCase,
    private val computeDccValidityUseCase: ComputeDccValidityUseCase,
    private val isCertificateEUValidUseCase: IsCertificateEUValidUseCase,
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return WalletCertificateListViewModel(
            configurationManager,
            walletRepository,
            getSmartWalletMapUseCase,
            computeDccValidityUseCase,
            isCertificateEUValidUseCase,
        ) as T
    }
}
