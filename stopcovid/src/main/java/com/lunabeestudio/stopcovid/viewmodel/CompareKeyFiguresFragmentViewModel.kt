/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.repository.KeyFigureRepository
import kotlinx.coroutines.flow.Flow

class CompareKeyFiguresFragmentViewModel(
    private val keyFigureRepository: KeyFigureRepository,
) : ViewModel() {

    var keyFigure1: KeyFigure? = null
    var keyFigure2: KeyFigure? = null

    val allKeyFiguresWithFavoritesFlow: Flow<List<KeyFigure>> = keyFigureRepository.allKeyFiguresWithFavoritesFlow()

    suspend fun setKeyFiguresWithLabel(label1: String?, label2: String?) {
        keyFigure1 = label1?.let { keyFigureRepository.keyFigureByLabel(it).data }
        keyFigure2 = label2?.let { keyFigureRepository.keyFigureByLabel(it).data }
    }
}

class CompareKeyFiguresFragmentViewModelFactory(
    private val keyFigureRepository: KeyFigureRepository,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return CompareKeyFiguresFragmentViewModel(
            keyFigureRepository,
        ) as T
    }
}
