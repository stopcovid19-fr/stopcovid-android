/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.stopcovid.model.EuropeanCertificate
import com.lunabeestudio.stopcovid.repository.WalletRepository
import com.lunabeestudio.stopcovid.usecase.IsCertificateEUValidUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class WalletFullscreenDccViewModel(
    private val walletRepository: WalletRepository,
    private val isCertificateEUValidUseCase: IsCertificateEUValidUseCase,
) : ViewModel() {
    fun getCertificate(id: String): Flow<TacResult<EuropeanCertificate>> {
        return walletRepository.walletCertificateFlow.map { result ->
            result.mapData { certificates ->
                certificates
                    ?.filterIsInstance<EuropeanCertificate>()
                    ?.firstOrNull { certificate -> certificate.id == id }
            }
        }
    }

    fun isCertificateEUValid(certificate: EuropeanCertificate) = isCertificateEUValidUseCase(certificate)
}

class WalletFullscreenDccViewModelFactory(
    private val walletRepository: WalletRepository,
    private val isCertificateEUValidUseCase: IsCertificateEUValidUseCase,
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return WalletFullscreenDccViewModel(
            walletRepository,
            isCertificateEUValidUseCase,
        ) as T
    }
}
