/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.domain.repository.KeyFigureRepository

class SelectKeyFiguresFragmentViewModel(
    private val keyFigureRepository: KeyFigureRepository,
) : ViewModel() {

    suspend fun getKeyFigures(): TacResult<List<KeyFigure>> {
        return keyFigureRepository.allKeyFigures()
    }
}

class SelectKeyFiguresFragmentViewModelFactory(
    private val keyFigureRepository: KeyFigureRepository,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return SelectKeyFiguresFragmentViewModel(
            keyFigureRepository,
        ) as T
    }
}
