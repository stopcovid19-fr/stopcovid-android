/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/01/03 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import android.content.Context
import android.graphics.Bitmap
import android.graphics.pdf.PdfRenderer
import android.os.ParcelFileDescriptor
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.lunabeestudio.domain.model.FeaturedInfo
import com.lunabeestudio.common.extension.getFileNameHDFromFeaturedInfo
import com.lunabeestudio.domain.repository.FeaturedInfoRepository
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.stateIn
import timber.log.Timber
import java.io.File

class FullScreenFeaturedInfoViewModel(
    private val featuredInfoRepository: FeaturedInfoRepository,
    val id: Int,
) : ViewModel() {

    val featuredInfo: StateFlow<FeaturedInfo?> = flow {
        emit(featuredInfoRepository.getFeaturedInfoByIndex(id))
    }.stateIn(viewModelScope, SharingStarted.Eagerly, null)

    suspend fun refreshImagePDF() {
        featuredInfo.value?.let {
            featuredInfoRepository.saveResourceInLocal(
                it.hd,
                it.getFileNameHDFromFeaturedInfo(),
            )
        }
    }

    fun getBitmapFromPdfFile(context: Context): Bitmap? {
        return try {
            val file = File(context.filesDir, featuredInfo.value?.getFileNameHDFromFeaturedInfo().orEmpty())
            val fileDescriptor = ParcelFileDescriptor.open(
                file,
                ParcelFileDescriptor.MODE_READ_ONLY,
            )
            PdfRenderer(fileDescriptor).use { pdfRenderer ->
                pdfRenderer.openPage(0).use { page ->
                    val bitmap = Bitmap.createBitmap(
                        featuredInfo.value?.hdWidth ?: page.width,
                        featuredInfo.value?.hdHeight ?: page.height,
                        Bitmap.Config.ARGB_8888,
                    )
                    page.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY)
                    bitmap
                }
            }
        } catch (e: Exception) {
            Timber.e(e)
            null
        }
    }
}

class FullScreenFeaturedInfoViewModelFactory(
    private val featuredInfoRepository: FeaturedInfoRepository,
    val id: Int,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return FullScreenFeaturedInfoViewModel(
            featuredInfoRepository,
            id,
        ) as T
    }
}
