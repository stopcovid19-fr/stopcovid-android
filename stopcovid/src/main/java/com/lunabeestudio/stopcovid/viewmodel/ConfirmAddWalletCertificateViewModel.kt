/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.stopcovid.core.utils.SingleLiveEvent
import com.lunabeestudio.stopcovid.model.WalletCertificate
import com.lunabeestudio.stopcovid.repository.WalletRepository
import com.lunabeestudio.stopcovid.worker.BlacklistWorker
import kotlinx.coroutines.launch

class ConfirmAddWalletCertificateViewModel(
    private val walletRepository: WalletRepository,
) : ViewModel() {

    val error: SingleLiveEvent<Exception> = SingleLiveEvent()
    val loading: SingleLiveEvent<Boolean> = SingleLiveEvent()

    val certificates: TacResult<List<WalletCertificate>>
        get() = walletRepository.walletCertificateFlow.value

    suspend fun saveCertificate(walletCertificate: WalletCertificate, context: Context?) {
        walletRepository.saveCertificate(walletCertificate)
        context?.let { BlacklistWorker.start(it, walletCertificate.id) }
    }

    fun isDuplicated(certificate: WalletCertificate): Boolean {
        return walletRepository.certificateExists(certificate)
    }

    suspend fun forceRefreshCertificates() {
        walletRepository.forceRefreshCertificatesFlow()
    }

    fun deleteLostCertificates() {
        viewModelScope.launch {
            walletRepository.deleteLostCertificates()
        }
    }

    fun resetWalletCryptoKeyGeneratedFlag() {
        walletRepository.resetKeyCryptoGeneratedFlag()
    }
}

class ConfirmAddWalletCertificateViewModelFactory(
    private val walletRepository: WalletRepository,
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return ConfirmAddWalletCertificateViewModel(
            walletRepository,
        ) as T
    }
}
