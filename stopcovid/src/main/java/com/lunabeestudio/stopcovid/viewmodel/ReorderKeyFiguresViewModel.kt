/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.stopcovid.usecase.GetFavoriteKeyFiguresUseCase
import com.lunabeestudio.stopcovid.usecase.ReorderFavoriteKeyFiguresUseCase
import kotlinx.coroutines.launch

class ReorderKeyFiguresViewModel(
    private val getFavoriteKeyFiguresUseCase: GetFavoriteKeyFiguresUseCase,
    private val reorderFavoriteKeyFiguresUseCase: ReorderFavoriteKeyFiguresUseCase,
) : ViewModel() {

    val favFigures: LiveData<List<KeyFigure>?> by lazy {
        getFavoriteKeyFiguresUseCase().asLiveData(viewModelScope.coroutineContext)
    }

    fun reorderFavorite(oldPosition: Int, newPosition: Int) {
        viewModelScope.launch {
            reorderFavoriteKeyFiguresUseCase.invoke(favFigures.value, oldPosition, newPosition)
        }
    }
}

class ReorderKeyFiguresViewModelFactory(
    private val getFavoriteKeyFiguresUseCase: GetFavoriteKeyFiguresUseCase,
    private val reorderFavoriteKeyFiguresUseCase: ReorderFavoriteKeyFiguresUseCase,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return ReorderKeyFiguresViewModel(
            getFavoriteKeyFiguresUseCase,
            reorderFavoriteKeyFiguresUseCase,
        ) as T
    }
}
