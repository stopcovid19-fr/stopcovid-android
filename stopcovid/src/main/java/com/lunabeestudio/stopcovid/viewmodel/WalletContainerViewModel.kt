/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.stopcovid.model.WalletCertificate
import com.lunabeestudio.stopcovid.repository.WalletRepository
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

class WalletContainerViewModel(
    private val walletRepository: WalletRepository,
) : ViewModel() {

    val certificates: StateFlow<TacResult<List<WalletCertificate>>> = walletRepository.walletCertificateFlow
        .stateIn(viewModelScope, SharingStarted.Eagerly, TacResult.Loading(emptyList()))

    suspend fun getCertificatesCount(): Int {
        return walletRepository.getCertificateCount()
    }

    suspend fun forceRefreshCertificates() {
        walletRepository.forceRefreshCertificatesFlow()
    }

    fun deleteLostCertificates() {
        viewModelScope.launch {
            walletRepository.deleteLostCertificates()
        }
    }
}

class WalletContainerViewModelFactory(
    private val walletRepository: WalletRepository,
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return WalletContainerViewModel(
            walletRepository,
        ) as T
    }
}
