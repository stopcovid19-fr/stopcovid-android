/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.domain.repository.KeyFigureRepository
import com.lunabeestudio.stopcovid.usecase.GetFavoriteKeyFiguresUseCase
import kotlinx.coroutines.launch

class KeyFiguresViewModel(
    private val keyFigureRepository: KeyFigureRepository,
    private val getFavoriteKeyFiguresUseCase: GetFavoriteKeyFiguresUseCase,
) : ViewModel() {

    val figures: LiveData<TacResult<List<KeyFigure>?>> by lazy {
        keyFigureRepository.keyFiguresWithDepartmentFlow.asLiveData(viewModelScope.coroutineContext)
    }

    val favFigures: LiveData<List<KeyFigure>?> by lazy {
        getFavoriteKeyFiguresUseCase().asLiveData(viewModelScope.coroutineContext)
    }

    fun toggleFavorite(keyFigure: KeyFigure) {
        viewModelScope.launch {
            keyFigureRepository.toggleFavorite(keyFigure)
        }
    }
}

class KeyFiguresViewModelFactory(
    private val keyFigureRepository: KeyFigureRepository,
    private val getFavoriteKeyFiguresUseCase: GetFavoriteKeyFiguresUseCase,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return KeyFiguresViewModel(
            keyFigureRepository,
            getFavoriteKeyFiguresUseCase,
        ) as T
    }
}
