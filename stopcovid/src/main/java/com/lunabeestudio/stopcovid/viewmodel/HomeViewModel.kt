/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import com.lunabeestudio.domain.model.FeaturedInfo
import com.lunabeestudio.domain.model.InfoCenterData
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.stopcovid.core.model.WalletState
import com.lunabeestudio.stopcovid.core.utils.Event
import com.lunabeestudio.common.extension.getFileThumbnailFromFeaturedInfo
import com.lunabeestudio.domain.repository.FeaturedInfoRepository
import com.lunabeestudio.domain.repository.InfoCenterRepository
import com.lunabeestudio.domain.repository.KeyFigureMapRepository
import com.lunabeestudio.domain.repository.KeyFigureRepository
import com.lunabeestudio.stopcovid.extension.showSmartWallet
import com.lunabeestudio.stopcovid.manager.ConfigurationManager
import com.lunabeestudio.stopcovid.model.EuropeanCertificate
import com.lunabeestudio.stopcovid.model.SmartWalletMap
import com.lunabeestudio.stopcovid.model.SmartWalletState
import com.lunabeestudio.stopcovid.repository.WalletRepository
import com.lunabeestudio.stopcovid.usecase.GetFavoriteKeyFiguresUseCase
import com.lunabeestudio.stopcovid.usecase.GetKeyFiguresWithCompareUseCase
import com.lunabeestudio.stopcovid.usecase.GetSmartWalletMapUseCase
import com.lunabeestudio.stopcovid.usecase.GetSmartWalletStateUseCase
import com.lunabeestudio.stopcovid.usecase.RefreshInfoCenterUseCase
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn

class HomeViewModel(
    private val configurationManager: ConfigurationManager,
    walletRepository: WalletRepository,
    private val sharedPreferences: SharedPreferences,
    getSmartWalletMapMapUseCase: GetSmartWalletMapUseCase,
    private val getSmartWalletStateUseCase: GetSmartWalletStateUseCase,
    private val keyFigureRepository: KeyFigureRepository,
    keyFigureMapRepository: KeyFigureMapRepository,
    private val infoCenterRepository: InfoCenterRepository,
    private val refreshInfoCenterUseCase: RefreshInfoCenterUseCase,
    private val getCompareKeyFiguresUseCase: GetKeyFiguresWithCompareUseCase,
    getFavoriteKeyFiguresUseCase: GetFavoriteKeyFiguresUseCase,
    private val featuredInfoRepository: FeaturedInfoRepository,
) : ViewModel() {

    val loadingInProgress: MutableLiveData<Boolean> = MutableLiveData(false)

    val favoriteDcc: LiveData<Event<EuropeanCertificate?>> = walletRepository.walletCertificateFlow.map { list ->
        Event(
            list
                .data
                ?.filterIsInstance<EuropeanCertificate>()
                ?.firstOrNull { certificate ->
                    (certificate as? EuropeanCertificate)?.isFavorite == true
                },
        )
    }.asLiveData(timeoutInMs = 0)

    val smartWalletMap: StateFlow<SmartWalletMap?> = getSmartWalletMapMapUseCase()
        .stateIn(viewModelScope, SharingStarted.Eagerly, emptyMap())

    val keyFigures: LiveData<TacResult<List<KeyFigure>?>> = keyFigureRepository.keyFiguresWithDepartmentFlow
        .asLiveData(viewModelScope.coroutineContext)

    val hasKeyFigureMap: StateFlow<Boolean> = keyFigureMapRepository.availableKeyFiguresFlow().map {
        it.isNotEmpty()
    }.stateIn(viewModelScope, SharingStarted.Eagerly, false)

    val favKeyFigures: LiveData<List<KeyFigure>?> = getFavoriteKeyFiguresUseCase()
        .asLiveData(viewModelScope.coroutineContext)

    val featuredInfo: StateFlow<List<FeaturedInfo>> = featuredInfoRepository.flowAllFeaturedInfo()
        .stateIn(viewModelScope, SharingStarted.Eagerly, listOf())

    val nowTimeMs: MutableLiveData<Long> = MutableLiveData(System.currentTimeMillis())

    val infoCenterData: LiveData<InfoCenterData> = nowTimeMs.switchMap {
        infoCenterRepository.getHomeInfoCenterData(NUMBER_NEWS_DISPLAYED, it / 1000).asLiveData(
            viewModelScope.coroutineContext,
        )
    }

    suspend fun refreshImageFeaturedInfo() {
        featuredInfo.value.forEach { info ->
            featuredInfoRepository.saveResourceInLocal(
                info.thumbnail,
                info.getFileThumbnailFromFeaturedInfo(),
            )
        }
    }

    suspend fun refreshKeyFigures() {
        keyFigureRepository.onAppForeground()
    }

    fun getWalletState(): WalletState {
        var result = WalletState.NONE
        if (sharedPreferences.showSmartWallet && configurationManager.configuration.isSmartWalletOn) {
            smartWalletMap.value?.forEach { (_, certificate) ->
                val smartWalletState = getSmartWalletStateUseCase(certificate)
                val state = when (smartWalletState) {
                    is SmartWalletState.Expired -> WalletState.ALERT
                    is SmartWalletState.ExpireSoon -> WalletState.WARNING
                    is SmartWalletState.Eligible -> WalletState.ELIGIBLE
                    is SmartWalletState.EligibleSoon -> WalletState.ELIGIBLE_SOON
                    is SmartWalletState.Valid -> WalletState.NONE
                }
                // Take highest state
                if (result.ordinal < state.ordinal) {
                    result = state
                }
            }
        }
        return result
    }

    suspend fun refreshNews() {
        refreshInfoCenterUseCase()
    }

    fun getCompareKeyFigures(): Pair<KeyFigure, KeyFigure>? = getCompareKeyFiguresUseCase(
        keyFigures.value?.data.orEmpty(),
    )

    companion object {
        private const val NUMBER_NEWS_DISPLAYED: Int = 4
    }
}

class HomeViewModelFactory(
    private val configurationManager: ConfigurationManager,
    private val walletRepository: WalletRepository,
    private val sharedPreferences: SharedPreferences,
    private val getSmartWalletMapUseCase: GetSmartWalletMapUseCase,
    private val getSmartWalletStateUseCase: GetSmartWalletStateUseCase,
    private val keyFigureRepository: KeyFigureRepository,
    private val infoCenterRepository: InfoCenterRepository,
    private val refreshInfoCenterUseCase: RefreshInfoCenterUseCase,
    private val getCompareKeyFiguresUseCase: GetKeyFiguresWithCompareUseCase,
    private val getFavoriteKeyFiguresUseCase: GetFavoriteKeyFiguresUseCase,
    private val keyFigureMapRepository: KeyFigureMapRepository,
    private val featuredInfoRepository: FeaturedInfoRepository,
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return HomeViewModel(
            configurationManager,
            walletRepository,
            sharedPreferences,
            getSmartWalletMapUseCase,
            getSmartWalletStateUseCase,
            keyFigureRepository,
            keyFigureMapRepository,
            infoCenterRepository,
            refreshInfoCenterUseCase,
            getCompareKeyFiguresUseCase,
            getFavoriteKeyFiguresUseCase,
            featuredInfoRepository,
        ) as T
    }
}
