/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/2/28 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.lunabeestudio.domain.model.InfoCenterData
import com.lunabeestudio.domain.repository.InfoCenterRepository
import com.lunabeestudio.stopcovid.usecase.RefreshInfoCenterUseCase

class InfoCenterViewModel(
    infoCenterRepository: InfoCenterRepository,
    private val refreshInfoCenterUseCase: RefreshInfoCenterUseCase,
) : ViewModel() {
    val infoCenterData: LiveData<InfoCenterData> = infoCenterRepository.getDetailInfoCenterData()
        .asLiveData(viewModelScope.coroutineContext)

    suspend fun refreshInfoCenter() {
        refreshInfoCenterUseCase()
    }
}

class InfoCenterViewModelFactory(
    private val infoCenterRepository: InfoCenterRepository,
    private val refreshInfoCenterUseCase: RefreshInfoCenterUseCase,
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return InfoCenterViewModel(
            infoCenterRepository = infoCenterRepository,
            refreshInfoCenterUseCase = refreshInfoCenterUseCase,
        ) as T
    }
}
