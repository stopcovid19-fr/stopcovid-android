/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2021/24/6 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.lunabeestudio.domain.model.WalletCertificateType
import com.lunabeestudio.domain.model.smartwallet.SmartWalletValidity
import com.lunabeestudio.stopcovid.core.utils.SingleLiveEvent
import com.lunabeestudio.stopcovid.extension.testResultIsNegative
import com.lunabeestudio.stopcovid.model.EuropeanCertificate
import com.lunabeestudio.stopcovid.repository.WalletRepository
import com.lunabeestudio.stopcovid.usecase.ComputeDccValidityUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DccValidityViewModel(
    private val walletRepository: WalletRepository,
    private val computeDccValidityUseCase: ComputeDccValidityUseCase,
    private val dispatcherIO: CoroutineDispatcher = Dispatchers.IO,
) : ViewModel() {

    suspend fun getValidity(certificateId: String): SmartWalletValidity? {
        return (walletRepository.getCertificateById(certificateId) as? EuropeanCertificate)?.let {
            computeDccValidityUseCase(it)
        }
    }

    val showWalletEvent: SingleLiveEvent<Unit> = SingleLiveEvent()

    fun addCertificateInFavorite(certificateId: String) {
        viewModelScope.launch(dispatcherIO) {
            (walletRepository.getCertificateById(certificateId) as? EuropeanCertificate)?.let {
                if (!it.isFavorite) {
                    walletRepository.toggleFavorite(it)
                }
            }
            showWalletEvent.postValue(Unit)
        }
    }

    suspend fun shouldShowKonfetti(certificateId: String): Boolean {
        return (walletRepository.getCertificateById(certificateId) as? EuropeanCertificate)?.let { europeanCertificate ->
            europeanCertificate.type == WalletCertificateType.VACCINATION_EUROPE ||
                (europeanCertificate.greenCertificate.testResultIsNegative == true)
        } ?: false
    }
}

class DccValidityViewModelFactory(
    private val walletRepository: WalletRepository,
    private val computeDccValidityUseCase: ComputeDccValidityUseCase,
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return DccValidityViewModel(
            walletRepository,
            computeDccValidityUseCase,
        ) as T
    }
}
