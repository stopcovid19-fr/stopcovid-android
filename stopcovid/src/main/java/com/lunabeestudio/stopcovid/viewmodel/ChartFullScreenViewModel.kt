/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/1/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.repository.KeyFigureRepository
import com.lunabeestudio.stopcovid.model.ChartFullScreenData

class ChartFullScreenViewModel(
    private val keyFigureRepository: KeyFigureRepository,
) : ViewModel() {
    val chartData: MutableLiveData<ChartFullScreenData> = MutableLiveData()

    var keyFigure: KeyFigure? = null
    var keyFigure2: KeyFigure? = null

    suspend fun setKeyFiguresWithLabel() {
        keyFigure = chartData.value?.keyFigureKey?.let { keyFigureRepository.keyFigureWithDepartmentByLabel(it).data }
        keyFigure2 = chartData.value?.keyFigureKey2?.let { keyFigureRepository.keyFigureWithDepartmentByLabel(it).data }
    }
}

class ChartFullScreenViewModelFactory(
    private val keyFigureRepository: KeyFigureRepository,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return ChartFullScreenViewModel(
            keyFigureRepository,
        ) as T
    }
}
