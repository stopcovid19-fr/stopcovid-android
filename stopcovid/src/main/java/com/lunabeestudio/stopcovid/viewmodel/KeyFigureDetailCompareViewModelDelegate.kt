/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/01/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import com.lunabeestudio.domain.model.FigureType
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.model.KeyFigureSeriesItem
import com.lunabeestudio.stopcovid.core.manager.LocalizedStrings
import com.lunabeestudio.stopcovid.extension.setScaleToOneDecimal
import com.lunabeestudio.stopcovid.extension.shortUnitStringKey
import com.lunabeestudio.stopcovid.model.KeyFigureComparisonData
import com.lunabeestudio.stopcovid.model.KeyFigureComparisonDescriptionData
import com.lunabeestudio.stopcovid.model.KeyFigureComparisonRowData
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.Date

interface KeyFigureDetailCompareViewModelDelegate {
    fun generateComparisonDataFromKeyFigure(keyFigure: KeyFigure, strings: LocalizedStrings)
    val keyFigureComparisonData: StateFlow<KeyFigureComparisonData?>
}

class KeyFigureDetailCompareViewModelDelegateImpl : KeyFigureDetailCompareViewModelDelegate {

    private val _keyFigureComparisonData: MutableStateFlow<KeyFigureComparisonData?> = MutableStateFlow(null)
    override val keyFigureComparisonData: StateFlow<KeyFigureComparisonData?> = _keyFigureComparisonData.asStateFlow()

    override fun generateComparisonDataFromKeyFigure(keyFigure: KeyFigure, strings: LocalizedStrings) {
        _keyFigureComparisonData.value = keyFigure.figureType?.let { figureType ->
            keyFigure.series?.let { nationalSeries ->
                val dayMinusSeven = getLastWeekItem(nationalSeries)
                val evoPercent = dayMinusSeven?.let { calculateEvolutionPercent(it.value, nationalSeries.last().value) }
                val minItem = nationalSeries.minBy { it.value.toFloat() }.value.let { value ->
                    nationalSeries.filter { it.value == value }
                }
                val maxItem = nationalSeries.maxBy { it.value.toFloat() }.value.let { value ->
                    nationalSeries.filter { it.value == value }
                }

                val descriptionData = when {
                    keyFigure.figureType == FigureType.R || dayMinusSeven == null -> null
                    keyFigure.figureType == FigureType.PERC -> KeyFigureComparisonDescriptionData.Percentage(
                        oldValue = formatValue(nationalSeries.last().value, keyFigure, strings),
                        percentEvolution = "$evoPercent%",
                        pointsDiff = (nationalSeries.last().value.toFloat() - dayMinusSeven.value.toFloat()).setScaleToOneDecimal()
                            .toString(),
                    )
                    else -> KeyFigureComparisonDescriptionData.Float(
                        oldValue = formatValue(nationalSeries.last().value, keyFigure, strings),
                        percentEvolution = "$evoPercent%",
                    )
                }

                KeyFigureComparisonData(
                    labelKeyFigure = keyFigure.labelKey,
                    descriptionData = descriptionData,
                    keyFigureComparisonDayMinusSeven = dayMinusSeven?.let {
                        KeyFigureComparisonRowData(
                            date = listOf(Date.from(Instant.ofEpochSecond(dayMinusSeven.date))),
                            value = formatValue(dayMinusSeven.value, keyFigure, strings),
                            labelRes = "keyFigureDetailController.section.comparison.dayMinusSeven.label",
                        )
                    },
                    keyFigureComparisonMinValue = KeyFigureComparisonRowData(
                        date = minItem.map { Date.from(Instant.ofEpochSecond(it.date)) },
                        value = formatValue(minItem.first().value, keyFigure, strings),
                        labelRes = "keyFigureDetailController.section.comparison.min.label",
                    ),
                    keyFigureComparisonMaxValue = KeyFigureComparisonRowData(
                        date = maxItem.map { Date.from(Instant.ofEpochSecond(it.date)) },
                        value = formatValue(maxItem.first().value, keyFigure, strings),
                        labelRes = "keyFigureDetailController.section.comparison.max.label",
                    ),
                )
            }
        }
    }

    private fun getLastWeekItem(nationalSeries: List<KeyFigureSeriesItem>): KeyFigureSeriesItem? {
        val lastDay = Instant.ofEpochSecond(nationalSeries.last().date)
        val lastWeekDay = lastDay.minus(7, ChronoUnit.DAYS)
        return nationalSeries.firstOrNull { it.date == lastWeekDay.epochSecond }
    }

    private fun calculateEvolutionPercent(oldValue: Number, lastValue: Number): Float {
        val diff = lastValue.toFloat() - oldValue.toFloat()
        return (diff / oldValue.toFloat() * 100).setScaleToOneDecimal()
    }

    private fun formatValue(number: Number, keyFigure: KeyFigure, strings: LocalizedStrings): String {
        val shortUnit = strings[keyFigure.shortUnitStringKey] ?: ""
        return "$number$shortUnit"
    }
}
