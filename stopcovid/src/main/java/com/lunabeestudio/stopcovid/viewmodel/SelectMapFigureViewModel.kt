/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/12/21 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.repository.KeyFigureMapRepository
import com.lunabeestudio.domain.repository.KeyFigureRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

class SelectMapFigureViewModel(
    private val keyFigureRepository: KeyFigureRepository,
    private val keyFigureMapRepository: KeyFigureMapRepository,
) : ViewModel() {

    private val _keyFigures: MutableStateFlow<List<KeyFigure>> = MutableStateFlow(listOf())
    val keyFigures: StateFlow<List<KeyFigure>> = _keyFigures

    fun fetchKeyFigures() {
        viewModelScope.launch {
            val labelKeyFiguresMapAvailable = keyFigureMapRepository.availableKeyFiguresFlow().first()
            _keyFigures.value = keyFigureRepository.allKeyFigures().data?.filter {
                labelKeyFiguresMapAvailable.contains(it.labelKey)
            } ?: listOf()
        }
    }
}

class SelectMapFigureViewModelFactory(
    private val keyFigureRepository: KeyFigureRepository,
    private val keyFigureMapRepository: KeyFigureMapRepository,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return SelectMapFigureViewModel(
            keyFigureRepository,
            keyFigureMapRepository,
        ) as T
    }
}
