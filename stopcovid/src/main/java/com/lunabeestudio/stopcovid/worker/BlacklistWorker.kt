/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2021/10/03 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.worker

import android.content.Context
import androidx.work.Constraints
import androidx.work.CoroutineWorker
import androidx.work.ExistingWorkPolicy
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.Operation
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.stopcovid.Constants
import com.lunabeestudio.stopcovid.TousAntiCovid

class BlacklistWorker(appContext: Context, params: WorkerParameters) : CoroutineWorker(appContext, params) {
    override suspend fun doWork(): Result {
        val certificateId = inputData.getString(CERTIFICATE_ID_WORKER_INPUT_DATA_KEY)
        val result = (applicationContext as TousAntiCovid).injectionContainer.updateBlacklistUseCase(
            skipIndexUpdate = certificateId != null,
            certificateId = certificateId,
        )
        return if (result is TacResult.Success) {
            Result.success()
        } else {
            Result.failure()
        }
    }

    companion object {
        private const val CERTIFICATE_ID_WORKER_INPUT_DATA_KEY: String = "CERTIFICATE_ID_WORKER_INPUT_DATA_KEY"

        fun start(context: Context, certificateId: String?): Operation {
            val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

            val workRequest = OneTimeWorkRequestBuilder<BlacklistWorker>()
                .addTag(Constants.WorkerTags.BLACKLIST)
                .setConstraints(constraints)
                .setInputData(
                    workDataOf(
                        CERTIFICATE_ID_WORKER_INPUT_DATA_KEY to certificateId,
                    ),
                )
                .build()

            return WorkManager.getInstance(context)
                .enqueueUniqueWork(Constants.WorkerNames.BLACKLIST, ExistingWorkPolicy.KEEP, workRequest)
        }
    }
}
