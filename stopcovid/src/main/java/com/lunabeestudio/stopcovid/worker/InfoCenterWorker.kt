/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/9 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.worker

import android.content.Context
import androidx.preference.PreferenceManager
import androidx.work.Constraints
import androidx.work.CoroutineWorker
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import com.lunabeestudio.stopcovid.Constants
import com.lunabeestudio.stopcovid.TousAntiCovid
import com.lunabeestudio.stopcovid.extension.lastInfoCenterRefresh
import java.util.concurrent.TimeUnit
import kotlin.random.Random

class InfoCenterWorker(appContext: Context, params: WorkerParameters) : CoroutineWorker(appContext, params) {
    override suspend fun doWork(): Result {
        val tousAntiCovid = applicationContext as TousAntiCovid
        val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)

        val oldLastUpdatedAt = sharedPrefs.lastInfoCenterRefresh

        val lastUpdatedAt = tousAntiCovid.injectionContainer.refreshInfoCenterUseCase.invoke()

        if (lastUpdatedAt != null && lastUpdatedAt > oldLastUpdatedAt) {
            tousAntiCovid.injectionContainer.sendInfoCenterNotificationUseCase()
        }

        return Result.success() // do not retry
    }

    companion object {
        fun schedule(context: Context) {
            val randomDelayHours = Random.nextLong(20, 28)

            val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

            val infoCenterWorkRequest = PeriodicWorkRequestBuilder<InfoCenterWorker>(24, TimeUnit.HOURS)
                .setConstraints(constraints)
                .setInitialDelay(randomDelayHours, TimeUnit.HOURS)
                .build()

            WorkManager.getInstance(context)
                .enqueueUniquePeriodicWork(
                    Constants.WorkerNames.INFO_CENTER,
                    ExistingPeriodicWorkPolicy.REPLACE,
                    infoCenterWorkRequest,
                )
        }

        fun runNow(context: Context) {
            val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()

            val infoCenterWorkRequest = OneTimeWorkRequestBuilder<InfoCenterWorker>()
                .setConstraints(constraints)
                .build()

            WorkManager.getInstance(context).enqueue(infoCenterWorkRequest)
        }
    }
}
