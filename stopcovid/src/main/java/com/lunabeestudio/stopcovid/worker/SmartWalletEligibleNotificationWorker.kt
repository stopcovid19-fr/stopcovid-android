/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2021/4/10 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.worker

import android.content.Context
import androidx.preference.PreferenceManager
import androidx.work.CoroutineWorker
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.Operation
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.lunabeestudio.stopcovid.Constants
import com.lunabeestudio.stopcovid.TousAntiCovid
import com.lunabeestudio.stopcovid.extension.configurationDataSource
import com.lunabeestudio.stopcovid.extension.notificationSent
import java.util.concurrent.TimeUnit

class SmartWalletEligibleNotificationWorker(appContext: Context, params: WorkerParameters) : CoroutineWorker(
    appContext,
    params,
) {
    override suspend fun doWork(): Result {
        val notificationSentId = inputData.getString(NOTIFICATION_SENT_ID_WORKER_INPUT_DATA_KEY)
        val startTimeMs = inputData.getLong(START_TIME_MS_INPUT_DATA_KEY, System.currentTimeMillis())
        val notifElgMs = (applicationContext as TousAntiCovid).configurationDataSource().configuration.notifElgDelay.inWholeMilliseconds
        val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)

        if (System.currentTimeMillis() > startTimeMs + notifElgMs) {
            val newNotificationSent = sharedPrefs.notificationSent.toMutableSet()
            val notificationIdRemoved = newNotificationSent.remove(notificationSentId)

            if (notificationIdRemoved) {
                // reset notification sent
                sharedPrefs.notificationSent = newNotificationSent
                // trigger notif algo
                (applicationContext as TousAntiCovid).injectionContainer.smartWalletNotificationUseCase(
                    applicationContext,
                )
            }

            // Remove self and let SmartWalletNotificationUseCase re-add it with new startTime
            WorkManager.getInstance(applicationContext).cancelWorkById(id)
        } else if (!sharedPrefs.notificationSent.contains(notificationSentId)) {
            WorkManager.getInstance(applicationContext).cancelWorkById(id)
        }

        return Result.success()
    }

    companion object {
        private const val NOTIFICATION_SENT_ID_WORKER_INPUT_DATA_KEY: String = "NOTIFICATION_SENT_ID_WORKER_INPUT_DATA_KEY"
        private const val START_TIME_MS_INPUT_DATA_KEY: String = "START_TIME_MS_INPUT_DATA_KEY"

        fun start(
            context: Context,
            notificationSentId: String,
        ): Operation {
            val startTimeMs = System.currentTimeMillis()

            val repeatInterval = Constants.WorkerPeriodicTime.SMART_WALLET_ELIGIBLE_NOTIFICATION_DAY
            val repeatIntervalTimeUnit = TimeUnit.DAYS

            val workRequest = PeriodicWorkRequestBuilder<SmartWalletEligibleNotificationWorker>(
                repeatInterval,
                repeatIntervalTimeUnit,
            )
                .setInitialDelay(repeatInterval, repeatIntervalTimeUnit)
                .setInputData(
                    workDataOf(
                        NOTIFICATION_SENT_ID_WORKER_INPUT_DATA_KEY to notificationSentId,
                        START_TIME_MS_INPUT_DATA_KEY to startTimeMs,
                    ),
                )
                .addTag(Constants.WorkerTags.SMART_WALLET)
                .build()

            val workerId = Constants.WorkerNames.SMART_WALLET_ELIGIBLE_NOTIFICATION + ".$notificationSentId.$startTimeMs"

            return WorkManager.getInstance(context)
                .enqueueUniquePeriodicWork(
                    workerId,
                    ExistingPeriodicWorkPolicy.REPLACE,
                    workRequest,
                )
        }
    }
}
