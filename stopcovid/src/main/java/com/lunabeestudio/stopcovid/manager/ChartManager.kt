/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.manager

import timber.log.Timber
import kotlin.time.Duration
import kotlin.time.Duration.Companion.INFINITE
import kotlin.time.Duration.Companion.days

object ChartManager {

    enum class ChartRange(val labelKey: String, val rangeDuration: Duration) {
        THIRTY("keyFigureDetailController.chartRange.segmentTitle.30", 30.days),
        NINETY("keyFigureDetailController.chartRange.segmentTitle.90", 90.days),
        ALL("keyFigureDetailController.chartRange.segmentTitle.1000", INFINITE),
    }

    fun getChartRange(position: Int, itemCount: Int): ChartRange? = when {
        position == 0 && itemCount == 1 -> ChartRange.ALL
        position == 0 && itemCount == 2 -> ChartRange.NINETY
        position == 1 && itemCount == 2 -> ChartRange.THIRTY
        position == 0 && itemCount == 3 -> ChartRange.ALL
        position == 1 && itemCount == 3 -> ChartRange.NINETY
        position == 2 && itemCount == 3 -> ChartRange.THIRTY
        else -> {
            Timber.e("Unexpected range at $position with $itemCount items")
            null
        }
    }

    fun getItemCount(diffDuration: Duration): Int = when (diffDuration) {
        in Duration.ZERO..(ChartRange.THIRTY.rangeDuration + 1.days) -> 1
        in (ChartRange.THIRTY.rangeDuration + 1.days)..(ChartRange.NINETY.rangeDuration + 1.days) -> 2
        else -> 3
    }
}
