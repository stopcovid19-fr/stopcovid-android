/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/01/02 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fastitem

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.view.isVisible
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.extension.setImageFileIfValid
import com.lunabeestudio.stopcovid.databinding.ItemHomeFeaturedBinding
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import java.io.File

sealed interface ItemWeight {
    object MatchParent : ItemWeight
    data class Weight(val value: Float) : ItemWeight
}

class HomeFeaturedItem : AbstractBindingItem<ItemHomeFeaturedBinding>() {
    override val type: Int = R.id.home_featured_item

    var thumbnailFile: File? = null
    var onClick: View.OnClickListener? = null
    var title: String? = null
    var refreshButtonText: String? = null
    var weight: ItemWeight = ItemWeight.MatchParent
    var refreshButtonOnClick: View.OnClickListener? = null

    var binding: ItemHomeFeaturedBinding? = null

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ItemHomeFeaturedBinding {
        return ItemHomeFeaturedBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: ItemHomeFeaturedBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        this.binding = binding

        val spacingLargeInPx = binding.root.context.resources.getDimensionPixelSize(R.dimen.spacing_large)
        val spacingSmallInPx = binding.root.context.resources.getDimensionPixelSize(R.dimen.spacing_small)

        binding.root.setOnClickListener(onClick)
        binding.refreshButton.setOnClickListener(refreshButtonOnClick)
        binding.titleTextView.text = title
        binding.refreshButton.text = refreshButtonText
        val displayMetrics = binding.root.context.resources.displayMetrics

        val params = weight.let { safeWeight ->
            when (safeWeight) {
                is ItemWeight.Weight -> {
                    LinearLayout.LayoutParams(
                        (displayMetrics.widthPixels * safeWeight.value).toInt(),
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                    ).also {
                        it.setMargins(0, spacingSmallInPx, 0, spacingLargeInPx)
                    }
                }
                is ItemWeight.MatchParent -> {
                    LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                    ).also {
                        it.setMargins(spacingLargeInPx, spacingSmallInPx, spacingLargeInPx, spacingLargeInPx)
                    }
                }
            }
        }
        binding.root.layoutParams = params
        tryToShowData()
    }

    fun showLoading() {
        binding?.thumbnailImageView?.isVisible = false
        binding?.refreshButton?.isVisible = false
        binding?.progressBar?.isVisible = true
    }

    private fun tryToShowData() {
        thumbnailFile?.let {
            if (binding?.thumbnailImageView?.setImageFileIfValid(it) == true) {
                binding?.thumbnailImageView?.isVisible = true
                binding?.progressBar?.isVisible = false
                binding?.refreshButton?.isVisible = false
            } else {
                showEmpty()
            }
        }
    }

    private fun showEmpty() {
        binding?.thumbnailImageView?.isVisible = false
        binding?.refreshButton?.isVisible = true
        binding?.progressBar?.isVisible = false
    }

    override fun unbindView(binding: ItemHomeFeaturedBinding) {
        binding.root.setOnClickListener(null)
        binding.refreshButton.setOnClickListener(null)
        this.binding = null
        super.unbindView(binding)
    }
}

fun homeFeaturedItem(block: (HomeFeaturedItem.() -> Unit)): HomeFeaturedItem = HomeFeaturedItem().apply(
    block,
)
