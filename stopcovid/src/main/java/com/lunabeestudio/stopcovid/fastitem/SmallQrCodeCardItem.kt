/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fastitem

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.ViewGroup
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.extension.setTextOrHide
import com.lunabeestudio.stopcovid.databinding.ItemSmallQrCodeCardBinding
import com.mikepenz.fastadapter.binding.AbstractBindingItem

class SmallQrCodeCardItem : AbstractBindingItem<ItemSmallQrCodeCardBinding>() {

    override val type: Int = R.id.item_small_qr_code_card

    var generateBarcode: (() -> Bitmap)? = null
    var title: String? = null
    var body: String? = null
    var onClick: (() -> Unit)? = null

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ItemSmallQrCodeCardBinding {
        return ItemSmallQrCodeCardBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: ItemSmallQrCodeCardBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)
        binding.titleTextView.setTextOrHide(title)
        binding.bodyTextView.setTextOrHide(body)

        val bitmap = generateBarcode?.invoke()
        binding.qrCodeImageView.setImageBitmap(bitmap)

        binding.container.setOnClickListener {
            onClick?.invoke()
        }
    }
}

fun smallQrCodeCardItem(block: (SmallQrCodeCardItem.() -> Unit)): SmallQrCodeCardItem = SmallQrCodeCardItem().apply(
    block,
)
