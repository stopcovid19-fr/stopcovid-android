/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2021/20/5 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fastitem

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.extension.setTextOrHide
import com.lunabeestudio.stopcovid.databinding.ItemReorderBinding
import com.mikepenz.fastadapter.binding.AbstractBindingItem
import com.mikepenz.fastadapter.drag.IDraggable
import timber.log.Timber

class ReorderItem : AbstractBindingItem<ItemReorderBinding>(), IDraggable {
    override val type: Int = R.id.item_reorder
    override val isDraggable: Boolean = true

    var title: String? = null
    var subtitle: String? = null
    var isAccessibilityOn: Boolean = false

    var isFirst: Boolean = false
    var isLast: Boolean = false
    var onUpClick: (() -> Unit)? = null
    var upContentDescription: String? = null
    var upAfterClickContentDescription: String? = null
    var onDownClick: (() -> Unit)? = null
    var downContentDescription: String? = null
    var downAfterClickContentDescription: String? = null

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ItemReorderBinding {
        return ItemReorderBinding.inflate(inflater, parent, false)
    }

    override fun bindView(binding: ItemReorderBinding, payloads: List<Any>) {
        super.bindView(binding, payloads)

        binding.titleTextView.setTextOrHide(title)
        binding.subtitleTextView.setTextOrHide(subtitle)
        binding.upImageView.setOnClickListener {
            onUpClick?.invoke()
            it.announceForAccessibility(upAfterClickContentDescription)
        }
        binding.upImageView.contentDescription = upContentDescription
        binding.downImageView.setOnClickListener {
            onDownClick?.invoke()
            it.announceForAccessibility(downAfterClickContentDescription)
        }
        binding.downImageView.contentDescription = downContentDescription

        binding.upImageView.isVisible = isAccessibilityOn && !isFirst
        binding.downImageView.isVisible = isAccessibilityOn && !isLast
        binding.dragImageView.isVisible = !isAccessibilityOn
        Timber.e("isAccessibilityOn = $isAccessibilityOn")
    }

    override fun unbindView(binding: ItemReorderBinding) {
        super.unbindView(binding)
        binding.upImageView.setOnClickListener(null)
        binding.downImageView.setOnClickListener(null)
        binding.upImageView.isVisible = false
        binding.downImageView.isVisible = false
        binding.dragImageView.isVisible = true
    }
}

fun reorderItem(block: (ReorderItem.() -> Unit)): ReorderItem = ReorderItem().apply(block)
