/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.activity

import android.content.Intent
import android.os.Bundle
import android.view.HapticFeedbackConstants
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowInsetsControllerCompat
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.github.razir.progressbutton.DrawableButton
import com.github.razir.progressbutton.bindProgressButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.google.gson.Gson
import com.lunabeestudio.common.ConfigConstant
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.LocalizedApplication
import com.lunabeestudio.stopcovid.core.extension.fetchSystemColor
import com.lunabeestudio.stopcovid.core.extension.isNightMode
import com.lunabeestudio.stopcovid.core.extension.setTextOrHide
import com.lunabeestudio.stopcovid.core.manager.LocalizedStrings
import com.lunabeestudio.stopcovid.databinding.ActivityAppMaintenanceBinding
import com.lunabeestudio.stopcovid.extension.emitDefaultKonfetti
import com.lunabeestudio.stopcovid.extension.getBuildNumber
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.openInExternalBrowser
import com.lunabeestudio.stopcovid.manager.AppMaintenanceManager
import com.lunabeestudio.stopcovid.model.Info
import kotlinx.coroutines.launch
import nl.dionsegijn.konfetti.ParticleSystem

/**
 * The blocking activity
 */
class AppMaintenanceActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAppMaintenanceBinding
    private var konfettis: ParticleSystem? = null

    private val strings: LocalizedStrings by lazy {
        (application as? LocalizedApplication)?.localizedStrings ?: emptyMap()
    }

    private var info: Info? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityAppMaintenanceBinding.inflate(layoutInflater)
        setContentView(binding.root)
        WindowInsetsControllerCompat(window, window.decorView).isAppearanceLightStatusBars = !isNightMode()
        info = Gson().fromJson(intent.getStringExtra(EXTRA_INFO), Info::class.java)
        fillScreen()
        bindProgressButton(binding.refreshButton)

        onBackPressedDispatcher.addCallback(
            this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    finish()
                    if (canSkipForceUpgrade(info) && info?.appAvailability != false) {
                        navToApp()
                    }
                }
            },
        )
    }

    override fun onResume() {
        super.onResume()
        if (info?.shouldPopConfettis == true) {
            if (binding.konfettiView.width == 0) {
                val onLayoutChangeListener = object : View.OnLayoutChangeListener {
                    override fun onLayoutChange(p0: View?, p1: Int, p2: Int, p3: Int, p4: Int, p5: Int, p6: Int, p7: Int, p8: Int) {
                        binding.konfettiView.removeOnLayoutChangeListener(this)
                        emitKonfetti()
                    }
                }
                binding.konfettiView.addOnLayoutChangeListener(onLayoutChangeListener)
            } else {
                emitKonfetti()
            }
        }
    }

    private fun emitKonfetti() {
        binding.root.performHapticFeedback(
            HapticFeedbackConstants.LONG_PRESS,
            HapticFeedbackConstants.FLAG_IGNORE_VIEW_SETTING,
        )
        konfettis?.stop()
        konfettis = binding.konfettiView.emitDefaultKonfetti(binding)
    }

    private fun canSkipForceUpgrade(info: Info?): Boolean {
        val buildNumber = getBuildNumber()
        val minForcedBuildNumber = (info?.minForcedBuildNumber ?: 0)
        return buildNumber >= minForcedBuildNumber
    }

    /**
     * Fill the screen with
     */
    private fun fillScreen() {
        val info = this.info ?: return

        binding.titleTextView.setTextOrHide(info.getTitle(this))
        binding.messageTextView.setTextOrHide(info.getMessage(this))
        if (info.getButtonTitle(this) != null && info.getButtonUrl(this) != null) {
            binding.button.text = info.getButtonTitle(this)
            binding.button.setOnClickListener {
                startOpenInStore()
            }
            binding.button.visibility = View.VISIBLE
        } else {
            binding.button.visibility = View.GONE
        }
        if (canSkipForceUpgrade(info)) {
            binding.skipButton.setTextOrHide(strings["appMaintenanceController.later.button.title"])
            binding.skipButton.setOnClickListener {
                navToApp()
            }
        } else {
            binding.skipButton.isVisible = false
        }
        when (info.mode) {
            Info.Mode.MAINTENANCE -> {
                binding.imageView.setImageResource(AppMaintenanceManager.maintenanceIconRes)
                binding.imageView.visibility = View.VISIBLE
            }
            Info.Mode.UPGRADE -> {
                binding.imageView.setImageResource(AppMaintenanceManager.upgradeIconRes)
                binding.imageView.visibility = View.VISIBLE
            }
            Info.Mode.DISABLED -> {
                binding.imageView.setImageResource(AppMaintenanceManager.maintenanceIconRes)
                binding.imageView.visibility = View.VISIBLE
                binding.skipButton.visibility = View.GONE
            }
            null -> {
                binding.imageView.visibility = View.GONE
            }
        }
        binding.refreshButton.isVisible = info.mode == Info.Mode.MAINTENANCE
        binding.refreshButton.text = strings["common.tryAgain"]
        binding.refreshButton.setOnClickListener {
            binding.refreshButton.showProgress {
                progressColor = R.attr.colorOnPrimary.fetchSystemColor(this@AppMaintenanceActivity)
                gravity = DrawableButton.GRAVITY_CENTER
            }
            lifecycleScope.launch {
                AppMaintenanceManager.updateCheckForMaintenanceUpgrade(
                    this@AppMaintenanceActivity,
                    injectionContainer.okHttpClient,
                    appIsFreeCompletion = {
                        navToApp()
                    },
                    appIsBlockedCompletion = { info ->
                        this@AppMaintenanceActivity.info = info
                        binding.refreshButton.hideProgress(strings["common.tryAgain"])
                        binding.swipeRefreshLayout.isRefreshing = false
                        fillScreen()
                    },
                )
            }
        }
        binding.swipeRefreshLayout.setOnRefreshListener {
            lifecycleScope.launch {
                AppMaintenanceManager.updateCheckForMaintenanceUpgrade(
                    this@AppMaintenanceActivity,
                    injectionContainer.okHttpClient,
                    appIsFreeCompletion = {
                        navToApp()
                    },
                    appIsBlockedCompletion = { info ->
                        this@AppMaintenanceActivity.info = info
                        binding.swipeRefreshLayout.isRefreshing = false
                        fillScreen()
                    },
                )
            }
        }
    }

    private fun navToApp() {
        startActivity(Intent(this@AppMaintenanceActivity, SplashScreenActivity::class.java))
        finishAndRemoveTask()
    }

    private fun startOpenInStore() {
        if (!ConfigConstant.Store.GOOGLE.openInExternalBrowser(this, false)) {
            if (!ConfigConstant.Store.HUAWEI.openInExternalBrowser(this, false)) {
                ConfigConstant.Store.TAC_WEBSITE.openInExternalBrowser(this)
            }
        }
    }

    companion object {
        const val EXTRA_INFO: String = "extra.info"
    }
}
