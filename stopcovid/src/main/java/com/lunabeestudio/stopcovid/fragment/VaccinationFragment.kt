/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2021/01/10 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.util.LayoutDirection
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.fastitem.cardWithActionItem
import com.lunabeestudio.stopcovid.core.fastitem.spaceItem
import com.lunabeestudio.stopcovid.core.model.CardTheme
import com.lunabeestudio.stopcovid.extension.openInExternalBrowser
import com.mikepenz.fastadapter.GenericItem

class VaccinationFragment : MainFragment() {

    override fun getTitleKey(): String = "vaccinationController.title"

    override suspend fun getItems(): List<GenericItem> {
        val items = ArrayList<GenericItem>()

        items += spaceItem {
            spaceRes = R.dimen.spacing_large
            identifier = items.size.toLong()
        }

        items += cardWithActionItem(CardTheme.Primary) {
            mainTitle = strings["vaccinationController.location.title"]
            mainBody = strings["vaccinationController.location.subtitle"]
            linkifyMainBody = false
            mainImage = R.drawable.ic_vaccine_center2
            mainLayoutDirection = LayoutDirection.RTL
            identifier = "vaccinationController.location.title".hashCode().toLong()
            onCardClick = {
                context?.let { _context ->
                    strings["vaccinationController.vaccinationLocation.url"]?.openInExternalBrowser(_context)
                }
            }
        }

        items += spaceItem {
            spaceRes = R.dimen.spacing_large
            identifier = items.count().toLong()
        }

        items += cardWithActionItem(CardTheme.Default) {
            mainTitle = strings["vaccinationController.eligibility.title"]
            mainBody = strings["vaccinationController.eligibility.subtitle"]
            identifier = "vaccinationController.eligibility.title".hashCode().toLong()
        }

        items += spaceItem {
            spaceRes = R.dimen.spacing_large
            identifier = items.count().toLong()
        }

        return items
    }
}
