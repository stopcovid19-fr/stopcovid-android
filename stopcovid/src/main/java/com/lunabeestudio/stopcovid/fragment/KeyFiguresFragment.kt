/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.core.text.toSpannable
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import com.lunabeestudio.common.extension.chosenPostalCode
import com.lunabeestudio.domain.model.DepartmentKeyFigure
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.error.AppError
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.activity.MainActivity
import com.lunabeestudio.stopcovid.core.UiConstants
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.extension.getApplicationLocale
import com.lunabeestudio.stopcovid.core.extension.viewLifecycleOwnerOrNull
import com.lunabeestudio.stopcovid.core.fastitem.captionItem
import com.lunabeestudio.stopcovid.core.fastitem.cardWithActionItem
import com.lunabeestudio.stopcovid.core.fastitem.secondaryButtonItem
import com.lunabeestudio.stopcovid.core.fastitem.spaceItem
import com.lunabeestudio.stopcovid.core.model.Action
import com.lunabeestudio.stopcovid.extension.getKeyFigureForPostalCode
import com.lunabeestudio.stopcovid.extension.getSerializableCompat
import com.lunabeestudio.stopcovid.extension.getString
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.itemForFigure
import com.lunabeestudio.stopcovid.extension.labelStringKey
import com.lunabeestudio.stopcovid.extension.safeNavigate
import com.lunabeestudio.stopcovid.extension.showErrorSnackBar
import com.lunabeestudio.stopcovid.extension.transformHeartEmoji
import com.lunabeestudio.stopcovid.fastitem.KeyFigureCardItem
import com.lunabeestudio.stopcovid.fastitem.bigTitleItem
import com.lunabeestudio.stopcovid.fastitem.explanationActionCardItem
import com.lunabeestudio.stopcovid.fastitem.linkItem
import com.lunabeestudio.stopcovid.manager.ShareManager
import com.lunabeestudio.stopcovid.model.KeyFigureFragmentState
import com.lunabeestudio.stopcovid.utils.lazyFast
import com.lunabeestudio.stopcovid.viewmodel.KeyFiguresViewModel
import com.lunabeestudio.stopcovid.viewmodel.KeyFiguresViewModelFactory
import com.mikepenz.fastadapter.GenericItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.NumberFormat
import java.text.SimpleDateFormat

class KeyFiguresFragment : MainFragment() {

    private var state: KeyFigureFragmentState = KeyFigureFragmentState.ALL

    private val locale by lazyFast { context.getApplicationLocale() }
    private val numberFormat: NumberFormat by lazyFast { NumberFormat.getNumberInstance(locale) }
    private val dateFormat by lazyFast { SimpleDateFormat(UiConstants.DAY_MONTH_DATE_PATTERN, locale) }

    private val viewModel: KeyFiguresViewModel by viewModels {
        KeyFiguresViewModelFactory(
            injectionContainer.keyFigureRepository,
            injectionContainer.getFavoriteKeyFiguresUseCase,
        )
    }

    private val sharedPrefs: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(requireContext())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        state = arguments?.getSerializableCompat(STATE_ARG_KEY) ?: state
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.figures.observe(viewLifecycleOwner) {
            refreshScreen()
        }
        viewModel.favFigures.observe(viewLifecycleOwner) {
            refreshScreen()
        }
    }

    override fun refreshScreen() {
        super.refreshScreen()
        (activity as? MainActivity)?.binding?.tabLayout?.isVisible = true
    }

    override suspend fun getItems(): List<GenericItem> {
        val items = mutableListOf<GenericItem>()
        addInfoSectionWarningIfNeeded(items)
        addFetchErrorItemIfNeeded(items)
        addExplanationItemIfNeeded(items)
        addKeyFiguresItems(items)
        addReorderButtonIfNeeded(items)
        return items
    }

    private fun addReorderButtonIfNeeded(items: MutableList<GenericItem>) {
        if (state == KeyFigureFragmentState.FAVORITE && (viewModel.favFigures.value?.size ?: 0) > 1) {
            items += secondaryButtonItem {
                text = strings["keyfigures.reorder.button.title"]
                identifier = "keyfigures.reorder.button.title".hashCode().toLong()
                width = ViewGroup.LayoutParams.MATCH_PARENT
                onClickListener = View.OnClickListener {
                    parentFragment?.findNavControllerOrNull()?.safeNavigate(
                        KeyFiguresPagerFragmentDirections.actionKeyFiguresPagerFragmentToReorderKeyFiguresFragment(),
                    )
                }
            }
            items += spaceItem {
                spaceRes = R.dimen.spacing_large
                identifier = items.count().toLong()
            }
        }
    }

    private fun addInfoSectionWarningIfNeeded(items: MutableList<GenericItem>) {
        if (!strings["home.infoSection.warning"].isNullOrBlank()) {
            items += spaceItem {
                spaceRes = R.dimen.spacing_medium
                identifier = items.count().toLong()
            }
            items += cardWithActionItem {
                mainBody = strings["home.infoSection.warning"]
                mainMaxLines = 3
                onCardClick = {
                    findNavControllerOrNull()
                        ?.safeNavigate(
                            KeyFiguresPagerFragmentDirections.actionKeyFiguresPagerFragmentToInfoSectionWarningBottomSheetFragment(),
                        )
                }
                identifier = "home.infoSection.warning".hashCode().toLong()
            }
        }
    }

    private fun addFetchErrorItemIfNeeded(items: MutableList<GenericItem>) {
        viewModel.figures.value.let { keyFiguresResult ->
            val error = (keyFiguresResult as? TacResult.Failure)?.throwable
            if (error is AppError && error.code == AppError.Code.KEY_FIGURES_NOT_AVAILABLE) {
                items += spaceItem {
                    spaceRes = R.dimen.spacing_large
                    identifier = items.count().toLong()
                }

                items += explanationActionCardItem {
                    explanation = error.getString(strings)
                    bottomText = strings["keyFiguresController.fetchError.button"]
                    onClick = {
                        viewLifecycleOwnerOrNull()?.lifecycleScope?.launch {
                            (activity as? MainActivity)?.showProgress(true)
                            injectionContainer.keyFigureRepository.onAppForeground()
                            (activity as? MainActivity)?.showProgress(false)
                            refreshScreen()
                        }
                    }
                    identifier = "keyFiguresController.fetchError.button".hashCode().toLong()
                }
            }
        }
    }

    private fun addExplanationItemIfNeeded(items: MutableList<GenericItem>) {
        val explanations = strings["keyFiguresController.explanations.text"]
        val explanationsLink = strings["keyFiguresController.explanations.button"]
        if (!explanations.isNullOrBlank() || !explanationsLink.isNullOrBlank()) {
            items += spaceItem {
                spaceRes = R.dimen.spacing_large
                identifier = items.count().toLong()
            }
        }
        if (!explanations.isNullOrBlank()) {
            items += captionItem {
                text = explanations
                identifier = text.hashCode().toLong()
            }
        }
        if (!explanationsLink.isNullOrBlank()) {
            items += linkItem {
                text = explanationsLink
                onClickListener = View.OnClickListener {
                    findNavControllerOrNull()
                        ?.safeNavigate(
                            KeyFiguresPagerFragmentDirections.actionKeyFiguresPagerFragmentToMoreKeyFigureFragment(),
                        )
                }
                identifier = text.hashCode().toLong()
            }
        }
        if (explanations.isNullOrBlank() && explanationsLink.isNullOrBlank()) {
            items += spaceItem {
                spaceRes = R.dimen.spacing_large
                identifier = items.count().toLong()
            }
        }
    }

    private fun addKeyFiguresItems(items: MutableList<GenericItem>) {
        val keyFigures = viewModel.figures.value?.data
        val favoriteKeyFigures = viewModel.favFigures.value
        if (!keyFigures.isNullOrEmpty()) {
            when (state) {
                KeyFigureFragmentState.FAVORITE -> {
                    items += spaceItem {
                        spaceRes = R.dimen.spacing_medium
                        identifier = items.count().toLong()
                    }
                    if (favoriteKeyFigures?.isEmpty() == true) {
                        items += cardWithActionItem {
                            mainTitle = strings["keyfigures.noFavorite.cell.title"]
                            val spannedSubtitle = strings["keyFiguresController.noFavorite.cell.subtitle"]?.toSpannable()
                            context?.let { spannedSubtitle?.transformHeartEmoji(it) }
                            mainBody = spannedSubtitle
                            actions = listOf(
                                Action(
                                    label = strings["keyFiguresController.resetFavorite.button"],
                                    onClickListener = {
                                        lifecycleScope.launch {
                                            injectionContainer.keyFigureRepository.restoreDefaultFavorites()
                                        }
                                    },
                                ),
                            )
                            identifier = "keyfigures.noFavorite.cell.title".hashCode().toLong()
                        }
                    } else {
                        favoriteKeyFigures?.mapNotNullTo(items) {
                            val departmentKeyFigure = it.getKeyFigureForPostalCode(sharedPrefs.chosenPostalCode, configurationManager)
                            itemForFigure(it, departmentKeyFigure)
                        }
                    }
                }
                KeyFigureFragmentState.ALL -> {
                    keyFigures.groupBy { it.category }.forEach {
                        displayItemByCategory(items, it.value, it.key)
                    }
                }
            }

            items += spaceItem {
                spaceRes = R.dimen.spacing_large
                identifier = items.count().toLong()
            }
        }
    }

    private fun displayItemByCategory(items: MutableList<GenericItem>, figures: List<KeyFigure>, category: String) {
        strings["keyFiguresController.category.$category"]?.let { nameCategory ->
            items += spaceItem {
                spaceRes = R.dimen.spacing_medium
                identifier = items.count().toLong()
            }
            items += bigTitleItem {
                text = nameCategory
                identifier = "keyFiguresController.category.$category".hashCode().toLong()
            }
            figures.mapNotNullTo(items) {
                val departmentKeyFigure = it.getKeyFigureForPostalCode(sharedPrefs.chosenPostalCode, configurationManager)
                itemForFigure(it, departmentKeyFigure)
            }
        }
    }

    private fun itemForFigure(figure: KeyFigure, departmentKeyFigure: DepartmentKeyFigure?): KeyFigureCardItem? {
        return figure.itemForFigure(
            context = requireContext(),
            sharedPrefs = sharedPrefs,
            departmentKeyFigure = departmentKeyFigure,
            numberFormat = numberFormat,
            dateFormat = dateFormat,
            strings = strings,
        ) {
            shareContentDescription = stringsFormat(
                "accessibility.hint.keyFigure.share.withLabel",
                strings[figure.labelStringKey],
            )
            onShareCard = { binding ->
                viewLifecycleOwnerOrNull()?.lifecycleScope?.launch {
                    val uri = ShareManager.getShareCaptureUri(binding, "$label")
                    withContext(Dispatchers.Main) {
                        val shareString = if (rightLocation == null) {
                            stringsFormat("keyFigure.sharing.national", label, leftValue)
                        } else {
                            stringsFormat(
                                "keyFigure.sharing.department",
                                label,
                                leftLocation,
                                leftValue,
                                label,
                                rightValue,
                            )
                        }
                        ShareManager.shareImageAndText(requireContext(), uri, shareString) {
                            strings["common.error.unknown"]?.let { showErrorSnackBar(it) }
                        }
                    }
                }
            }
            onClickListener = View.OnClickListener {
                parentFragment?.findNavControllerOrNull()?.safeNavigate(
                    KeyFiguresPagerFragmentDirections.actionKeyFiguresPagerFragmentToKeyFigureDetailsFragment(
                        figure.labelKey,
                    ),
                )
            }
            favoriteContentDescription = strings["accessibility.hint.addToFavorite"]
            onFavoriteClick = {
                lifecycleScope.launch {
                    viewModel.toggleFavorite(figure)
                }
            }
            descriptionMaxLines = 2
        }
    }

    override fun getTitleKey(): String = "keyFiguresController.title"

    companion object {
        const val STATE_ARG_KEY: String = "STATE_ARG_KEY"

        fun newInstance(state: KeyFigureFragmentState): KeyFiguresFragment {
            val fragment = KeyFiguresFragment()
            fragment.arguments = Bundle().apply {
                putSerializable(STATE_ARG_KEY, state)
            }
            return fragment
        }
    }
}
