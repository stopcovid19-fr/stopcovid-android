/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/12/21 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.content.SharedPreferences
import android.os.Bundle
import android.text.Spanned
import android.util.TypedValue
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import com.lunabeestudio.stopcovid.Constants
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.extension.getApplicationLocale
import com.lunabeestudio.stopcovid.core.extension.viewLifecycleOwnerOrNull
import com.lunabeestudio.stopcovid.core.fastitem.cardWithActionItem
import com.lunabeestudio.stopcovid.core.fastitem.spaceItem
import com.lunabeestudio.stopcovid.core.model.Action
import com.lunabeestudio.stopcovid.extension.collectDataWithLifecycle
import com.lunabeestudio.stopcovid.extension.fromHtml
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.keyFigureMap
import com.lunabeestudio.stopcovid.extension.keyFigureState
import com.lunabeestudio.stopcovid.extension.keyFigureValue
import com.lunabeestudio.stopcovid.extension.safeNavigate
import com.lunabeestudio.stopcovid.fastitem.PayLoadMap
import com.lunabeestudio.stopcovid.fastitem.PayLoadMapType
import com.lunabeestudio.stopcovid.fastitem.mapFranceItem
import com.lunabeestudio.stopcovid.manager.ShareManager
import com.lunabeestudio.stopcovid.view.map.view.marker.KeyFigureMarkerView
import com.lunabeestudio.stopcovid.viewmodel.MapFigureViewModel
import com.lunabeestudio.stopcovid.viewmodel.MapFigureViewModelFactory
import com.lunabeestudio.stopcovid.viewmodel.RegionUiData
import com.mikepenz.fastadapter.GenericItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.yield
import java.text.DateFormat
import java.text.SimpleDateFormat

class MapFiguresFragment : MainFragment() {

    private var refreshScreenJob: Job? = null

    private val sharedPreferences: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(requireContext())
    }

    override fun getTitleKey(): String = "keyfigures.map.screen.title"

    private val identifierMap: Long = "mapItem".hashCode().toLong()

    private val toDateFormat: DateFormat by lazy {
        SimpleDateFormat("dd MMM yyyy", requireContext().getApplicationLocale())
    }

    private val regionUiData: RegionUiData by lazy {
        fetchRegionUiData()
    }

    val viewModel: MapFigureViewModel by viewModels {
        MapFigureViewModelFactory(
            strings,
            injectionContainer.keyFigureMapRepository,
            injectionContainer.keyFigureRepository,
            regionUiData,
            toDateFormat,
        )
    }

    private fun fetchRegionUiData(): RegionUiData {
        val colorTypedValue = TypedValue()
        context?.theme?.resolveAttribute(R.attr.colorRegionMap, colorTypedValue, true)
        return RegionUiData(
            ContextCompat.getColor(requireContext(), colorTypedValue.resourceId),
            ContextCompat.getColor(requireContext(), R.color.color_error),
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val labelKey = sharedPreferences.keyFigureMap
        val progress = sharedPreferences.keyFigureValue
        val state = sharedPreferences.keyFigureState

        viewModel.regionUiData = fetchRegionUiData()
        viewModel.setStatePlay(state)
        viewModel.setKeyFigureByLabel(labelKey, progress)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.chosenKeyFigureMap.collectDataWithLifecycle(viewLifecycleOwner) {
            refreshScreen()
        }

        // needed to avoid resetting the progress on key change + keeping it on refresh
        var firstRefresh = true
        viewModel.availableKeyFiguresFlow.collectDataWithLifecycle(viewLifecycleOwner) {
            if (!firstRefresh) {
                viewModel.setKeyFigureByLabel(
                    labelKey = viewModel.chosenKeyFigureMap.value?.labelKey,
                    progress = viewModel.progress.value,
                )
            } else {
                firstRefresh = false
            }
        }

        viewModel.statePlay.collectDataWithLifecycle(viewLifecycleOwner) { state ->
            yield()
            fastAdapter.notifyItemChanged(
                fastAdapter.getPosition(identifierMap),
                PayLoadMap(
                    PayLoadMapType.PAYLOAD_STATE,
                    listOf(state),
                ),
            )
        }

        viewModel.progress.collectDataWithLifecycle(viewLifecycleOwner) { progress ->
            yield()
            fastAdapter.notifyItemChanged(
                fastAdapter.getPosition(identifierMap),
                PayLoadMap(
                    PayLoadMapType.PAYLOAD_PROGRESS,
                    listOf(progress, viewModel.regionsMap),
                ),
            )
        }
    }

    override fun refreshScreen() {
        setTitle()
        refreshScreenJob?.cancel()
        refreshScreenJob = viewLifecycleOwnerOrNull()?.lifecycleScope?.launch(Dispatchers.Main) {
            val items = getItems()
            if (items.isEmpty()) {
                showLoading()
            } else {
                fastAdapter.setNewList(items)
                showData()
            }
        }
    }

    override suspend fun getItems(): List<GenericItem> {
        if (viewModel.chosenKeyFigureMap.value == null) return listOf()
        val items = mutableListOf<GenericItem>()

        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
        }
        items += cardWithActionItem {
            mainTitle = strings["${viewModel.chosenKeyFigureMap.value?.labelKey}.label"]
            mainBody = strings["${viewModel.chosenKeyFigureMap.value?.labelKey}.description"]
            mainMaxLines = Constants.Map.MAX_LINE_FIGURE_DEF
            onCardClick = {
                viewModel.chosenKeyFigureMap.value?.labelKey?.let {
                    findNavControllerOrNull()?.safeNavigate(
                        MapFiguresFragmentDirections.actionMapFiguresFragmentToDetailFigureMapBottomSheetFragment(it),
                    )
                }
            }
            actions = listOf(
                Action(
                    label = strings["keyfigures.map.screen.changeFigure"],
                    onClickListener = {
                        launchPickerMapFigure()
                    },
                ),
            )
            identifier = "infoKeyFigureMap".hashCode().toLong()
        }
        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
        }
        items += mapFranceItem {
            getMarkerView = { KeyFigureMarkerView(requireContext()) }
            identifier = identifierMap
            dataNotAvailableText = strings["keyfigures.map.screen.noDataAvailable"]
            maxSeekBarValue = viewModel.maxProgressSeekBar
            regions = viewModel.regionsMap
            initialProgress = viewModel.progress.value
            statePlayInit = viewModel.statePlay.value
            onProgressSeekBarChanged = { progress -> viewModel.setProgressManually(progress) }
            onPlayPauseClicked = { viewModel.invertStatePlay() }
            onShareCard = { binding -> ShareManager.shareChart(this@MapFiguresFragment, binding) }
            noDataRegionColor = R.color.color_silver
            noDataRegionSelectedColor = R.color.color_gray
            getMapDescription = ::getMapDescription
            keyFigureName = strings["${viewModel.chosenKeyFigureMap.value?.labelKey}.label"]
            contentDescription = strings["keyfigures.map.cell.accessibility.description"]
            contentDescriptionShare = strings["certificateSharingController.title"]
        }
        items += spaceItem {
            spaceRes = R.dimen.spacing_medium
        }
        items += cardWithActionItem {
            actions = listOf(
                Action(
                    label = strings["keyfigures.map.screen.displayMethod"],
                    onClickListener = {
                        findNavControllerOrNull()?.safeNavigate(
                            MapFiguresFragmentDirections.actionMapFiguresFragmentToKeyFigureMapColorExplanationBottomSheetFragment(),
                        )
                    },
                ),
            )
            identifier = "keyfigures.map.screen.displayMethod".hashCode().toLong()
        }
        items += spaceItem {
            spaceRes = R.dimen.spacing_large
        }

        return items
    }

    private fun getMapDescription(): Spanned {
        val minMaxDescription = strings["keyfigures.map.screen.minMaxFigures"]?.format(
            viewModel.formatValue(viewModel.minValue),
            viewModel.formatValue(viewModel.maxValue),
        )
        val descriptionText = strings["keyfigures.map.screen.description"]
        return "$descriptionText<br>$minMaxDescription".fromHtml()
    }

    override fun onDestroy() {
        super.onDestroy()
        sharedPreferences.keyFigureMap = viewModel.chosenKeyFigureMap.value?.labelKey
        sharedPreferences.keyFigureValue = viewModel.progress.value
        sharedPreferences.keyFigureState = viewModel.statePlay.value
    }

    private fun launchPickerMapFigure() {
        viewModel.chosenKeyFigureMap.value?.labelKey?.let { labelKey ->
            setFragmentResultListener(SelectMapFigureFragment.RESULT_LISTENER_KEY) { _, bundle ->
                bundle.getString(SelectMapFigureFragment.RESULT_LISTENER_BUNDLE_KEY)?.let { newLabelKey ->
                    sharedPreferences.keyFigureMap = newLabelKey
                    viewModel.setKeyFigureByLabel(newLabelKey, 0)
                }
            }
            findNavControllerOrNull()?.safeNavigate(
                MapFiguresFragmentDirections.actionMapFiguresFragmentToSelectMapFigureFragment(labelKey),
            )
        }
    }
}
