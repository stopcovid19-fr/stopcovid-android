/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2021/01/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.MenuHost
import androidx.core.view.MenuItemCompat
import androidx.core.view.MenuProvider
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import androidx.preference.PreferenceManager
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.tabs.TabLayoutMediator
import com.lunabeestudio.common.extension.chosenPostalCode
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.error.AppError
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.activity.MainActivity
import com.lunabeestudio.stopcovid.core.UiConstants
import com.lunabeestudio.stopcovid.core.extension.appCompatActivity
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.extension.getApplicationLocale
import com.lunabeestudio.stopcovid.core.extension.setTextOrHide
import com.lunabeestudio.stopcovid.core.extension.viewLifecycleOwnerOrNull
import com.lunabeestudio.stopcovid.core.fastitem.cardWithActionItem
import com.lunabeestudio.stopcovid.core.fragment.BaseFragment
import com.lunabeestudio.stopcovid.databinding.FragmentKeyfigureDetailsBinding
import com.lunabeestudio.stopcovid.extension.collectDataWithLifecycle
import com.lunabeestudio.stopcovid.extension.getKeyFigureForPostalCode
import com.lunabeestudio.stopcovid.extension.getString
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.itemForFigure
import com.lunabeestudio.stopcovid.extension.labelShortStringKey
import com.lunabeestudio.stopcovid.extension.labelStringKey
import com.lunabeestudio.stopcovid.extension.learnMoreStringKey
import com.lunabeestudio.stopcovid.extension.safeNavigate
import com.lunabeestudio.stopcovid.extension.showPostalCodeDialog
import com.lunabeestudio.stopcovid.manager.ChartManager
import com.lunabeestudio.stopcovid.manager.ShareManager
import com.lunabeestudio.stopcovid.model.KeyFigureComparisonDataType
import com.lunabeestudio.stopcovid.model.KeyFigureComparisonRowData
import com.lunabeestudio.stopcovid.utils.lazyFast
import com.lunabeestudio.stopcovid.viewbinding.bindToKeyFigureData
import com.lunabeestudio.stopcovid.viewmodel.KeyFigureDetailsFragmentViewModel
import com.lunabeestudio.stopcovid.viewmodel.KeyFigureDetailsFragmentViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.NumberFormat
import java.text.SimpleDateFormat
import kotlin.time.Duration.Companion.seconds

class KeyFigureDetailsFragment : BaseFragment() {

    private lateinit var binding: FragmentKeyfigureDetailsBinding

    private val args: KeyFigureDetailsFragmentArgs by navArgs()

    private val sharedPrefs: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(requireContext())
    }

    private val viewModel: KeyFigureDetailsFragmentViewModel by viewModels {
        KeyFigureDetailsFragmentViewModelFactory(strings, injectionContainer.keyFigureRepository)
    }

    private var shouldRefreshAllScreen = true

    private var keyFigureResult: TacResult<KeyFigure?> = TacResult.Success(null)
    private val numberFormat: NumberFormat by lazyFast { NumberFormat.getNumberInstance(getApplicationLocale()) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return FragmentKeyfigureDetailsBinding.inflate(inflater, container, false).also { detailsBinding ->
            binding = detailsBinding
            binding.detailsEvolutionTitle.textView.text = strings["keyFigureDetailController.section.evolution.title"]
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.keyFigure(args.labelKey).collectDataWithLifecycle(viewLifecycleOwner) {
            keyFigureResult = it
            if (shouldRefreshAllScreen) {
                refreshScreen()
            } else {
                shouldRefreshAllScreen = true
            }
            bindItemForFigure()
        }

        viewModel.keyFigureComparisonData.collectDataWithLifecycle(viewLifecycleOwner) {
            bindItemComparison()
        }

        addMenuProvider()
    }

    private fun addMenuProvider() {
        (activity as? MenuHost)?.addMenuProvider(
            object : MenuProvider {
                override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                    menuInflater.inflate(R.menu.postal_code_menu, menu)
                }

                override fun onPrepareMenu(menu: Menu) {
                    MenuItemCompat.setContentDescription(
                        menu.findItem(R.id.item_map),
                        strings["home.infoSection.newPostalCode.button"],
                    )
                    menu.findItem(R.id.item_map).isVisible = injectionContainer.configurationManager.configuration.displayDepartmentLevel
                }

                override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                    return when (menuItem.itemId) {
                        R.id.item_map -> {
                            if (sharedPrefs.chosenPostalCode == null) {
                                MaterialAlertDialogBuilder(requireContext()).showPostalCodeDialog(
                                    layoutInflater = layoutInflater,
                                    strings = strings,
                                    baseFragment = this@KeyFigureDetailsFragment,
                                    sharedPrefs = sharedPrefs,
                                )
                            } else {
                                findNavControllerOrNull()?.safeNavigate(
                                    KeyFigureDetailsFragmentDirections.actionKeyFigureDetailsFragmentToPostalCodeBottomSheetFragment(),
                                )
                            }
                            true
                        }
                        else -> false
                    }
                }
            },
            viewLifecycleOwner,
            Lifecycle.State.RESUMED,
        )
    }

    override fun refreshScreen() {
        binding.emptyLayout.emptyButton.setOnClickListener {
            (activity as? MainActivity)?.showProgress(true)
            lifecycleScope.launch(Dispatchers.IO) {
                injectionContainer.keyFigureRepository.onAppForeground()
                withContext(Dispatchers.Main) {
                    (activity as? MainActivity)?.showProgress(false)
                    refreshScreen()
                }
            }
        }
        appCompatActivity?.supportActionBar?.title = strings[keyFigureResult.data?.labelShortStringKey]
            ?: keyFigureResult.data?.labelStringKey ?: ""

        binding.emptyLayout.emptyTitleTextView.text = strings["infoCenterController.noInternet.title"]
        binding.emptyLayout.emptyDescriptionTextView.text = strings["infoCenterController.noInternet.subtitle"]
        binding.emptyLayout.emptyButton.text = strings["common.retry"]

        val error = (keyFigureResult as? TacResult.Failure)?.throwable
        binding.errorExplanationCard.root.isVisible = error is AppError && error.code == AppError.Code.KEY_FIGURES_NOT_AVAILABLE
        if (error is AppError && error.code == AppError.Code.KEY_FIGURES_NOT_AVAILABLE) {
            binding.errorExplanationCard.explanationTextView.setTextOrHide(error.getString(strings))
            binding.errorExplanationCard.bottomActionTextView.setTextOrHide(
                strings["keyFiguresController.fetchError.button"],
            )
            binding.errorExplanationCard.bottomActionTextView.setOnClickListener {
                viewLifecycleOwnerOrNull()?.lifecycleScope?.launch {
                    shouldRefreshAllScreen = true
                    (activity as? MainActivity)?.showProgress(true)
                    injectionContainer.keyFigureRepository.onAppForeground()
                    (activity as? MainActivity)?.showProgress(false)
                    refreshScreen()
                }
            }
        }
        val showLearnMore = strings[keyFigureResult.data?.learnMoreStringKey] != null
        binding.learnMoreTitle.root.isVisible = showLearnMore
        binding.learnMoreCard.root.isVisible = showLearnMore

        if (showLearnMore) {
            binding.learnMoreTitle.textView.text = strings["keyFigureDetailController.section.learnmore.title"]
            cardWithActionItem {
                mainBody = strings[keyFigureResult.data?.learnMoreStringKey]
            }.bindView(binding.learnMoreCard, emptyList())
        }

        setupPager()
        binding.emptyLayout.root.isVisible = keyFigureResult.data == null
        binding.contentLayout.isVisible = keyFigureResult.data != null
    }

    private fun bindItemForFigure() {
        keyFigureResult.data?.let { keyFigure ->
            keyFigure.itemForFigure(
                requireContext(),
                sharedPrefs,
                keyFigure.getKeyFigureForPostalCode(sharedPrefs.chosenPostalCode, injectionContainer.configurationManager),
                numberFormat,
                SimpleDateFormat(UiConstants.DAY_MONTH_DATE_PATTERN, getApplicationLocale()),
                strings,
            ) {
                shareContentDescription = stringsFormat(
                    "accessibility.hint.keyFigure.share.withLabel",
                    strings[keyFigure.labelStringKey],
                )
                onShareCard = { binding ->
                    viewLifecycleOwnerOrNull()?.lifecycleScope?.launch {
                        val uri = ShareManager.getShareCaptureUri(binding, "$label")
                        withContext(Dispatchers.Main) {
                            val shareString = if (rightLocation == null) {
                                stringsFormat("keyFigure.sharing.national", label, leftValue)
                            } else {
                                stringsFormat(
                                    "keyFigure.sharing.department",
                                    label,
                                    leftLocation,
                                    leftValue,
                                    label,
                                    rightValue,
                                )
                            }
                            ShareManager.shareImageAndText(requireContext(), uri, shareString) {
                                strings["common.error.unknown"]?.let { showErrorSnackBar(it) }
                            }
                        }
                    }
                }
                favoriteContentDescription = strings["accessibility.hint.addToFavorite"]
                onFavoriteClick = {
                    keyFigure.let {
                        lifecycleScope.launch {
                            shouldRefreshAllScreen = false
                            viewModel.toggleFavorite(it)
                        }
                    }
                }
                this.bindView(binding.keyfigureCard, emptyList())
            }
        }
    }

    private fun bindItemComparison() {
        binding.comparisonTitle.endTextView.isVisible = false
        if (viewModel.keyFigureComparisonData.value != null) {
            binding.comparisonTitle.textView.isVisible = true
            binding.comparisonCell.cardView.isVisible = true
            binding.comparisonTitle.textView.text = strings["keyFigureDetailController.section.comparison.title"]
            viewModel.keyFigureComparisonData.value?.let { data ->
                binding.comparisonCell.bindToKeyFigureData(
                    data = data,
                    strings = strings,
                    dateFormat = SimpleDateFormat(
                        UiConstants.DAY_SHORT_MONTH_YEAR_DATE_PATTERN,
                        getApplicationLocale(),
                    ),
                    numberFormat = numberFormat,
                    onClickDetailMax = {
                        navigateToComparisonDetail(
                            KeyFigureComparisonDataType.MAX,
                            data.keyFigureComparisonMaxValue,
                            data.labelKeyFigure,
                        )
                    },
                    onClickDetailMin = {
                        navigateToComparisonDetail(
                            KeyFigureComparisonDataType.MIN,
                            data.keyFigureComparisonMinValue,
                            data.labelKeyFigure,
                        )
                    },
                )
            }
        } else {
            binding.comparisonTitle.textView.isVisible = false
            binding.comparisonCell.cardView.isVisible = false
        }
    }

    private fun setupPager() {
        val keyFigure = keyFigureResult.data ?: return
        val diffTimeStamp = (keyFigure.series?.lastOrNull()?.date ?: 0L) - (keyFigure.series?.get(0)?.date ?: 0L)

        val itemCount = ChartManager.getItemCount(diffTimeStamp.seconds)

        val showTab = itemCount > 1
        binding.detailsTabLayout.isVisible = showTab
        binding.detailsViewPager.adapter = KeyFigureDetailsPagerAdapter(itemCount, keyFigure.labelKey)
        if (showTab) {
            TabLayoutMediator(binding.detailsTabLayout, binding.detailsViewPager) { tab, position ->
                val tabTitleKey = ChartManager.getChartRange(position, itemCount)?.labelKey
                tab.text = strings[tabTitleKey]
            }.attach()
        }
    }

    private inner class KeyFigureDetailsPagerAdapter(private val itemCount: Int, private val labelKey: String) : FragmentStateAdapter(
        childFragmentManager,
        lifecycle,
    ) {
        override fun getItemCount(): Int = itemCount
        override fun createFragment(position: Int): Fragment {
            return KeyFigureChartsFragment.newInstance(labelKey, ChartManager.getChartRange(position, itemCount))
        }
    }

    private fun showErrorSnackBar(message: String) {
        (activity as? MainActivity)?.showErrorSnackBar(message)
    }

    private fun navigateToComparisonDetail(dataType: KeyFigureComparisonDataType, data: KeyFigureComparisonRowData, labelKey: String) {
        val dateFormatFullMonth = SimpleDateFormat(
            UiConstants.DAY_FULL_MONTH_YEAR_DATE_PATTERN,
            getApplicationLocale(),
        )
        findNavControllerOrNull()?.navigate(
            KeyFigureDetailsFragmentDirections.actionKeyFigureDetailsFragmentToDateListKeyFigureComparisonBottomSheetFragment(
                data.date.map {
                    dateFormatFullMonth.format(it)
                }.asReversed().toTypedArray(),
                labelKey,
                data.value,
                dataType,
            ),
        )
    }
}
