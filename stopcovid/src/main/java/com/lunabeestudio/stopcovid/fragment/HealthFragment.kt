/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/10/29 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.view.View
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.fastitem.cardWithActionItem
import com.lunabeestudio.stopcovid.core.fastitem.spaceItem
import com.lunabeestudio.stopcovid.core.model.Action
import com.lunabeestudio.stopcovid.extension.openInExternalBrowser
import com.lunabeestudio.stopcovid.extension.safeNavigate
import com.lunabeestudio.stopcovid.fastitem.logoItem
import com.lunabeestudio.domain.model.LinkType
import com.lunabeestudio.domain.model.RisksUILevelSection
import com.lunabeestudio.domain.model.RisksUILevelSectionLink
import com.mikepenz.fastadapter.GenericItem

class HealthFragment : MainFragment() {

    override fun getTitleKey(): String = "myHealthController.title"

    override suspend fun getItems(): List<GenericItem> {
        return tracingNotActivatedItems()
    }

    private fun tracingNotActivatedItems(): ArrayList<GenericItem> {
        val items = ArrayList<GenericItem>()

        items += logoItem {
            imageRes = R.drawable.diagnosis
            identifier = items.count().toLong()
        }
        risksLevelRepository.getRisksUILevelSections()?.let { risksUILevelSections ->
            items += contactCaseItems(risksUILevelSections)
        }

        return items
    }

    private fun contactCaseItems(sections: List<RisksUILevelSection>): List<GenericItem> {
        val items = mutableListOf<GenericItem>()
        sections.forEach { riskUILevelSection ->
            items += cardWithActionItem {
                mainTitle = strings[riskUILevelSection.section]
                mainBody = strings[riskUILevelSection.description]

                riskUILevelSection.link?.let { link ->
                    actions = listOf(
                        Action(
                            icon = null,
                            label = strings[link.label],
                            showBadge = false,
                            onClickListener = actionForLink(link),
                        ),
                    )
                }
                identifier = items.count().toLong()
            }

            items += spaceItem {
                spaceRes = R.dimen.spacing_medium
                identifier = items.count().toLong()
            }
        }

        return items
    }

    private fun actionForLink(link: RisksUILevelSectionLink): View.OnClickListener {
        return when (link.type) {
            LinkType.web -> View.OnClickListener {
                strings[link.action]?.openInExternalBrowser(it.context, true)
            }
            LinkType.ctrl -> View.OnClickListener {
                findNavControllerOrNull()?.safeNavigate(
                    HealthFragmentDirections.actionHealthFragmentToGestureFragment(),
                )
            }
        }
    }
}
