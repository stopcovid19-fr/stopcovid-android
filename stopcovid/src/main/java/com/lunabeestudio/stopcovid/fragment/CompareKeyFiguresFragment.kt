/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavOptions
import androidx.preference.PreferenceManager
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import com.lunabeestudio.domain.model.Configuration
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.extension.appCompatActivity
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.fragment.BaseFragment
import com.lunabeestudio.stopcovid.databinding.FragmentCompareKeyfigureBinding
import com.lunabeestudio.stopcovid.extension.collectDataWithLifecycle
import com.lunabeestudio.stopcovid.extension.configurationDataSource
import com.lunabeestudio.stopcovid.extension.getDefaultFigureLabel1
import com.lunabeestudio.stopcovid.extension.getDefaultFigureLabel2
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.keyFigureCompare1
import com.lunabeestudio.stopcovid.extension.keyFigureCompare2
import com.lunabeestudio.stopcovid.extension.safeNavigate
import com.lunabeestudio.stopcovid.manager.ChartManager
import com.lunabeestudio.stopcovid.viewmodel.CompareKeyFiguresFragmentViewModel
import com.lunabeestudio.stopcovid.viewmodel.CompareKeyFiguresFragmentViewModelFactory
import kotlinx.coroutines.launch
import kotlin.math.max
import kotlin.time.Duration.Companion.seconds

class CompareKeyFiguresFragment : BaseFragment() {

    private lateinit var binding: FragmentCompareKeyfigureBinding

    private val sharedPreferences: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(requireContext())
    }

    private val viewModel: CompareKeyFiguresFragmentViewModel by viewModels {
        CompareKeyFiguresFragmentViewModelFactory(injectionContainer.keyFigureRepository)
    }

    val configuration: Configuration? by lazy {
        context?.configurationDataSource()?.configuration
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        return FragmentCompareKeyfigureBinding.inflate(inflater, container, false)
            .also { binding = it }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch {
            viewModel.allKeyFiguresWithFavoritesFlow.collectDataWithLifecycle(viewLifecycleOwner) {
                viewModel.setKeyFiguresWithLabel(
                    sharedPreferences.keyFigureCompare1 ?: configuration?.getDefaultFigureLabel1(),
                    sharedPreferences.keyFigureCompare2 ?: configuration?.getDefaultFigureLabel2(),
                )
                if (viewModel.keyFigure1 == null || viewModel.keyFigure2 == null) {
                    findNavControllerOrNull()?.safeNavigate(
                        CompareKeyFiguresFragmentDirections.actionCompareKeyFiguresFragmentToChooseKeyFiguresCompareFragment(),
                        NavOptions.Builder().setPopUpTo(R.id.compareKeyFiguresFragment, true).build(),
                    )
                }
                refreshScreen()
            }
        }
    }

    override fun refreshScreen() {
        appCompatActivity?.supportActionBar?.title = strings["keyfigures.comparison.screen.title"]
        binding.apply {
            infoTextView.text = strings["keyfigures.comparison.evolution.section.subtitle"]

            chooseKeyFiguresButton.text = strings["keyfigures.comparison.keyfiguresChoice.button.title"]
            chooseKeyFiguresButton.setOnClickListener {
                findNavControllerOrNull()?.safeNavigate(
                    CompareKeyFiguresFragmentDirections.actionCompareKeyFiguresFragmentToChooseKeyFiguresCompareFragment(),
                )
            }
        }
        setupPager()
    }

    private fun setupPager() {
        val maxMinDate = max(
            viewModel.keyFigure1?.series?.get(0)?.date ?: 0L,
            viewModel.keyFigure2?.series?.get(0)?.date ?: 0L,
        )
        val diffTimeStamp = (viewModel.keyFigure1?.series?.lastOrNull()?.date ?: 0L) - maxMinDate
        val itemCount = ChartManager.getItemCount(diffTimeStamp.seconds)
        val showTab = itemCount > 1
        binding.detailsTabLayout.isVisible = showTab
        binding.detailsViewPager.adapter = CompareKeyFiguresPagerAdapter(itemCount)
        if (showTab) {
            TabLayoutMediator(
                binding.detailsTabLayout,
                binding.detailsViewPager,
            ) { tab, position ->
                val tabTitleKey = ChartManager.getChartRange(position, itemCount)?.labelKey
                tab.text = strings[tabTitleKey]
            }.attach()
        }
    }

    private inner class CompareKeyFiguresPagerAdapter(private val itemCount: Int) :
        FragmentStateAdapter(
            childFragmentManager,
            lifecycle,
        ) {
        override fun getItemCount(): Int = itemCount
        override fun createFragment(position: Int): Fragment {
            return CompareKeyFiguresChartsFragment.newInstance(
                viewModel.keyFigure1?.labelKey,
                viewModel.keyFigure2?.labelKey,
                ChartManager.getChartRange(position, itemCount),
            )
        }
    }
}
