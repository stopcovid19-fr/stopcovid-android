/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull

class UniversalQrScanFragment : QRCodeDccFragment() {

    override fun getTitleKey(): String = "universalQrScanController.title"
    override val explanationKey: String = "universalQrScanController.explanation"

    override fun onCodeScanned(code: String) {
        setFragmentResult(
            SCANNED_CODE_RESULT_KEY,
            bundleOf(SCANNED_CODE_BUNDLE_KEY to code),
        )
        findNavControllerOrNull()?.popBackStack(R.id.homeFragment, false)
    }

    companion object {
        const val SCANNED_CODE_RESULT_KEY: String = "UNIVERSAL_QR_SCAN_FRAGMENT.SCANNED_CODE_RESULT_KEY"
        const val SCANNED_CODE_BUNDLE_KEY: String = "UNIVERSAL_QR_SCAN_FRAGMENT.SCANNED_CODE_BUNDLE_KEY"
    }
}
