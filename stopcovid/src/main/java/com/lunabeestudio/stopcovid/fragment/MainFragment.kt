/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.os.Bundle
import android.view.View
import androidx.core.view.doOnNextLayout
import androidx.core.view.isVisible
import com.google.android.material.appbar.AppBarLayout
import com.lunabeestudio.domain.manager.DebugManager
import com.lunabeestudio.domain.repository.RisksLevelRepository
import com.lunabeestudio.stopcovid.activity.MainActivity
import com.lunabeestudio.stopcovid.core.extension.appCompatActivity
import com.lunabeestudio.stopcovid.core.extension.registerToAppBarLayoutForLiftOnScroll
import com.lunabeestudio.stopcovid.core.fragment.FastAdapterFragment
import com.lunabeestudio.stopcovid.databinding.ActivityMainBinding
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.manager.ConfigurationManager
import com.lunabeestudio.stopcovid.manager.LinksManager
import com.lunabeestudio.stopcovid.manager.MoreKeyFiguresManager
import com.lunabeestudio.stopcovid.manager.PrivacyManager
import com.lunabeestudio.stopcovid.repository.WalletRepository
import com.lunabeestudio.stopcovid.utils.lazyFast

abstract class MainFragment : FastAdapterFragment() {
    abstract fun getTitleKey(): String

    private fun getActivityBinding(): ActivityMainBinding? = (activity as? MainActivity)?.binding

    override fun getAppBarLayout(): AppBarLayout? = getActivityBinding()?.appBarLayout

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if ((activity as? MainActivity)?.binding?.tabLayout?.isVisible == true) {
            postponeEnterTransition()
            (activity as? MainActivity)?.binding?.appBarLayout?.doOnNextLayout {
                startPostponedEnterTransition()
            }
            (activity as? MainActivity)?.binding?.tabLayout?.isVisible = false
        }
        getActivityBinding()?.appBarLayout?.let { appBarLayout ->
            binding?.recyclerView?.registerToAppBarLayoutForLiftOnScroll(appBarLayout)
        }
    }

    override fun refreshScreen() {
        setTitle()
        super.refreshScreen()
    }

    protected open fun setTitle() {
        appCompatActivity?.supportActionBar?.title = strings[getTitleKey()]
    }

    protected fun showSnackBar(message: String) {
        (activity as? MainActivity)?.showSnackBar(message)
    }

    protected val risksLevelRepository: RisksLevelRepository by lazyFast {
        injectionContainer.risksLevelRepository
    }
    protected val linksManager: LinksManager by lazyFast {
        injectionContainer.linksManager
    }
    protected val moreKeyFiguresManager: MoreKeyFiguresManager by lazyFast {
        injectionContainer.moreKeyFiguresManager
    }
    protected val privacyManager: PrivacyManager by lazyFast {
        injectionContainer.privacyManager
    }
    protected val walletRepository: WalletRepository by lazyFast {
        injectionContainer.walletRepository
    }
    protected val debugManager: DebugManager by lazyFast {
        injectionContainer.debugManager
    }
    protected val configurationManager: ConfigurationManager by lazyFast {
        injectionContainer.configurationManager
    }
}
