/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/10/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.navArgs
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.lunabeestudio.stopcovid.Constants
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.activity.MainActivity
import com.lunabeestudio.stopcovid.core.extension.appCompatActivity
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.extension.toDimensSize
import com.lunabeestudio.stopcovid.core.fragment.BaseFragment
import com.lunabeestudio.stopcovid.databinding.FragmentWalletFullscreen2ddocBinding
import com.lunabeestudio.stopcovid.extension.fullDescription
import com.lunabeestudio.stopcovid.extension.fullNameUppercase
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.safeNavigate
import com.lunabeestudio.stopcovid.manager.ShareManager
import com.lunabeestudio.stopcovid.model.FrenchCertificate
import com.lunabeestudio.stopcovid.viewmodel.WalletFullscreen2DdocViewModel
import com.lunabeestudio.stopcovid.viewmodel.WalletFullscreen2DdocViewModelFactory

class WalletFullscreen2DdocFragment : BaseFragment() {

    private val args: WalletFullscreen2DdocFragmentArgs by navArgs()

    private val viewModel: WalletFullscreen2DdocViewModel by viewModels {
        WalletFullscreen2DdocViewModelFactory(
            injectionContainer.walletRepository,
        )
    }

    private var frenchCertificate: FrenchCertificate? = null

    private val barcodeEncoder = BarcodeEncoder()
    private val qrCodeSize by lazy {
        R.dimen.qr_code_fullscreen_size.toDimensSize(requireContext()).toInt()
    }

    private lateinit var binding: FragmentWalletFullscreen2ddocBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentWalletFullscreen2ddocBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appCompatActivity?.supportActionBar?.title = strings["walletController.title"]
        viewModel.getCertificate(args.id).observe(viewLifecycleOwner) { frenchCertificate ->
            this.frenchCertificate = frenchCertificate
            refreshScreen()
        }

        binding.formatTextView.text = Constants.QrCode.FORMAT_2D_DOC

        binding.shareButton.text = strings["common.share"]
        binding.shareButton.setOnClickListener {
            showCertificateSharingBottomSheet()
        }
        addMenuProvider()
    }

    private fun addMenuProvider() {
        (activity as? MenuHost)?.addMenuProvider(
            object : MenuProvider {
                override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                    menuInflater.inflate(R.menu.fullscreen_qr_code_menu, menu)
                }

                override fun onPrepareMenu(menu: Menu) {
                    super.onPrepareMenu(menu)
                    menu.findItem(R.id.qr_code_menu_share).title = strings["walletController.menu.share"]
                    menu.findItem(R.id.qr_code_menu_more).title = strings["accessibility.hint.otherActions"]
                }

                override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                    return when (menuItem.itemId) {
                        R.id.qr_code_menu_share -> {
                            showCertificateSharingBottomSheet()
                            true
                        }
                        R.id.qr_code_menu_more -> {
                            showQrCodeMoreActionBottomSheet()
                            true
                        }
                        else -> false
                    }
                }
            },
            viewLifecycleOwner,
            Lifecycle.State.RESUMED,
        )
    }

    override fun refreshScreen() {
        val frenchCertificate = frenchCertificate ?: return

        binding.barcodeSecuredView.bitmap =
            barcodeEncoder.encodeBitmap(
                frenchCertificate.value,
                BarcodeFormat.DATA_MATRIX,
                qrCodeSize,
                qrCodeSize,
            )
        binding.detailsTextView.text = frenchCertificate.fullNameUppercase()
        binding.sha256TextView.text = frenchCertificate.sha256
    }

    private fun showCertificateSharingBottomSheet() {
        val activityBinding = (activity as? MainActivity)?.binding ?: return
        val text = frenchCertificate?.fullDescription(
            strings,
            injectionContainer.configurationManager.configuration,
            context,
            null,
        )
        ShareManager.setupCertificateSharingBottomSheet(this, text) {
            binding.barcodeSecuredView.runUnsecured {
                ShareManager.getShareCaptureUri(activityBinding, ShareManager.certificateScreenshotFilename)
            }
        }
        findNavControllerOrNull()?.safeNavigate(
            WalletFullscreen2DdocFragmentDirections.actionFullscreen2DdocFragmentToCertificateSharingBottomSheetFragment(),
        )
    }

    private fun showQrCodeMoreActionBottomSheet() {
        setFragmentResultListener(QrCodeMoreActionBottomSheetFragment.MORE_ACTION_RESULT_KEY) { _, bundle ->
            if (bundle.getBoolean(QrCodeMoreActionBottomSheetFragment.MORE_ACTION_BUNDLE_KEY_SHARE_REQUESTED, false)) {
                findNavControllerOrNull()?.addOnDestinationChangedListener(
                    object : NavController.OnDestinationChangedListener {
                        override fun onDestinationChanged(
                            controller: NavController,
                            destination: NavDestination,
                            arguments: Bundle?,
                        ) {
                            if (controller.currentDestination?.id == R.id.fullscreen2DdocFragment) {
                                showCertificateSharingBottomSheet()
                                controller.removeOnDestinationChangedListener(this)
                            }
                        }
                    },
                )
            }
        }

        findNavControllerOrNull()?.safeNavigate(
            WalletFullscreen2DdocFragmentDirections.actionFullscreen2DdocFragmentToQrCodeMoreActionBottomSheetFragment(
                showShare = true,
                showBrightness = true,
            ),
        )
    }
}
