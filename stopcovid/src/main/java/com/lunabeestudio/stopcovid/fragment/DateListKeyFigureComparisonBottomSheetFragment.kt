/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/01/20 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.lunabeestudio.stopcovid.core.LocalizedApplication
import com.lunabeestudio.stopcovid.core.manager.LocalizedStrings
import com.lunabeestudio.stopcovid.databinding.FragmentDateListKeyFigureComparisonBottomSheetBinding
import com.lunabeestudio.stopcovid.extension.fromHtml

class DateListKeyFigureComparisonBottomSheetFragment : BottomSheetDialogFragment() {

    private val args: DateListKeyFigureComparisonBottomSheetFragmentArgs by navArgs()

    private lateinit var binding: FragmentDateListKeyFigureComparisonBottomSheetBinding

    private val strings: LocalizedStrings
        get() = (activity?.application as? LocalizedApplication)?.localizedStrings ?: emptyMap()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentDateListKeyFigureComparisonBottomSheetBinding.inflate(inflater, container, false)

        binding.titleTextView.text = strings["keyFigureDetailController.section.comparison.${args.dataType.name.lowercase()}.longLabel"]
            ?.format(strings["${args.labelKeyFigure}.label"])

        binding.descriptionTextView.text = strings["keyFigureDetailController.section.comparison.dateOccurrences.description"]?.format(
            args.value,
            args.dates.size,
        )?.fromHtml()

        binding.dateListTextView.text = args.dates.joinToString("\n")

        return binding.root
    }
}
