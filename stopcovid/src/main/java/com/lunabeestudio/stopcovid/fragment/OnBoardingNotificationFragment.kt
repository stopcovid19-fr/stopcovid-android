/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/04/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.Manifest
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.lunabeestudio.stopcovid.Constants
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.extension.openAppSettings
import com.lunabeestudio.stopcovid.core.extension.showPermissionSettingsDialog
import com.lunabeestudio.stopcovid.core.fastitem.captionItem
import com.lunabeestudio.stopcovid.core.fastitem.spaceItem
import com.lunabeestudio.stopcovid.core.fastitem.titleItem
import com.lunabeestudio.stopcovid.extension.safeNavigate
import com.lunabeestudio.stopcovid.fastitem.logoItem
import com.mikepenz.fastadapter.GenericItem

class OnBoardingNotificationFragment : OnBoardingFragment() {

    private val sharedPreferences: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(requireContext())
    }

    private var permissionResultLauncher: ActivityResultLauncher<String>? = null

    override fun getTitleKey(): String = "onboarding.beAwareController.title"
    override fun getButtonTitleKey(): String = "onboarding.beAwareController.allowNotifications"
    override fun getOnButtonClick(): () -> Unit = {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            permissionResultLauncher?.launch(Manifest.permission.POST_NOTIFICATIONS)
        } else {
            showNextFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        permissionResultLauncher = registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            if (isGranted) {
                showNextFragment()
            } else if (!shouldShowRequestPermissionRationale(Manifest.permission.POST_NOTIFICATIONS)) {
                context?.showPermissionSettingsDialog(
                    strings = strings,
                    messageKey = "proximityController.error.noNotifications",
                    positiveKey = "common.settings",
                    cancelable = true,
                    positiveAction = {
                        openAppSettings()
                    },
                )
            }
        }
    }

    override suspend fun getItems(): List<GenericItem> {
        val items = arrayListOf<GenericItem>()

        items += logoItem {
            imageRes = R.drawable.notification
            identifier = items.size.toLong()
        }
        items += spaceItem {
            spaceRes = R.dimen.spacing_xlarge
            identifier = items.size.toLong()
        }
        items += titleItem {
            text = strings["onboarding.beAwareController.mainMessage.title"]
            gravity = Gravity.CENTER
            identifier = items.size.toLong()
        }
        items += captionItem {
            text = strings["onboarding.beAwareController.mainMessage.subtitle"]
            gravity = Gravity.CENTER
            identifier = items.size.toLong()
        }

        return items
    }

    private fun showNextFragment() {
        sharedPreferences.edit {
            putBoolean(Constants.SharedPrefs.ON_BOARDING_DONE, true)
        }
        findNavControllerOrNull()
            ?.safeNavigate(OnBoardingNotificationFragmentDirections.actionOnBoardingNotificationFragmentToMainActivity())
        activity?.finishAndRemoveTask()
    }
}
