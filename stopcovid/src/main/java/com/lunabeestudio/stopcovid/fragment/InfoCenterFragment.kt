/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/10/29 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.os.Bundle
import android.view.View
import androidx.core.content.edit
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import com.lunabeestudio.stopcovid.Constants
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.extension.viewLifecycleOwnerOrNull
import com.lunabeestudio.stopcovid.core.fastitem.spaceItem
import com.lunabeestudio.stopcovid.extension.getRelativeDateTimeString
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.startTextIntent
import com.lunabeestudio.stopcovid.fastitem.infoCenterDetailCardItem
import com.lunabeestudio.stopcovid.viewmodel.InfoCenterViewModel
import com.lunabeestudio.stopcovid.viewmodel.InfoCenterViewModelFactory
import com.mikepenz.fastadapter.GenericItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.time.Duration.Companion.seconds

class InfoCenterFragment : TimeMainFragment() {

    private val viewModel: InfoCenterViewModel by viewModels {
        InfoCenterViewModelFactory(
            injectionContainer.infoCenterRepository,
            injectionContainer.refreshInfoCenterUseCase,
        )
    }

    private val tagRecyclerPool = RecyclerView.RecycledViewPool()

    val args: InfoCenterFragmentArgs by navArgs()

    private var alreadyScrolledToItem: Boolean = false

    private val smoothScroller: RecyclerView.SmoothScroller by lazy {
        object : LinearSmoothScroller(context) {
            override fun getVerticalSnapPreference(): Int {
                return SNAP_TO_START
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        alreadyScrolledToItem = savedInstanceState?.getBoolean(SAVE_INSTANCE_ALREADY_SCROLL, false) ?: false

        viewModel.infoCenterData.observe(viewLifecycleOwner) {
            refreshScreen()
        }

        binding?.emptyLayout?.emptyButton?.setOnClickListener {
            showLoading()
            viewLifecycleOwnerOrNull()?.lifecycleScope?.launch(Dispatchers.IO) {
                viewModel.refreshInfoCenter()
                withContext(Dispatchers.Main) {
                    refreshScreen()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        PreferenceManager.getDefaultSharedPreferences(requireContext()).edit {
            putBoolean(Constants.SharedPrefs.HAS_NEWS, false)
        }
        viewLifecycleOwnerOrNull()?.lifecycleScope?.launch {
            viewModel.refreshInfoCenter()
        }
    }

    override fun getTitleKey(): String = "infoCenterController.title"

    override suspend fun getItems(): List<GenericItem> {
        val items = ArrayList<GenericItem>()

        val infoCenterData = viewModel.infoCenterData.value

        if (infoCenterData != null) {
            items += spaceItem {
                spaceRes = R.dimen.spacing_large
                identifier = items.size.toLong()
            }

            infoCenterData.entries.forEach { info ->
                val infoTitle = info.title
                val infoDescription = info.description

                items += infoCenterDetailCardItem {
                    header = info.timestamp.seconds.getRelativeDateTimeString(
                        requireContext(),
                        this@InfoCenterFragment.strings,
                    )
                    title = infoTitle
                    body = infoDescription
                    link = info.buttonLabel
                    this.tags = info.tags ?: emptyList()
                    url = info.url
                    tagRecyclerViewPool = this@InfoCenterFragment.tagRecyclerPool
                    shareContentDescription = strings["accessibility.hint.info.share"]
                    onShareCard = {
                        stringsFormat("info.sharing.title", infoTitle)?.let { requireContext().startTextIntent(it) }
                    }
                    identifier = info.id
                }

                items += spaceItem {
                    spaceRes = R.dimen.spacing_large
                    identifier = items.size.toLong()
                }
            }
        }

        scrollToFirstIfNeeded()

        return items
    }

    private fun scrollToFirstIfNeeded() {
        args.infoIdentifier.takeIf { it != -1L && !alreadyScrolledToItem }?.let { identifier ->
            binding?.recyclerView?.postDelayed({
                val infoIndex = fastAdapter.getPosition(identifier)
                if (infoIndex != RecyclerView.NO_POSITION) {
                    smoothScroller.targetPosition = infoIndex
                    binding?.recyclerView?.layoutManager?.startSmoothScroll(smoothScroller)
                }
                alreadyScrolledToItem = true
            }, 200,)
        }
    }

    override fun refreshScreen() {
        super.refreshScreen()

        binding?.emptyLayout?.emptyTitleTextView?.text = strings["infoCenterController.noInternet.title"]
        binding?.emptyLayout?.emptyDescriptionTextView?.text = strings["infoCenterController.noInternet.subtitle"]
        binding?.emptyLayout?.emptyButton?.text = strings["common.retry"]
    }

    override fun timeRefresh() {
        refreshScreen()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean(SAVE_INSTANCE_ALREADY_SCROLL, alreadyScrolledToItem)
        super.onSaveInstanceState(outState)
    }

    companion object {
        private const val SAVE_INSTANCE_ALREADY_SCROLL: String = "Save.Instance.Already.Scroll"
    }
}
