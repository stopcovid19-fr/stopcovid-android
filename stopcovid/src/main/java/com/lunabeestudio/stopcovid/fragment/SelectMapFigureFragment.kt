/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/12/21 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.lunabeestudio.stopcovid.Constants
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.extension.viewLifecycleOwnerOrNull
import com.lunabeestudio.stopcovid.extension.collectDataWithLifecycle
import com.lunabeestudio.stopcovid.extension.descriptionStringKey
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.labelStringKey
import com.lunabeestudio.stopcovid.fastitem.selectionItem
import com.lunabeestudio.stopcovid.viewmodel.SelectMapFigureViewModel
import com.lunabeestudio.stopcovid.viewmodel.SelectMapFigureViewModelFactory
import com.mikepenz.fastadapter.GenericItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SelectMapFigureFragment : MainFragment() {

    private var labelKey: String? = null
    private val args: SelectMapFigureFragmentArgs by navArgs()

    val viewModel: SelectMapFigureViewModel by viewModels {
        SelectMapFigureViewModelFactory(
            injectionContainer.keyFigureRepository,
            injectionContainer.keyFigureMapRepository,
        )
    }

    override fun getTitleKey(): String = "keyfigures.map.keyfiguresList.screen.title"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        labelKey = args.labelKey
        viewModel.fetchKeyFigures()
        viewModel.keyFigures.collectDataWithLifecycle(viewLifecycleOwner) {
            refreshScreen()
        }
    }

    override fun refreshScreen() {
        setTitle()
        if (viewModel.keyFigures.value.isNotEmpty()) {
            viewLifecycleOwnerOrNull()?.lifecycleScope?.launch(Dispatchers.Main) {
                val items = getItems()
                fastAdapter.setNewList(items)
                showData()
            }
        } else {
            showLoading()
        }
    }

    override suspend fun getItems(): List<GenericItem> {
        val items = arrayListOf<GenericItem>()
        getFiguresList(items)
        return items
    }

    private fun getFiguresList(items: ArrayList<GenericItem>) {
        val keyFigures = viewModel.keyFigures.value
        keyFigures.mapTo(items) {
            selectionItem {
                title = strings[it.labelStringKey]
                identifier = it.labelStringKey.hashCode().toLong()
                caption = strings[it.descriptionStringKey]
                identifier = title.hashCode().toLong()
                maxLineCaption = Constants.Map.MAX_LINE_FIGURE_SELECT
                if (labelKey == it.labelKey) {
                    showSelection = true
                    context?.let {
                        val color = ContextCompat.getColor(it, R.color.color_gray)
                        iconTint = color
                        textColor = color
                    }
                } else {
                    onClick = {
                        labelKey = it.labelKey
                        backToMapFragment()
                    }
                }
            }
        }
    }

    private fun backToMapFragment() {
        setFragmentResult(
            RESULT_LISTENER_KEY,
            bundleOf(
                RESULT_LISTENER_BUNDLE_KEY to labelKey,
            ),
        )
        findNavControllerOrNull()?.popBackStack()
    }

    companion object {
        const val RESULT_LISTENER_KEY: String = "resultFigureMapPicker"
        const val RESULT_LISTENER_BUNDLE_KEY: String = "resultKeyFigure"
    }
}
