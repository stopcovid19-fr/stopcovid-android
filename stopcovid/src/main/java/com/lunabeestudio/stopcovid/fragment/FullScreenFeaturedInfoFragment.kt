/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/01/03 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.lunabeestudio.stopcovid.core.extension.appCompatActivity
import com.lunabeestudio.stopcovid.core.extension.viewLifecycleOwnerOrNull
import com.lunabeestudio.stopcovid.core.fragment.BaseFragment
import com.lunabeestudio.stopcovid.databinding.FragmentFullScreenFeaturedInfoBinding
import com.lunabeestudio.stopcovid.extension.collectDataWithLifecycle
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.openInExternalBrowser
import com.lunabeestudio.stopcovid.viewmodel.FullScreenFeaturedInfoViewModel
import com.lunabeestudio.stopcovid.viewmodel.FullScreenFeaturedInfoViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.yield

class FullScreenFeaturedInfoFragment : BaseFragment() {

    private var refreshScreenJob: Job? = null

    private val args: FullScreenFeaturedInfoFragmentArgs by navArgs()

    private val binding: FragmentFullScreenFeaturedInfoBinding by lazy {
        FragmentFullScreenFeaturedInfoBinding.inflate(layoutInflater)
    }

    private val viewModel: FullScreenFeaturedInfoViewModel by viewModels {
        FullScreenFeaturedInfoViewModelFactory(
            injectionContainer.featuredInfoRepository,
            args.featuredInfoId,
        )
    }

    private var alreadyRefreshed: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showLoading()

        viewModel.featuredInfo.collectDataWithLifecycle(viewLifecycleOwner) {
            refreshScreen()
        }

        binding.emptyLayout.emptyButton.setOnClickListener { refreshFile() }
        binding.emptyLayout.emptyButton.text = strings["common.retry"]
        binding.emptyLayout.emptyTitleTextView.text = strings["infoCenterController.noInternet.subtitle"]
    }

    override fun refreshScreen() {
        refreshScreenJob?.cancel()
        refreshScreenJob = viewLifecycleOwnerOrNull()?.lifecycleScope?.launch(Dispatchers.Main) {
            yield()
            setTitle()
            val pdfBitmap = viewModel.getBitmapFromPdfFile(requireContext())
            if (pdfBitmap != null) {
                showData()
                binding.documentPhotoView.contentDescription = viewModel.featuredInfo.value?.hint
                binding.documentPhotoView.setImageBitmap(pdfBitmap)

                viewModel.featuredInfo.value?.url?.let { url ->
                    binding.knowMoreButton.apply {
                        isVisible = true
                        text = strings["common.readMore"]
                        setOnClickListener {
                            url.openInExternalBrowser(it.context)
                        }
                    }
                }
            } else {
                if (!alreadyRefreshed) {
                    alreadyRefreshed = true
                    refreshFile()
                } else {
                    showEmpty()
                }
            }
        }
    }

    private fun refreshFile() {
        viewLifecycleOwner.lifecycleScope.launch {
            showLoading()
            viewModel.refreshImagePDF()
            refreshScreen()
        }
    }

    private fun showLoading() {
        binding.emptyLayout.emptyLayout.isVisible = false
        binding.dataLayout.isVisible = false
        binding.progressBar.isVisible = true
    }

    private fun showData() {
        binding.emptyLayout.emptyLayout.isVisible = false
        binding.dataLayout.isVisible = true
        binding.progressBar.isVisible = false
    }

    private fun showEmpty() {
        binding.emptyLayout.emptyLayout.isVisible = true
        binding.dataLayout.isVisible = false
        binding.progressBar.isVisible = false
    }

    fun setTitle() {
        appCompatActivity?.supportActionBar?.title = viewModel.featuredInfo.value?.title
    }
}
