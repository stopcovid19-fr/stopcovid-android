/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2021/01/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.DimenRes
import androidx.core.os.bundleOf
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import com.lunabeestudio.common.extension.chosenPostalCode
import com.lunabeestudio.domain.model.DepartmentKeyFigure
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.core.databinding.ItemCaptionBinding
import com.lunabeestudio.stopcovid.core.extension.appCompatActivity
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.extension.findParentFragmentByType
import com.lunabeestudio.stopcovid.core.extension.safeEmojiSpanify
import com.lunabeestudio.stopcovid.core.fragment.BaseFragment
import com.lunabeestudio.stopcovid.databinding.ItemKeyFigureChartCardBinding
import com.lunabeestudio.stopcovid.extension.getKeyFigureForPostalCode
import com.lunabeestudio.stopcovid.extension.getSerializableCompat
import com.lunabeestudio.stopcovid.extension.hasAverageChart
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.labelStringKey
import com.lunabeestudio.stopcovid.extension.safeNavigate
import com.lunabeestudio.stopcovid.fastitem.keyFigureCardChartItem
import com.lunabeestudio.stopcovid.manager.ChartManager
import com.lunabeestudio.stopcovid.manager.ShareManager
import com.lunabeestudio.stopcovid.model.ChartFullScreenData
import com.lunabeestudio.stopcovid.model.ChartInformation
import com.lunabeestudio.stopcovid.utils.lazyFast
import com.lunabeestudio.stopcovid.viewmodel.KeyFigureChartsFragmentViewModel
import com.lunabeestudio.stopcovid.viewmodel.KeyFigureChartsFragmentViewModelFactory
import kotlin.time.Duration
import kotlinx.coroutines.launch

class KeyFigureChartsFragment : BaseFragment() {

    private val sharedPrefs: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(requireContext())
    }

    private val viewModel: KeyFigureChartsFragmentViewModel by viewModels {
        KeyFigureChartsFragmentViewModelFactory(injectionContainer.keyFigureRepository)
    }

    private lateinit var rootLayout: LinearLayout

    private val durationToShow: Duration by lazyFast {
        val range = arguments?.getSerializableCompat(RANGE_ARG_KEY) ?: ChartManager.ChartRange.ALL
        range.rangeDuration
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        rootLayout = LinearLayout(inflater.context).apply {
            layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
            )
            orientation = LinearLayout.VERTICAL
            id = R.id.keyfigures_charts_container
        }
        return rootLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycleScope.launch {
            viewModel.setKeyFiguresWithLabel(arguments?.getString(LABEL_KEY_ARG_KEY))
            refreshScreen()
        }

        findNavControllerOrNull()?.currentBackStackEntry?.savedStateHandle?.getLiveData<Boolean>(
            PostalCodeBottomSheetFragment.SHOULD_BE_REFRESHED_KEY,
        )?.observe(viewLifecycleOwner) { shouldBeRefreshed ->
            if (shouldBeRefreshed) {
                refreshScreen()
            }
        }
    }

    override fun refreshScreen() {
        rootLayout.removeAllViewsInLayout()
        getItemsView().forEach(rootLayout::addView)
    }

    private fun getItemsView(): List<View> {
        val views = mutableListOf<View>()

        if (viewModel.keyFigure == null) {
            return emptyList()
        }

        val resources = rootLayout.context.resources

        if (viewModel.keyFigure?.series?.isEmpty() == true) {
            views += ItemCaptionBinding.inflate(layoutInflater, rootLayout, false).apply {
                root.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                    topMargin += resources.getDimensionPixelSize(R.dimen.spacing_large)
                }
                textView.text = stringsFormat(
                    "keyFigureDetailController.nodata",
                    strings[viewModel.keyFigure?.labelStringKey],
                ).safeEmojiSpanify()
            }.root
        } else {
            context?.let { safeContext ->
                viewModel.keyFigure?.let { figure ->
                    val departmentKeyFigure = figure.getKeyFigureForPostalCode(
                        sharedPrefs.chosenPostalCode,
                        injectionContainer.configurationManager,
                    )
                    val chartsToDisplay = getChartTypesToDisplay(figure, departmentKeyFigure)

                    views += chartsToDisplay.mapIndexed { index, chartDataType ->
                        @DimenRes val marginTopRes: Int? = if (index == 0) R.dimen.spacing_medium else null

                        @DimenRes val marginBottomRes: Int =
                            if (index < chartsToDisplay.count() - 1) {
                                R.dimen.spacing_medium
                            } else {
                                R.dimen.spacing_large
                            }
                        val chartInfo = ChartInformation(
                            context = safeContext,
                            strings = strings,
                            keyFigure = figure,
                            chartDataType = chartDataType,
                            departmentKeyFigure = departmentKeyFigure,
                            durationToShow = durationToShow,
                        )

                        ItemKeyFigureChartCardBinding.inflate(layoutInflater, rootLayout, false).apply {
                            this.root.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                                marginTopRes?.let { topMargin += resources.getDimensionPixelSize(marginTopRes) }
                                bottomMargin += resources.getDimensionPixelSize(marginBottomRes)
                            }
                            keyFigureCardChartItem {
                                chartData = chartInfo.chartData
                                chartType = chartInfo.chartType
                                limitLineData = chartInfo.limitLineData
                                chartExplanationLabel = chartInfo.chartExplanationLabel
                                shareContentDescription = strings["accessibility.hint.keyFigure.chart.share"]
                                onShareCard = { binding ->
                                    ShareManager.shareChart(this@KeyFigureChartsFragment, binding)
                                }
                                onClickListener = getChartOnClickListener(figure.labelKey, chartDataType)
                            }.bindView(this, emptyList())
                        }.root
                    }
                }
            }
        }

        return views
    }

    private fun getChartTypesToDisplay(figure: KeyFigure, departmentKeyFigure: DepartmentKeyFigure?): List<ChartDataType> {
        val chartsToDisplay: MutableList<ChartDataType> = mutableListOf()

        if (figure.displayOnSameChart) {
            chartsToDisplay += ChartDataType.MULTI
        } else {
            if (departmentKeyFigure != null) {
                chartsToDisplay += ChartDataType.LOCAL
            }
            chartsToDisplay += ChartDataType.GLOBAL
        }
        if (figure.hasAverageChart()) {
            chartsToDisplay += ChartDataType.AVERAGE
        }
        return chartsToDisplay
    }

    fun setTitle(title: String) {
        appCompatActivity?.supportActionBar?.title = title
    }

    private fun getChartOnClickListener(labelKey: String, chartDataType: ChartDataType): View.OnClickListener = View.OnClickListener {
        findParentFragmentByType<KeyFigureDetailsFragment>()?.findNavControllerOrNull()?.safeNavigate(
            KeyFigureDetailsFragmentDirections.actionKeyFigureDetailsFragmentToChartFullScreenActivity(
                ChartFullScreenData(
                    keyFigureKey = labelKey,
                    chartDataType = chartDataType,
                    durationToShow = durationToShow,
                    keyFigureKey2 = null,
                ),
            ),
        )
    }

    companion object {
        private const val LABEL_KEY_ARG_KEY = "LABEL_KEY_ARG_KEY"
        private const val RANGE_ARG_KEY = "RANGE_ARG_KEY"

        fun newInstance(labelKey: String, range: ChartManager.ChartRange?): KeyFigureChartsFragment {
            return KeyFigureChartsFragment().apply {
                arguments = bundleOf(
                    LABEL_KEY_ARG_KEY to labelKey,
                    RANGE_ARG_KEY to range,
                )
            }
        }
    }
}
