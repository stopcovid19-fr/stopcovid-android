/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/29/10 - for the STOP-COVID project
 */

package com.lunabeestudio.stopcovid.fragment

import android.content.SharedPreferences
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.text.toSpannable
import androidx.core.view.doOnNextLayout
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.preference.PreferenceManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.domain.model.WalletCertificateType
import com.lunabeestudio.error.AppError
import com.lunabeestudio.error.TACError
import com.lunabeestudio.stopcovid.R
import com.lunabeestudio.stopcovid.activity.MainActivity
import com.lunabeestudio.stopcovid.core.extension.findNavControllerOrNull
import com.lunabeestudio.stopcovid.core.extension.findParentFragmentByType
import com.lunabeestudio.stopcovid.core.extension.formatOrNull
import com.lunabeestudio.stopcovid.core.extension.getApplicationLocale
import com.lunabeestudio.stopcovid.core.extension.toDimensSize
import com.lunabeestudio.stopcovid.core.fastitem.captionItem
import com.lunabeestudio.stopcovid.core.fastitem.spaceItem
import com.lunabeestudio.stopcovid.core.utils.Event
import com.lunabeestudio.stopcovid.extension.collectDataWithLifecycle
import com.lunabeestudio.stopcovid.extension.countryCode
import com.lunabeestudio.stopcovid.extension.fullDescription
import com.lunabeestudio.stopcovid.extension.fullNameList
import com.lunabeestudio.stopcovid.extension.getString
import com.lunabeestudio.stopcovid.extension.infosDescription
import com.lunabeestudio.stopcovid.extension.injectionContainer
import com.lunabeestudio.stopcovid.extension.isAutoTest
import com.lunabeestudio.stopcovid.extension.isFrench
import com.lunabeestudio.stopcovid.extension.isFromMultiPassIssuer
import com.lunabeestudio.stopcovid.extension.isSignatureExpired
import com.lunabeestudio.stopcovid.extension.observeEventAndConsume
import com.lunabeestudio.stopcovid.extension.raw
import com.lunabeestudio.stopcovid.extension.safeNavigate
import com.lunabeestudio.stopcovid.extension.showErrorSnackBar
import com.lunabeestudio.stopcovid.extension.showSmartWallet
import com.lunabeestudio.stopcovid.extension.statusStringKey
import com.lunabeestudio.stopcovid.extension.tagStringKey
import com.lunabeestudio.stopcovid.extension.testResultIsNegative
import com.lunabeestudio.stopcovid.extension.titleDescription
import com.lunabeestudio.stopcovid.extension.toRaw
import com.lunabeestudio.stopcovid.extension.transformHeartEmoji
import com.lunabeestudio.stopcovid.extension.vaccineDose
import com.lunabeestudio.stopcovid.extension.vaccineMedicinalProduct
import com.lunabeestudio.stopcovid.fastitem.CertificateCardItem
import com.lunabeestudio.stopcovid.fastitem.bigTitleItem
import com.lunabeestudio.stopcovid.fastitem.certificateCardItem
import com.lunabeestudio.stopcovid.manager.ShareManager
import com.lunabeestudio.stopcovid.model.CertificateHeaderState
import com.lunabeestudio.stopcovid.model.EuropeanCertificate
import com.lunabeestudio.stopcovid.model.FrenchCertificate
import com.lunabeestudio.stopcovid.model.SmartWalletState
import com.lunabeestudio.stopcovid.model.VaccinationCertificate
import com.lunabeestudio.stopcovid.model.WalletCertificate
import com.lunabeestudio.stopcovid.model.WalletSection
import com.lunabeestudio.stopcovid.utils.lazyFast
import com.lunabeestudio.stopcovid.viewmodel.WalletCertificateListViewModel
import com.lunabeestudio.stopcovid.viewmodel.WalletCertificateListViewModelFactory
import com.mikepenz.fastadapter.GenericItem
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date

class WalletCertificateListFragment : MainFragment(), PagerTabFragment {

    private val barcodeEncoder: BarcodeEncoder = BarcodeEncoder()
    private val qrCodeSize: Int by lazy {
        R.dimen.qr_code_size.toDimensSize(requireContext()).toInt()
    }

    private val sharedPrefs: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(requireContext())
    }

    private val viewModel by viewModels<WalletCertificateListViewModel> {
        WalletCertificateListViewModelFactory(
            injectionContainer.configurationManager,
            injectionContainer.walletRepository,
            injectionContainer.getSmartWalletMapUseCase,
            injectionContainer.computeDccValidityUseCase,
            injectionContainer.isCertificateEUValidUseCase,
        )
    }

    private val longDateFormat by lazyFast { SimpleDateFormat.getDateInstance(DateFormat.LONG, getApplicationLocale()) }

    override fun getTitleKey(): String = "walletController.title"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.error.observe(viewLifecycleOwner) {
            showErrorSnackBar(
                when (it) {
                    is TACError -> it.getString(strings)
                    else -> AppError(AppError.Code.UNKNOWN, it.message ?: "").getString(strings)
                },
            )
        }
        viewModel.loading.observe(viewLifecycleOwner) { isLoading ->
            (activity as? MainActivity)?.showProgress(isLoading)
        }

        viewModel.certificates.collectDataWithLifecycle(viewLifecycleOwner) { result ->
            refreshScreen()

            if (result is TacResult.Success) {
                val scrollToCertificateId = arguments?.getString(ARGUMENTS_SCROLL_TO_CERTIFICATE_ID_KEY)
                arguments?.remove(ARGUMENTS_SCROLL_TO_CERTIFICATE_ID_KEY)
                if (scrollToCertificateId != null) {
                    result.data?.firstOrNull { certificate -> certificate.id == scrollToCertificateId }?.let(
                        ::scrollToCertificate,
                    )
                }
            }

            debugManager.logObserveCertificate(result.toRaw())
        }
        viewModel.smartWalletProfiles.collectDataWithLifecycle(viewLifecycleOwner) {
            refreshScreen()
        }

        viewModel.scrollEvent.observeEventAndConsume(viewLifecycleOwner, false, Event.NO_ID, ::scrollToCertificate)
    }

    override fun refreshScreen() {
        super.refreshScreen()
        (activity as? MainActivity)?.binding?.tabLayout?.isVisible = true
    }

    override suspend fun getItems(): List<GenericItem> {
        val items = ArrayList<GenericItem>()

        items += spaceItem {
            spaceRes = R.dimen.spacing_large
            identifier = items.size.toLong()
        }

        val certificates = viewModel.certificates.value.data

        val hasInvalidDcc = {
            certificates?.any { certificate ->
                val testResultIsNotNegative = (certificate as? EuropeanCertificate)?.greenCertificate?.testResultIsNegative == false
                val isSignatureExpired = (certificate as? EuropeanCertificate)?.isSignatureExpired == true

                testResultIsNotNegative || isSignatureExpired
            } == true
        }

        val hasBlacklistedCertificate = { certificates?.any { it.isBlacklisted == true } == true }

        val hasSmartWalletExpiredDcc = {
            viewModel.smartWalletProfiles.value?.any { (_, certificate) ->
                val smartWalletState = injectionContainer.getSmartWalletStateUseCase(certificate)
                smartWalletState !is SmartWalletState.Valid
            } == true
        }
        val hasExpiredDcc = {
            sharedPrefs.showSmartWallet &&
                injectionContainer.configurationManager.configuration.isSmartWalletOn &&
                hasSmartWalletExpiredDcc()
        }
        val hasErrorCertificate = hasInvalidDcc() || hasBlacklistedCertificate() || hasExpiredDcc()

        if (hasErrorCertificate) {
            items += captionItem {
                text = strings["walletController.certificateWarning"]
                identifier = text.hashCode().toLong()
            }
            items += spaceItem {
                spaceRes = R.dimen.spacing_large
                identifier = items.size.toLong()
            }
        }

        items += bigTitleItem {
            text = strings["walletController.favoriteCertificateSection.title"]
            identifier = text.hashCode().toLong()
        }

        val favoriteCertificates = viewModel.groupedCertificates.data?.get(WalletSection.FAVORITE)
        if (favoriteCertificates.isNullOrEmpty()) {
            items += captionItem {
                val spannedSubtitle = strings["walletController.favoriteCertificateSection.subtitle"]?.toSpannable()
                context?.let {
                    spannedSubtitle?.transformHeartEmoji(it)
                }
                spannedText = spannedSubtitle
                identifier = text.hashCode().toLong()
            }
            items += captionItem {
                val widgetInfoText = strings["walletController.favoriteCertificateSection.widget"]
                text = widgetInfoText
                identifier = text.hashCode().toLong()
            }
        }
        favoriteCertificates?.forEach { certificate ->
            items += certificateCardItemFromWalletDocument(certificate)
        }
        items += spaceItem {
            spaceRes = R.dimen.spacing_large
            identifier = items.size.toLong()
        }

        viewModel.groupedCertificates.data?.forEach {
            if (it.key != WalletSection.FAVORITE) {
                items += bigTitleItem {
                    text = strings[it.key.titleKey]
                    identifier = text.hashCode().toLong()
                }
                it.value.forEach { certificate ->
                    items += certificateCardItemFromWalletDocument(certificate)
                }
                items += spaceItem {
                    spaceRes = R.dimen.spacing_large
                    identifier = items.size.toLong()
                }
            }
        }

        return items
    }

    private fun scrollToCertificate(certificate: WalletCertificate) {
        binding?.recyclerView?.doOnNextLayout {
            val identifier = certificate.fastAdapterIdentifier()

            val position = fastAdapter.getItemById(identifier)?.second
            if (position != null) {
                binding?.recyclerView?.smoothScrollToPosition(position)
            } else {
                // Retry scroll once
                binding?.recyclerView?.postDelayed(
                    {
                        fastAdapter.getItemById(identifier)?.second?.let { position ->
                            binding?.recyclerView?.smoothScrollToPosition(position)
                        }
                    },
                    200,
                )
            }
        }
    }

    override fun onTabSelected() {
        binding?.recyclerView?.let { (activity as? MainActivity)?.binding?.appBarLayout?.setLiftOnScrollTargetView(it) }

        findParentFragmentByType<WalletContainerFragment>()?.let { walletContainerFragment ->
            walletContainerFragment.setupBottomAction(strings["walletController.addCertificate"]) {
                walletContainerFragment.findNavControllerOrNull()
                    ?.safeNavigate(
                        WalletContainerFragmentDirections.actionWalletContainerFragmentToWalletQRCodeFragment(),
                    )
            }
        }
    }

    private fun certificateCardItemFromWalletDocument(certificate: WalletCertificate): CertificateCardItem {
        val generateBarcode: () -> Bitmap
        var smartWalletState: SmartWalletState? = null
        var showSmartWalletState = false

        when (certificate) {
            is FrenchCertificate -> {
                generateBarcode = {
                    barcodeEncoder.encodeBitmap(
                        certificate.value,
                        BarcodeFormat.DATA_MATRIX,
                        qrCodeSize,
                        qrCodeSize,
                    )
                }
            }
            is EuropeanCertificate -> {
                generateBarcode = {
                    barcodeEncoder.encodeBitmap(
                        certificate.value,
                        BarcodeFormat.QR_CODE,
                        qrCodeSize,
                        qrCodeSize,
                    )
                }

                smartWalletState = injectionContainer.getSmartWalletStateUseCase(certificate)
                if (injectionContainer.configurationManager.configuration.isSmartWalletOn &&
                    sharedPrefs.showSmartWallet &&
                    (viewModel.smartWalletProfiles.value?.values?.any { it.id == certificate.id } == true)
                ) {
                    showSmartWalletState = true
                }
            }
        }

        val infoDescription = getInfoDescription(certificate, smartWalletState, showSmartWalletState)
        val warningDescription = getWarningDescription(certificate, smartWalletState, showSmartWalletState)
        val errorDescription = getErrorDescription(certificate, smartWalletState, showSmartWalletState)
        val description = getDescription(certificate, smartWalletState)

        val (headerState, headerText) = when {
            errorDescription.isNotBlank() -> Pair(CertificateHeaderState.ERROR, errorDescription)
            warningDescription.isNotBlank() -> Pair(CertificateHeaderState.WARNING, warningDescription)
            else -> Pair(CertificateHeaderState.INFO, infoDescription)
        }

        return certificateCardItem {
            this.generateBarcode = generateBarcode
            titleText = certificate.titleDescription(strings)
            nameText = certificate.fullNameList(injectionContainer.configurationManager.configuration)
            descriptionText = description
            this.headerText = headerText
            this.headerState = headerState
            share = strings["walletController.menu.share"]
            delete = strings["walletController.menu.delete"]
            readMoreText = strings["common.readNext"]
            onReadMoreClick = {
                findParentFragmentByType<WalletContainerFragment>()?.findNavControllerOrNull()?.safeNavigate(
                    WalletContainerFragmentDirections.actionWalletContainerFragmentToSimpleTextBottomSheetFragment(
                        headerText,
                        headerState,
                    ),
                )
            }

            when (certificate) {
                is FrenchCertificate -> {
                    tag1Text = strings[certificate.tagStringKey()]
                    tag2Text = (certificate as? VaccinationCertificate)?.statusStringKey()?.let(strings::get)
                }
                is EuropeanCertificate -> {
                    if (certificate.isSignatureExpired) {
                        tag2Text = strings["wallet.expired.pillTitle"]
                        tag2ColorRes = R.color.color_error
                    } else if (certificate.isFromMultiPassIssuer()) {
                        tag2Text = strings["wallet.tag.multiPass"]
                        tag2ColorRes = R.color.color_leather
                    }

                    if (certificate.type == WalletCertificateType.VACCINATION_EUROPE) {
                        certificate.greenCertificate.vaccineDose?.let { (first, second) ->
                            tag1Text = stringsFormat("wallet.proof.europe.vaccine.doses", first, second)
                        }
                    } else {
                        tag1Text = strings["enum.HCertType.${certificate.type.code}"]
                    }
                }
            }

            onShare = { barcodeBitmap ->
                findParentFragmentByType<WalletContainerFragment>()?.let { containerFragment ->
                    val uri = barcodeBitmap?.let { bitmap ->
                        ShareManager.getShareCaptureUriFromBitmap(requireContext(), bitmap, "qrCode")
                    }
                    val text = certificate.fullDescription(
                        strings = strings,
                        configuration = injectionContainer.configurationManager.configuration,
                        context = context,
                        smartWalletState = smartWalletState,
                    )
                        .takeIf { uri != null }
                    ShareManager.setupCertificateSharingBottomSheet(containerFragment, text) {
                        uri
                    }
                    containerFragment.findNavControllerOrNull()?.safeNavigate(
                        WalletContainerFragmentDirections.actionWalletContainerFragmentToCertificateSharingBottomSheetFragment(),
                    )
                }
            }
            onDelete = {
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle(strings["walletController.menu.delete.alert.title"])
                    .setMessage(strings["walletController.menu.delete.alert.message"])
                    .setNegativeButton(strings["common.cancel"], null)
                    .setPositiveButton(strings["common.confirm"]) { _, _ ->
                        viewModel.removeCertificate(certificate)
                        debugManager.logDeleteCertificates(certificate.raw, "from wallet")
                    }
                    .show()
            }
            onClick = {
                findParentFragmentByType<WalletContainerFragment>()?.navigateToFullscreenCertificate(certificate)
            }
            actionContentDescription = strings["accessibility.hint.otherActions"]
            favoriteContentDescription = strings["accessibility.hint.addToFavorite"]

            favoriteState = when {
                certificate is FrenchCertificate -> CertificateCardItem.FavoriteState.HIDDEN
                certificate is EuropeanCertificate && certificate.isFavorite -> CertificateCardItem.FavoriteState.CHECKED
                else -> CertificateCardItem.FavoriteState.NOT_CHECKED
            }

            onFavoriteClick = {
                if ((certificate as? EuropeanCertificate)?.isFavorite != true) {
                    binding?.recyclerView?.doOnNextLayout {
                        binding?.recyclerView?.smoothScrollToPosition(0)
                    }
                }

                (certificate as? EuropeanCertificate)?.let(viewModel::toggleFavorite)
            }
            bottomText = strings["walletController.favoriteCertificateSection.openFullScreen"]

            identifier = certificate.fastAdapterIdentifier()
        }
    }

    private fun getDescription(certificate: WalletCertificate, smartWalletState: SmartWalletState?): String {
        var description = certificate.infosDescription(
            strings = strings,
            configuration = injectionContainer.configurationManager.configuration,
            context = context,
            smartWalletState = smartWalletState,
        )
        (certificate as? EuropeanCertificate)?.let { europeanCertificate ->
            if (!viewModel.isCertificateEUValid(europeanCertificate)) {
                description += "\n${strings["wallet.proof.europe.test.negative.tagNotUe"]}"
            }
        }

        return description
    }

    private fun getInfoDescription(
        certificate: WalletCertificate,
        smartWalletState: SmartWalletState?,
        showSmartWalletState: Boolean,
    ): String {
        val eligibilityInfo =
            when {
                !showSmartWalletState -> null
                smartWalletState is SmartWalletState.Eligible -> {
                    val sb = StringBuilder()

                    sb.append(
                        (certificate as? EuropeanCertificate)?.greenCertificate?.vaccineMedicinalProduct?.let { vaccineProduct ->
                            stringsFormat(
                                "smartWallet.eligibility.info.${certificate.type.code}.$vaccineProduct",
                                certificate.firstName,
                            )
                        } ?: stringsFormat(
                            "smartWallet.eligibility.info.${certificate.type.code}",
                            certificate.firstName,
                        ),
                    )

                    stringsFormat(
                        "smartWallet.eligibility.info.${smartWalletState.eligibility?.labelId}",
                        certificate.firstName,
                    )?.let {
                        sb.append(" ")
                        sb.append(it)
                    }

                    sb.toString()
                }
                smartWalletState is SmartWalletState.EligibleSoon -> {
                    val sb = StringBuilder()

                    sb.append(
                        (certificate as? EuropeanCertificate)?.greenCertificate?.vaccineMedicinalProduct?.let { vaccineProduct ->
                            stringsFormat(
                                "smartWallet.eligibility.soon.info.${certificate.type.code}.$vaccineProduct",
                                certificate.firstName,
                                longDateFormat.format(smartWalletState.eligibility?.date ?: Date()),
                            )
                        } ?: stringsFormat(
                            "smartWallet.eligibility.soon.info.${certificate.type.code}",
                            certificate.firstName,
                            longDateFormat.format(smartWalletState.eligibility?.date ?: Date()),
                        ),
                    )

                    stringsFormat(
                        "smartWallet.eligibility.soon.info.${smartWalletState.eligibility?.labelId}",
                        certificate.firstName,
                        longDateFormat.format(smartWalletState.eligibility?.date ?: Date()),
                    )?.let {
                        sb.append(" ")
                        sb.append(it)
                    }

                    sb.toString()
                }
                else -> null
            }

        val greenCertificate = (certificate as? EuropeanCertificate)?.greenCertificate
        val foreignCountryInfo = if (greenCertificate?.isFrench == false) {
            strings["wallet.proof.europe.foreignCountryWarning.${greenCertificate.countryCode?.lowercase()}"]
        } else {
            null
        }
        return listOfNotNull(
            eligibilityInfo,
            foreignCountryInfo,
        )
            .joinToString("\n\n")
    }

    private fun getWarningDescription(
        certificate: WalletCertificate,
        smartWalletState: SmartWalletState?,
        showSmartWalletState: Boolean,
    ): String {
        val expirationWarning = if (showSmartWalletState && smartWalletState is SmartWalletState.ExpireSoon) {
            val label = (certificate as? EuropeanCertificate)?.greenCertificate?.vaccineMedicinalProduct?.let { vaccineProduct ->
                strings["smartWallet.expiration.soon.warning.${certificate.type.code}.$vaccineProduct"]
            } ?: strings["smartWallet.expiration.soon.warning.${certificate.type.code}"]

            label.formatOrNull(longDateFormat.format(smartWalletState.smartWalletValidity?.end ?: Date()))
        } else {
            null
        }
        val blacklistWarning = if (certificate.isBlacklisted == true) {
            strings["wallet.blacklist.warning"]
        } else {
            null
        }

        // Fix SIDEP has generated positive test instead of recovery
        val greenCertificate = (certificate as? EuropeanCertificate)?.greenCertificate
        val positiveSidepErrorWarning = if (greenCertificate?.testResultIsNegative == false) {
            strings["wallet.proof.europe.test.positiveSidepError"]
        } else {
            null
        }
        val autotestWarning = if (greenCertificate?.isAutoTest == true) {
            strings["wallet.autotest.warning"]
        } else {
            null
        }
        return listOfNotNull(
            expirationWarning,
            blacklistWarning,
            positiveSidepErrorWarning,
            autotestWarning,
        )
            .joinToString("\n\n")
    }

    private fun getErrorDescription(
        certificate: WalletCertificate,
        smartWalletState: SmartWalletState?,
        showSmartWalletState: Boolean,
    ): String {
        val expirationError = if (showSmartWalletState && smartWalletState is SmartWalletState.Expired) {
            val label = (certificate as? EuropeanCertificate)?.greenCertificate?.vaccineMedicinalProduct?.let { vaccineProduct ->
                strings["smartWallet.expiration.error.${certificate.type.code}.$vaccineProduct"]
            } ?: strings["smartWallet.expiration.error.${certificate.type.code}"]

            label.formatOrNull(longDateFormat.format(smartWalletState.smartWalletValidity?.end ?: Date()))
        } else {
            null
        }
        return listOfNotNull(
            expirationError,
        )
            .joinToString("\n\n")
    }

    private fun WalletCertificate.fastAdapterIdentifier(): Long = id.hashCode().toLong()

    companion object {
        private const val ARGUMENTS_SCROLL_TO_CERTIFICATE_ID_KEY = "ARGUMENTS_SCROLL_TO_CERTIFICATE_ID_KEY"

        fun getInstance(scrollToCertificateId: String?): WalletCertificateListFragment {
            return WalletCertificateListFragment().apply {
                arguments = bundleOf(
                    ARGUMENTS_SCROLL_TO_CERTIFICATE_ID_KEY to scrollToCertificateId,
                )
            }
        }
    }
}
