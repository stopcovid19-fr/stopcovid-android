/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/9 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.usecase

import android.content.Context
import android.content.SharedPreferences
import com.lunabeestudio.common.extension.getApplicationLanguage
import com.lunabeestudio.domain.syncmanager.InfoCenterSyncManager
import com.lunabeestudio.stopcovid.extension.lastInfoCenterRefresh
import dagger.hilt.android.qualifiers.ApplicationContext
import java.util.Date
import javax.inject.Inject

class RefreshInfoCenterUseCase @Inject constructor(
    @ApplicationContext private val context: Context,
    private val infoCenterSyncManager: InfoCenterSyncManager,
    private val sharedPrefs: SharedPreferences,
) {
    suspend operator fun invoke(): Date? {
        val lastUpdatedAt = infoCenterSyncManager.sync(
            localSuffix = context.getApplicationLanguage(),
        )
        lastUpdatedAt?.let(sharedPrefs::lastInfoCenterRefresh::set)

        return lastUpdatedAt
    }
}
