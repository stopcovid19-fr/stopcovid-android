/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/22 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.usecase

import android.content.SharedPreferences
import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.stopcovid.extension.getDefaultFigureLabel1
import com.lunabeestudio.stopcovid.extension.getDefaultFigureLabel2
import com.lunabeestudio.stopcovid.extension.keyFigureCompare1
import com.lunabeestudio.stopcovid.extension.keyFigureCompare2
import com.lunabeestudio.stopcovid.manager.ConfigurationManager

class GetKeyFiguresWithCompareUseCase(
    private val sharedPrefs: SharedPreferences,
    private val configurationManager: ConfigurationManager,
) {
    operator fun invoke(keyFigures: List<KeyFigure>): Pair<KeyFigure, KeyFigure>? {
        val keyFigure1 = keyFigures.firstOrNull {
            it.labelKey == sharedPrefs.keyFigureCompare1
        }

        val keyFigure2 = keyFigures.firstOrNull {
            it.labelKey == sharedPrefs.keyFigureCompare2
        }

        return if (keyFigure1 != null && keyFigure2 != null) {
            Pair(keyFigure1, keyFigure2)
        } else {
            val defaultFigureLabel1 = configurationManager.configuration.getDefaultFigureLabel1()
            val defaultFigureLabel2 = configurationManager.configuration.getDefaultFigureLabel2()

            // Reset user choice to default
            sharedPrefs.keyFigureCompare1 = defaultFigureLabel1
            sharedPrefs.keyFigureCompare2 = defaultFigureLabel2

            val defaultKeyFigure1 = keyFigures.firstOrNull {
                it.labelKey == defaultFigureLabel1
            }

            val defaultKeyFigure2 = keyFigures.firstOrNull {
                it.labelKey == defaultFigureLabel2
            }

            if (defaultKeyFigure1 != null && defaultKeyFigure2 != null) {
                Pair(defaultKeyFigure1, defaultKeyFigure2)
            } else {
                null
            }
        }
    }
}
