/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2021/6/10 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.usecase

import com.lunabeestudio.domain.extension.walletPublicKey
import com.lunabeestudio.domain.model.getForKeyId
import com.lunabeestudio.domain.repository.DccCertificateRepository
import com.lunabeestudio.error.AppError
import com.lunabeestudio.stopcovid.extension.isFrench
import com.lunabeestudio.stopcovid.manager.ConfigurationManager
import com.lunabeestudio.stopcovid.model.EuropeanCertificate
import com.lunabeestudio.stopcovid.model.FrenchCertificate
import com.lunabeestudio.stopcovid.model.WalletCertificate

class VerifyCertificateUseCase(
    private val configurationManager: ConfigurationManager,
    private val dccCertificateRepository: DccCertificateRepository,
) {
    operator fun invoke(walletCertificate: WalletCertificate) {
        val key: String? = when (walletCertificate) {
            is EuropeanCertificate -> dccCertificateRepository.certificates.getForKeyId(
                walletCertificate.keyCertificateId,
            )
            is FrenchCertificate -> configurationManager.configuration.walletPublicKey(
                walletCertificate.keyAuthority,
                walletCertificate.keyCertificateId,
            )
        }

        if (key != null) {
            walletCertificate.verifyKey(key)
        } else if ((walletCertificate as? EuropeanCertificate)?.greenCertificate?.isFrench == true ||
            walletCertificate !is EuropeanCertificate
        ) {
            // Only check French certificates
            throw AppError(AppError.Code.WALLET_CERTIFICATE_UNKNOWN_ERROR)
        }
    }
}
