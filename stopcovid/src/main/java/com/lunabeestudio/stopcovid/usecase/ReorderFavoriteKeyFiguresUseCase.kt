/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/9 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.usecase

import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.repository.KeyFigureRepository

class ReorderFavoriteKeyFiguresUseCase(
    private val keyFiguresRepository: KeyFigureRepository,
) {
    suspend operator fun invoke(favList: List<KeyFigure>?, oldPosition: Int, newPosition: Int) {
        favList?.toMutableList()?.apply {
            val movedKeyFigure = removeAt(oldPosition)
            val prevOrder = getOrNull(newPosition - 1)?.favoriteOrder ?: (first().favoriteOrder - 1f)
            val nextOrder = getOrNull(newPosition)?.favoriteOrder ?: (last().favoriteOrder + 1f)
            keyFiguresRepository.saveFavorite(movedKeyFigure.copy(favoriteOrder = (prevOrder + nextOrder) / 2f))
        }
    }
}
