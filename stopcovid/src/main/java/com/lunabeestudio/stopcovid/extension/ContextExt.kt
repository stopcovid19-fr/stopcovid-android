/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/13/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.extension

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import com.lunabeestudio.domain.repository.KeyFigureRepository
import com.lunabeestudio.stopcovid.Constants
import com.lunabeestudio.stopcovid.InjectionContainer
import com.lunabeestudio.stopcovid.TousAntiCovid
import com.lunabeestudio.stopcovid.core.manager.StringsManager
import com.lunabeestudio.stopcovid.manager.ConfigurationManager
import com.lunabeestudio.stopcovid.repository.WalletRepository
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.coroutines.withTimeoutOrNull
import kotlin.coroutines.resume

private val Context.injectionContainer: InjectionContainer
    get() = (applicationContext as TousAntiCovid).injectionContainer

fun Context.configurationDataSource(): ConfigurationManager = injectionContainer.configurationManager
fun Context.stringsManager(): StringsManager = injectionContainer.stringsManager
fun Context.walletRepository(): WalletRepository = injectionContainer.walletRepository
fun Context.keyFigureRepository(): KeyFigureRepository = injectionContainer.keyFigureRepository

/**
 * Start an explicit intent with text to share
 *
 * @param text The text to share
 */
fun Context.startTextIntent(text: String) {
    val sendIntent: Intent = Intent().apply {
        action = Intent.ACTION_SEND
        putExtra(Intent.EXTRA_TEXT, text)
        type = "text/plain"
    }

    val shareIntent = Intent.createChooser(sendIntent, null)
    startActivity(shareIntent)
}

suspend fun Context.isLowStorage(): Boolean {
    return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
        withTimeoutOrNull(2_000L) {
            suspendCancellableCoroutine { continuation ->
                registerReceiver(
                    object : BroadcastReceiver() {
                        init {
                            continuation.invokeOnCancellation {
                                unregisterReceiver(this)
                            }
                        }

                        override fun onReceive(context: Context?, intent: Intent?) {
                            unregisterReceiver(this)
                            continuation.resume(true)
                        }
                    },
                    @Suppress("DEPRECATION")
                    IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW),
                )
            }
        } ?: false
    } else {
        cacheDir?.let { cacheDir -> cacheDir.freeSpace / 1024 / 1024 < Constants.Android.STORAGE_THRESHOLD_MB } ?: false
    }
}

fun Context.getBuildNumber(): Long {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        packageManager.getPackageInfo(packageName, PackageManager.PackageInfoFlags.of(0)).longVersionCode
    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
        @Suppress("DEPRECATION")
        packageManager.getPackageInfo(packageName, 0).longVersionCode
    } else {
        @Suppress("DEPRECATION")
        packageManager.getPackageInfo(packageName, 0).versionCode.toLong()
    }
}
