/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.extension

import android.annotation.SuppressLint
import android.content.Context
import android.text.SpannableStringBuilder
import com.lunabeestudio.domain.extension.yearMonthDayUsParser
import com.lunabeestudio.domain.model.Configuration
import com.lunabeestudio.domain.model.RawWalletCertificate
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.domain.model.WalletCertificateType
import com.lunabeestudio.domain.model.smartwallet.SmartWalletValidity
import com.lunabeestudio.stopcovid.core.extension.getApplicationLocale
import com.lunabeestudio.stopcovid.core.extension.stringsFormat
import com.lunabeestudio.stopcovid.core.manager.LocalizedStrings
import com.lunabeestudio.stopcovid.model.EuropeanCertificate
import com.lunabeestudio.stopcovid.model.FrenchCertificate
import com.lunabeestudio.stopcovid.model.SanitaryCertificate
import com.lunabeestudio.stopcovid.model.SmartWalletState
import com.lunabeestudio.stopcovid.model.VaccinationCertificate
import com.lunabeestudio.stopcovid.model.WalletCertificate
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import java.util.concurrent.TimeUnit
import kotlin.time.Duration.Companion.hours
import timber.log.Timber

fun WalletCertificate.fullNameList(configuration: Configuration): String {
    val prefix =
        configuration.dccKidsEmoji?.let { dccKidsEmoji ->
            birthDate()?.let { birthDate ->
                val maxKidEmojiCal = Calendar.getInstance().apply {
                    time = birthDate
                    add(Calendar.YEAR, dccKidsEmoji.age)
                }
                val nowCal = Calendar.getInstance()
                if (nowCal.before(maxKidEmojiCal)) {
                    val emojiIdx = (rawBirthDate() + fullName()).iOSCommonHash().mod(dccKidsEmoji.emojis.size)
                    dccKidsEmoji.emojis.getOrNull(emojiIdx).orEmpty() + " "
                } else {
                    null
                }
            }
        }.orEmpty()

    return (prefix + fullNameUppercase()).trim()
}

fun WalletCertificate.rawBirthDate(): String? {
    return when (this) {
        is EuropeanCertificate -> greenCertificate.dateOfBirth
        is FrenchCertificate -> birthDate
    }
}

fun WalletCertificate.birthDate(): Date? {
    return when (this) {
        is EuropeanCertificate -> yearMonthDayUsParser().parseOrNull(greenCertificate.dateOfBirth)
        is FrenchCertificate -> birthDate?.let { dayMonthYearUsParser().parseOrNull(it) }
    }
}

fun WalletCertificate.fullNameUppercase(): String =
    fullName().uppercase()

fun WalletCertificate.fullName(): String =
    "${firstName.orEmpty()} ${name.orEmpty()}".trim()

fun WalletCertificate.titleDescription(strings: LocalizedStrings): String {
    val titleBuilder = SpannableStringBuilder()
    if ((this as? EuropeanCertificate)?.greenCertificate?.isFrench == false) {
        titleBuilder.append(this.greenCertificate.countryCode?.countryCodeToFlagEmoji?.plus(" ") ?: "")
    }
    titleBuilder.append(
        when (this) {
            is EuropeanCertificate -> {
                if (this.type == WalletCertificateType.MULTI_PASS) {
                    strings["wallet.proof.${type.code}.title"] ?: ""
                } else {
                    strings["wallet.proof.europe.${type.code}.title"] ?: ""
                }
            }
            else -> strings["wallet.proof.${type.stringKey}.title"] ?: ""
        }.trim(),
    )
    return titleBuilder.toString()
}

fun WalletCertificate.infosDescription(
    strings: LocalizedStrings,
    configuration: Configuration,
    context: Context?,
    smartWalletState: SmartWalletState?,
): String {
    val text = when (type) {
        WalletCertificateType.SANITARY,
        WalletCertificateType.VACCINATION,
        -> strings["wallet.proof.${type.stringKey}.infos"]
        WalletCertificateType.MULTI_PASS,
        WalletCertificateType.SANITARY_EUROPE,
        WalletCertificateType.EXEMPTION,
        WalletCertificateType.VACCINATION_EUROPE,
        WalletCertificateType.RECOVERY_EUROPE,
        -> strings["wallet.proof.europe.${type.code}.identity"]
    }
    return formatText(strings, configuration, text, false, context, smartWalletState).trim()
}

fun WalletCertificate.fullDescription(
    strings: LocalizedStrings,
    configuration: Configuration,
    context: Context?,
    smartWalletState: SmartWalletState?,
): String {
    val text = when (this) {
        is EuropeanCertificate -> {
            if (this.type == WalletCertificateType.MULTI_PASS) {
                strings["wallet.proof.${type.code}.description"]
            } else {
                strings["wallet.proof.europe.${type.code}.description"]
            }
        }
        else -> strings["wallet.proof.${type.stringKey}.description"]
    }
    return formatText(strings, configuration, text, true, context, smartWalletState).trim()
}

@SuppressLint("SimpleDateFormat")
private fun WalletCertificate.formatText(
    strings: LocalizedStrings,
    configuration: Configuration,
    textToFormat: String?,
    shouldAddFlag: Boolean,
    context: Context?,
    smartWalletState: SmartWalletState?,
): String {
    val dateFormat = shortDateFormat(context)
    val analysisDateFormat = shortDateTimeFormat(context)
    return when (this) {
        is FrenchCertificate -> formatFrenchCertificateText(
            strings = strings,
            configuration = configuration,
            textToFormat = textToFormat,
            dateFormat = dateFormat,
            analysisDateFormat = analysisDateFormat,
        )
        is EuropeanCertificate -> {
            val descriptionBuilder = StringBuilder()
            if (!this.greenCertificate.isFrench && shouldAddFlag) {
                descriptionBuilder.append(this.greenCertificate.countryCode?.countryCodeToFlagEmoji ?: "")
            }

            descriptionBuilder.append(
                formatDccText(
                    textToFormat,
                    strings,
                    dateFormat,
                    analysisDateFormat,
                    false,
                    smartWalletState,
                ),
            )

            if (isSignatureExpired) {
                descriptionBuilder.append(
                    expirationString(
                        strings = strings,
                        dateFormat = dateFormat,
                    ).let { "\n$it" },
                )
            } else {
                descriptionBuilder.append(
                    validityString(
                        configuration = configuration,
                        strings = strings,
                        forceEnglish = false,
                        smartWalletState = smartWalletState,
                        dateFormat = dateFormat,
                        analysisDateFormat = analysisDateFormat,
                    ).let { "\n$it" },
                )
            }

            descriptionBuilder.toString()
        }
    }
}

fun EuropeanCertificate.expirationString(strings: Map<String, String>, dateFormat: SimpleDateFormat): String {
    return strings.stringsFormat("europeanCertificate.fullscreen.expirationDate", dateFormat.format(expirationTime)).orEmpty()
}

private fun FrenchCertificate.formatFrenchCertificateText(
    strings: LocalizedStrings,
    configuration: Configuration,
    textToFormat: String?,
    dateFormat: SimpleDateFormat,
    analysisDateFormat: SimpleDateFormat,
): String {
    var text = textToFormat
    return when (this) {
        is SanitaryCertificate -> {
            text = text?.replace("<${SanitaryCertificate.SanitaryCertificateFields.FIRST_NAME.code}>", firstName.orNA())
            text = text?.replace("<${SanitaryCertificate.SanitaryCertificateFields.NAME.code}>", name.orNA())
            text = text?.replace(
                "<${SanitaryCertificate.SanitaryCertificateFields.BIRTH_DATE.code}>",
                birthDate.orNA(),
            )

            val genderString =
                strings["wallet.proof.${type.stringKey}.${SanitaryCertificate.SanitaryCertificateFields.GENDER.code}.$gender"]
            text = text?.replace("<${SanitaryCertificate.SanitaryCertificateFields.GENDER.code}>", genderString.orNA())

            val analysisCodeString = strings.stringsFormat(
                "wallet.proof.${type.stringKey}.loinc.$analysisCode",
                analysisCode,
            )
            text = text?.replace(
                "<${SanitaryCertificate.SanitaryCertificateFields.ANALYSIS_CODE.code}>",
                analysisCodeString ?: "LOINC:$analysisCode",
            )

            val dateString = analysisDate?.let { analysisDateFormat.format(it) }.orNA()
            text = text?.replace("<${SanitaryCertificate.SanitaryCertificateFields.ANALYSIS_DATE.code}>", dateString)

            val resultString =
                strings["wallet.proof.${type.stringKey}.${SanitaryCertificate.SanitaryCertificateFields.TEST_RESULT.code}.$testResult"]
            text = text?.replace(
                oldValue = "<${SanitaryCertificate.SanitaryCertificateFields.TEST_RESULT.code}>",
                newValue = resultString.orNA(),
            )

            if (testResult == "N") {
                val timeIndicator = validityString(
                    configuration = configuration,
                    strings = strings,
                    forceEnglish = false,
                    smartWalletState = null,
                    dateFormat = dateFormat,
                    analysisDateFormat = analysisDateFormat,
                )
                text = text?.plus("\n$timeIndicator")
            }

            text ?: ""
        }
        is VaccinationCertificate -> {
            text = text?.replace(
                "<${VaccinationCertificate.VaccinationCertificateFields.FIRST_NAME.code}>",
                firstName.orNA(),
            )
            text = text?.replace("<${VaccinationCertificate.VaccinationCertificateFields.NAME.code}>", name.orNA())
            text = text?.replace(
                "<${VaccinationCertificate.VaccinationCertificateFields.BIRTH_DATE.code}>",
                birthDate.orNA(),
            )
            text = text?.replace(
                "<${VaccinationCertificate.VaccinationCertificateFields.DISEASE_NAME.code}>",
                diseaseName.orNA(),
            )
            text = text?.replace(
                "<${VaccinationCertificate.VaccinationCertificateFields.PROPHYLACTIC_AGENT.code}>",
                prophylacticAgent.orNA(),
            )
            text = text?.replace(
                "<${VaccinationCertificate.VaccinationCertificateFields.VACCINE_NAME.code}>",
                vaccineName.orNA(),
            )
            text = text?.replace(
                "<${VaccinationCertificate.VaccinationCertificateFields.VACCINE_MAKER.code}>",
                vaccineMaker.orNA(),
            )
            text = text?.replace(
                "<${VaccinationCertificate.VaccinationCertificateFields.LAST_VACCINATION_STATE_RANK.code}>",
                lastVaccinationStateRank.orNA(),
            )
            text = text?.replace(
                "<${VaccinationCertificate.VaccinationCertificateFields.COMPLETE_CYCLE_DOSE_COUNT.code}>",
                completeCycleDosesCount.orNA(),
            )

            text = text?.replace(
                "<${VaccinationCertificate.VaccinationCertificateFields.LAST_VACCINATION_DATE.code}>",
                lastVaccinationDate?.let { dateFormat.format(it) }.orNA(),
            )

            val vaxCode = VaccinationCertificate.VaccinationCertificateFields.VACCINATION_CYCLE_STATE.code
            val vaccinationState = strings["wallet.proof.${type.stringKey}.$vaxCode.$vaccinationCycleState"]
            text = text?.replace("<$vaxCode>", vaccinationState.orNA())
            text ?: ""
        }
    }
}

fun EuropeanCertificate.fullScreenBorderDescription(
    strings: LocalizedStrings,
    configuration: Configuration,
    smartWalletState: SmartWalletState?,
): String {
    val descriptionBuilder = StringBuilder()
    val dateFormat = shortDateFormat(Locale.ENGLISH)
    val analysisDateFormat = shortDateTimeFormat(Locale.ENGLISH)
    descriptionBuilder.append(
        formatDccText(
            inputText = strings["europeanCertificate.fullscreen.englishDescription.${type.code}"],
            strings = strings,
            dateFormat = dateFormat,
            dateTimeFormat = analysisDateFormat,
            forceEnglish = true,
            smartWalletState = smartWalletState,
        ),
    )

    if (!isSignatureExpired) {
        descriptionBuilder.append(
            validityString(
                configuration = configuration,
                strings = strings,
                forceEnglish = true,
                smartWalletState = null,
                dateFormat = dateFormat,
                analysisDateFormat = analysisDateFormat,
            ).let { "\n$it" },
        )
    }

    return descriptionBuilder.toString()
}

fun EuropeanCertificate.fullScreenBorderFooter(
    strings: LocalizedStrings,
    context: Context?,
): String {
    val sb = StringBuilder()
        .appendLine(sha256)
        .appendLine(expirationString(strings, shortDateFormat(context)))
    return sb.toString().trimEnd()
}

fun EuropeanCertificate.fullScreenDescription(
    strings: LocalizedStrings,
    context: Context,
    smartWalletState: SmartWalletState,
): String {
    return when (this.type) {
        WalletCertificateType.SANITARY,
        WalletCertificateType.VACCINATION,
        WalletCertificateType.SANITARY_EUROPE,
        WalletCertificateType.VACCINATION_EUROPE,
        WalletCertificateType.RECOVERY_EUROPE,
        WalletCertificateType.EXEMPTION,
        -> fullNameUppercase()
        WalletCertificateType.MULTI_PASS -> {
            val dateFormat = shortDateFormat(context)
            val dateTimeFormat = shortDateTimeFormat(context)
            formatDccText(
                // Fix <X_DATE> vs <X_DATE_TIME>
                inputText = strings["multiPassCertificate.fullscreen"]
                    ?.replace("<TO_DATE>", "<TO_DATE_TIME>")
                    ?.replace("<FROM_DATE>", "<FROM_DATE_TIME>"),
                strings = strings,
                dateFormat = dateFormat,
                dateTimeFormat = dateTimeFormat,
                forceEnglish = false,
                smartWalletState = smartWalletState,
            )
        }
    }
}

fun EuropeanCertificate.multipassPickerCaption(
    strings: LocalizedStrings,
    context: Context,
    smartWalletState: SmartWalletState,
    configuration: Configuration,
): String {
    val dateFormat = shortDateFormat(context)
    val dateTimeFormat = shortDateTimeFormat(context)

    val typeAndDate = strings["multiPass.selectionScreen.${this.type.code}.description"].takeIf { !it.isNullOrEmpty() }
    val validity = if (isSignatureExpired) {
        null
    } else {
        validityString(
            configuration = configuration,
            strings = strings,
            forceEnglish = false,
            smartWalletState = smartWalletState,
            dateFormat = dateFormat,
            analysisDateFormat = dateTimeFormat,
        ).takeIf { it.isNotEmpty() }
    }

    return formatDccText(
        inputText = listOfNotNull(typeAndDate, validity).joinToString("\n"),
        strings = strings,
        dateFormat = dateFormat,
        dateTimeFormat = dateTimeFormat,
        forceEnglish = false,
        smartWalletState = smartWalletState,
    )
}

private fun shortDateTimeFormat(context: Context?): SimpleDateFormat = shortDateTimeFormat(
    context.getApplicationLocale(),
)
private fun shortDateTimeFormat(locale: Locale): SimpleDateFormat = SimpleDateFormat("d MMM yyyy, HH:mm", locale)
fun shortDateFormat(context: Context?): SimpleDateFormat = shortDateFormat(context.getApplicationLocale())
private fun shortDateFormat(locale: Locale): SimpleDateFormat = SimpleDateFormat("d MMM yyyy", locale)

fun EuropeanCertificate.formatDccText(
    inputText: String?,
    strings: LocalizedStrings,
    dateFormat: DateFormat,
    dateTimeFormat: DateFormat,
    forceEnglish: Boolean,
    smartWalletState: SmartWalletState?,
): String {
    var formattedText = inputText

    when (this.type) {
        WalletCertificateType.SANITARY,
        WalletCertificateType.VACCINATION,
        -> {
            Timber.e("Unexpected type ${this.type} with ${this.javaClass.simpleName}")
        }
        WalletCertificateType.VACCINATION_EUROPE -> {
            val vaccineName = this.greenCertificate.vaccineMedicinalProduct?.let { strings["vac.product.$it"] ?: it }
            val vaccineDoseString = this.greenCertificate.vaccineDose?.let { "${it.first}/${it.second}" }

            formattedText = formattedText?.replace("<VACCINE_NAME>", vaccineName.toString().orNA())
            formattedText = formattedText?.replace("<VACCINE_DOSES>", vaccineDoseString.orEmpty())
            formattedText = formattedText?.replace(
                "<DATE>",
                this.greenCertificate.vaccineDate?.let(dateFormat::format).orNA(),
            )
        }
        WalletCertificateType.RECOVERY_EUROPE -> {
            formattedText = formattedText?.replace(
                "<DATE>",
                this.greenCertificate.recoveryDateOfFirstPositiveTest?.let(dateFormat::format).orNA(),
            )
        }
        WalletCertificateType.SANITARY_EUROPE -> {
            val testDateFormat: DateFormat = if (greenCertificate.testResultIsNegative != true) {
                dateFormat
            } else {
                dateTimeFormat
            }

            val testName = this.greenCertificate.testType?.let {
                val testNameStringKey = StringBuilder("test.man.")
                if (forceEnglish) {
                    testNameStringKey.append("englishDescription.")
                }
                testNameStringKey.append(it)
                strings[testNameStringKey.toString()]
            }
            formattedText = formattedText?.replace("<ANALYSIS_CODE>", testName.orNA())

            val testResult = this.greenCertificate.testResultCode?.let {
                val testResultStringKey = StringBuilder("wallet.proof.europe.test.")
                if (forceEnglish) {
                    testResultStringKey.append("englishDescription.")
                }
                testResultStringKey.append(it)
                strings[testResultStringKey.toString()]
            }
            formattedText = formattedText?.replace("<ANALYSIS_RESULT>", testResult.orNA())
            formattedText = formattedText?.replace(
                "<DATE>",
                this.greenCertificate.testDateTimeOfCollection?.let(testDateFormat::format).orNA(),
            )
        }
        WalletCertificateType.EXEMPTION,
        WalletCertificateType.MULTI_PASS,
        -> {
            /* no-op */
        }
    }

    // Generic fallback placeholder
    formattedText = formattedText?.replace("<DATE>", dateTimeFormat.format(this.timestamp))
    formattedText = formattedText?.replace("<FULL_NAME>", fullNameUppercase())
    formattedText = formattedText?.replace("<FIRSTNAME>", firstName.orNA())
    formattedText = formattedText?.replace("<LASTNAME>", name.orNA())
    formattedText = formattedText?.replace(
        "<BIRTHDATE>",
        this.birthDate()?.let(dateFormat::format) ?: greenCertificate.dateOfBirth,
    )
    formattedText = formattedText?.replace(
        "<FROM_DATE>",
        smartWalletState?.smartWalletValidity?.start?.let(dateFormat::format).orNA(),
    )
    formattedText = formattedText?.replace(
        "<FROM_DATE_TIME>",
        smartWalletState?.smartWalletValidity?.start?.let(dateTimeFormat::format).orNA(),
    )
    formattedText = formattedText?.replace(
        "<TO_DATE>",
        smartWalletState?.smartWalletValidity?.end?.let(dateFormat::format).orNA(),
    )
    formattedText = formattedText?.replace(
        "<TO_DATE_TIME>",
        smartWalletState?.smartWalletValidity?.end?.let(dateTimeFormat::format).orNA(),
    )

    return formattedText.orEmpty()
}

fun VaccinationCertificate.statusStringKey(): String {
    val vaxCode = VaccinationCertificate.VaccinationCertificateFields.VACCINATION_CYCLE_STATE.code
    return "wallet.proof.${type.stringKey}.$vaxCode.$vaccinationCycleState"
}

fun WalletCertificate.tagStringKey(): String {
    return "wallet.proof.${type.stringKey}.pillTitle"
}

private fun WalletCertificate.validityString(
    configuration: Configuration,
    strings: LocalizedStrings,
    forceEnglish: Boolean,
    smartWalletState: SmartWalletState?,
    dateFormat: SimpleDateFormat,
    analysisDateFormat: SimpleDateFormat,
): String {
    val isNotNegativeTest =
        type != WalletCertificateType.SANITARY_EUROPE || (this as? EuropeanCertificate)?.greenCertificate?.testResultIsNegative != true

    return if (this is EuropeanCertificate && isNotNegativeTest) {
        val code = if (type == WalletCertificateType.SANITARY_EUROPE &&
            greenCertificate.testResultIsNegative == false
        ) {
            "positiveTest"
        } else {
            type.code
        }

        smartWalletState?.let { state ->
            state.smartWalletValidity?.let { validity ->
                val validityStart = validity.start
                val validityEnd = validity.end

                when {
                    validityEnd?.future() == false -> "smartwallet.$code.valid.none"
                    validityStart != null && validityEnd != null -> "smartwallet.$code.valid.startend"
                    validityStart != null && validityEnd == null -> "smartwallet.$code.valid.start"
                    validityStart == null && validityEnd != null -> "smartwallet.$code.valid.end"
                    else -> null
                }
            }?.let { key ->
                formatDccText(
                    strings[key],
                    strings,
                    dateFormat,
                    analysisDateFormat,
                    false,
                    state,
                )
            } ?: strings["smartwallet.$code.valid.none"]
        }.orEmpty()
    } else {
        val testCertificateValidityThresholds = configuration.testCertificateValidityThresholds
        val timeSinceCreation = System.currentTimeMillis() - timestamp
        val validityThresholdInHours = testCertificateValidityThresholds
            .filter { TimeUnit.HOURS.toMillis(it.toLong()) > timeSinceCreation }
            .minOrNull()

        val validityStringKeyBuilder = StringBuilder("wallet.proof.")

        if (forceEnglish) {
            validityStringKeyBuilder.append("englishDescription.")
        }

        if (validityThresholdInHours != null) {
            validityStringKeyBuilder.append("lessThanSpecificHours")
            strings.stringsFormat(validityStringKeyBuilder.toString(), validityThresholdInHours)
        } else {
            validityStringKeyBuilder.append("moreThanSpecificHours")
            val maxValidityInHours = testCertificateValidityThresholds.maxOrNull() ?: 0
            strings.stringsFormat(validityStringKeyBuilder.toString(), maxValidityInHours)
        }.orEmpty()
    }
}

val WalletCertificate.raw: RawWalletCertificate
    get() = RawWalletCertificate(
        id = id,
        type = type,
        value = value,
        timestamp = timestamp,
        isFavorite = (this as? EuropeanCertificate)?.isFavorite ?: false,
        rootWalletCertificateId = (this as? EuropeanCertificate)?.rootWalletCertificateId,
        isBlacklisted = isBlacklisted,
    )

fun WalletCertificate.isEligibleForSmartWallet(): Boolean {
    if (this !is EuropeanCertificate) return false

    val isCompleteVaccine = isCompleteVaccine()
    val isRecoveryOrTestPositive = greenCertificate.isRecoveryOrTestPositive

    return (isCompleteVaccine || isRecoveryOrTestPositive) && isBlacklisted != true && !isSignatureExpired
}

fun EuropeanCertificate.isCompleteVaccine(): Boolean = (
    type == WalletCertificateType.VACCINATION_EUROPE &&
        (greenCertificate.vaccineDose?.let { (first, second) -> first >= second } == true)
    )

val EuropeanCertificate.isSignatureExpired: Boolean
    get() = expirationTime < System.currentTimeMillis()

fun TacResult<List<WalletCertificate>>.toRaw(): TacResult<List<RawWalletCertificate>> {
    return when (this) {
        is TacResult.Failure -> TacResult.Failure(throwable, failureData?.map { it.raw })
        is TacResult.Loading -> TacResult.Loading(partialData?.map { it.raw }, progress)
        is TacResult.Success -> TacResult.Success(successData.map { it.raw })
    }
}

fun EuropeanCertificate.isNegativeTestValid(configuration: Configuration): Boolean =
    if (greenCertificate.testResultIsNegative == true) {
        configuration.activityPassSkipNegTestHours.hours.inWholeMilliseconds > (System.currentTimeMillis() - timestamp)
    } else {
        false
    }

fun EuropeanCertificate.isExpired(configuration: Configuration, validity: SmartWalletValidity?): Boolean = isSignatureExpired || when {
    validity != null -> (validity.end?.time ?: Long.MAX_VALUE) < System.currentTimeMillis()
    greenCertificate.testResultIsNegative == true -> !isNegativeTestValid(configuration)
    else -> false
}

fun EuropeanCertificate.isFromMultiPassIssuer(): Boolean {
    return greenCertificate.getDgci().startsWith("URN:UVCI:01:FR:DGSAG/")
}
