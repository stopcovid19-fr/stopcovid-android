/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2021/01/12 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.extension

import com.lunabeestudio.stopcovid.model.EuropeanCertificate

fun EuropeanCertificate.smartWalletProfileId(): String {
    return ((firstName ?: name).orEmpty() + greenCertificate.dateOfBirth).uppercase()
}

fun EuropeanCertificate.multipassProfileId(): String {
    return (
        greenCertificate.person.givenName
            ?.split(Regex("\\s+|-+"))
            ?.firstOrNull()
            ?.uppercase()
            ?: greenCertificate.person.standardisedFamilyName
                .trimEnd('<')
        ) +
        greenCertificate.dateOfBirth
}
