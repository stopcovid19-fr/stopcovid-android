/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/2/14 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.extension

import androidx.fragment.app.FragmentContainerView
import androidx.navigation.NavController
import androidx.navigation.findNavController
import timber.log.Timber

/**
 * Find a [NavController] associated with a [FragmentContainerView].
 */
fun FragmentContainerView.findNavControllerOrNull(): NavController? =
    try {
        findNavController()
    } catch (e: IllegalStateException) {
        Timber.e(e, "Failed to find the NavController")
        null
    }
