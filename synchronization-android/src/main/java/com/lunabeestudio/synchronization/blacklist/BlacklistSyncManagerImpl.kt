/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/2 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.synchronization.blacklist

import com.lunabeestudio.domain.di.BlacklistBaseUrl
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.domain.syncmanager.BlacklistSyncManager
import com.lunabeestudio.local.blacklist.BlacklistPrefixDao
import com.lunabeestudio.local.blacklist.BlacklistPrefixRoom
import com.lunabeestudio.remote.RetrofitClient
import com.lunabeestudio.remote.extension.fromCacheOrEtags
import com.lunabeestudio.remote.blacklist.BlacklistApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import timber.log.Timber
import javax.inject.Inject

class BlacklistSyncManagerImpl @Inject constructor(
    @BlacklistBaseUrl baseUrl: String,
    private val blacklistPrefixDao: BlacklistPrefixDao,
    okHttpClient: OkHttpClient,
) : BlacklistSyncManager {
    private val api: BlacklistApi = RetrofitClient.getProtoService(baseUrl, BlacklistApi::class.java, okHttpClient)

    override fun syncIndexes(): Flow<TacResult<Unit?>> {
        return flow {
            try {
                val response = api.getIndex()
                when {
                    response.fromCacheOrEtags -> emit(TacResult.Success(null))
                    response.isSuccessful -> {
                        val prefixes = response.body()?.pList?.map {
                            BlacklistPrefixRoom(it)
                        }.orEmpty()
                        withContext(Dispatchers.IO) {
                            blacklistPrefixDao.updatePrefixes(prefixes)
                        }
                        emit(TacResult.Success(Unit))
                    }
                    else -> {
                        Timber.e("Index synchronization failed")
                        emit(TacResult.Failure())
                    }
                }
            } catch (e: Exception) {
                Timber.e(e)
                emit(TacResult.Failure(e))
            }
        }.onStart {
            emit(TacResult.Loading())
        }
    }
}
