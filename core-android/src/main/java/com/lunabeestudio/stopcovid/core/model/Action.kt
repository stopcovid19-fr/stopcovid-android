/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/18 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.core.model

import android.view.View
import androidx.annotation.DrawableRes

data class Action(
    @DrawableRes
    val icon: Int? = null,

    val label: String? = null,
    val showBadge: Boolean = false,
    val loading: Boolean = false,
    val showArrow: Boolean = true,
    val onClickListener: View.OnClickListener?,
)
