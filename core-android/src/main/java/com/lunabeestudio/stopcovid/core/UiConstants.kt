/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/13/05 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.core

object UiConstants {

    object SharedPrefs {
        const val LAST_STRINGS_REFRESH: String = "Last.Strings.Refresh"
    }

    enum class Notification(val channelId: String, val notificationId: Int) {
        UPGRADE("upgrade", 4),
        NEWS("news", 7),
        CERTIFICATE_REMINDER("reminder", 10),
        SMART_WALLET("smartWallet", 12),
    }

    const val DAY_MONTH_DATE_PATTERN: String = "dd MMMM"

    const val DAY_SHORT_MONTH_YEAR_DATE_PATTERN: String = "dd MMM yyyy"
    const val DAY_FULL_MONTH_YEAR_DATE_PATTERN: String = "dd MMMM yyyy"

    object KeyFigure {
        val COLOR_MIN_COMPARISON: ComparisonColor = ComparisonColor(
            darkColor = "#00AC8C",
            lightColor = "#5C9390",
        )
        val COLOR_MAX_COMPARISON: ComparisonColor = ComparisonColor(
            darkColor = "#DA8FFF",
            lightColor = "#815C93",
        )

        data class ComparisonColor(
            val darkColor: String,
            val lightColor: String,
        ) {
            fun getColorFromTheme(isDarkMode: Boolean): String = if (isDarkMode) darkColor else lightColor
        }
    }

    object ImageView {
        const val MaxSize: Int = 2000
    }
}
