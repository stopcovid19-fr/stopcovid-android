/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2020/17/12 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.stopcovid.core.model

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import com.lunabeestudio.domain.model.Configuration
import com.lunabeestudio.domain.model.WalletPublicKey
import com.lunabeestudio.domain.model.smartwallet.SmartWalletVacc
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.minutes

internal data class ApiConfiguration(
    @SerializedName("version")
    val version: Int,
    @SerializedName("app.displayVaccination")
    val displayVaccination: Boolean,
    @SerializedName("app.keyfigures.displayDepartmentLevel")
    val displayDepartmentLevel: Boolean,
    @SerializedName("app.displaySanitaryCertificatesWallet")
    val displaySanitaryCertificatesWallet: Boolean,
    @SerializedName("app.walletPubKeys")
    val walletPublicKeys: String,
    @SerializedName("app.wallet.testCertificateValidityThresholds")
    val testCertificateValidityThresholds: String,
    @SerializedName("app.wallet.maxCertBeforeWarning")
    val maxCertBeforeWarning: Int,
    @SerializedName("app.ratingsKeyFiguresOpeningThreshold")
    val ratingsKeyFiguresOpeningThreshold: Int,
    @SerializedName("app.displayUrgentDgs")
    val displayUrgentDgs: Boolean,
    @SerializedName("app.notif")
    val notif: String,
    @SerializedName("app.activityPass.generationServerPublicKey")
    val generationServerPublicKey: String,
    @SerializedName("app.activityPass.skipNegTestHours")
    val activityPassSkipNegTestHours: Int,
    @SerializedName("app.smartwallet.vacc")
    val smartWalletVacc: String,
    @SerializedName("app.smartwallet.notif")
    val smartWalletNotif: Boolean,
    @SerializedName("app.isSmartwalletOn")
    val isSmartWalletOn: Boolean,
    @SerializedName("app.keyfigures.compareColors")
    val compareColors: String,
    @SerializedName("app.keyfigures.compare")
    val keyFiguresCombination: String,
    @SerializedName("app.wallet.dccKids")
    val dccKids: String,
    @SerializedName("app.displayMultipass")
    val displayMultipass: Boolean,
    @SerializedName("app.multipass.list")
    val multipassConfig: String,
    @SerializedName("app.smartwallet.engine")
    val smartwalletEngine: String,
    @SerializedName("app.blacklistFreqInMinutes")
    val blacklistFreqInMinutes: Int,
    @SerializedName("app.smartwallet.notifElgDays")
    val notifElgDays: Int,
    @SerializedName("app.updateFilesAfterDays")
    val updateFilesAfterDays: Int,
    @SerializedName("app.displayKeyFiguresFrMap")
    val isKeyFigureMapFrDisplayed: Boolean?,
    @SerializedName("app.displayHomeKeyFigures")
    var displayedHomeKeyFigures: Boolean?,
    @SerializedName("app.displayHomeRecommendations")
    var displayHomeRecommendations: Boolean?,
    @SerializedName("app.displayHomeUsefulLinks")
    var displayHomeUsefulLinks: Boolean?,
    @SerializedName("app.displayHomeVaccination")
    var displayHomeVaccination: Boolean?,
    @SerializedName("app.displayInTheSpotlight")
    var displayInTheSpotlight: Boolean?,
)

internal fun ApiConfiguration.toDomain(gson: Gson): Configuration {
    val compareColorList = gson.fromJson<List<Map<String, String>>?>(
        compareColors,
        object : TypeToken<List<Map<String, String>>>() {}.type,
    )
    val colorsCompareKeyFigures = compareColorList?.let {
        try {
            Configuration.ColorsCompareKeyFigures(
                Configuration.ColorCompareKeyFigures(
                    it[0]["d"],
                    it[0]["l"],
                ),
                Configuration.ColorCompareKeyFigures(
                    it[1]["d"],
                    it[1]["l"],
                ),
            )
        } catch (e: IndexOutOfBoundsException) {
            null
        }
    }

    val walletPublicKeyList = gson.fromJson<List<WalletPublicKey>?>(
        walletPublicKeys,
        object : TypeToken<List<WalletPublicKey>?>() {}.type,
    )

    val testCertificateValidityThresholdList = gson.fromJson<List<Int>>(
        testCertificateValidityThresholds,
        object : TypeToken<List<Int>>() {}.type,
    )

    val notifMap = gson.fromJson<Map<String, Any>?>(
        notif,
        object : TypeToken<Map<String, Any>?>() {}.type,
    )
    val notification = notifMap?.let {
        try {
            Configuration.Notification(
                title = (it["t"] as? String)!!,
                subtitle = (it["s"] as? String)!!,
                url = (it["u"] as? String)!!,
                version = (it["v"] as? Number)?.toInt()!!,
            )
        } catch (e: NullPointerException) {
            null
        }
    }

    val apiKeyFiguresCombinationList = gson.fromJson<List<Map<String, String>>>(
        this.keyFiguresCombination,
        object : TypeToken<List<Map<String, String>>>() {}.type,
    )
    val keyFiguresCombinationList = apiKeyFiguresCombinationList?.let { list ->
        list.map { map ->
            Configuration.KeyFigureCombination(
                map["t"],
                map["k1"],
                map["k2"],
            )
        }
    }

    val smartWalletVacc = gson.fromJson(smartWalletVacc, SmartWalletVacc::class.java)
    val dccKidsEmoji = gson.fromJson(dccKids, ApiDccKidsEmoji::class.java)?.dccKidsEmoji
    val multipassConfig = gson.fromJson(multipassConfig, ApiMultipassConfig::class.java)?.toMultipassConfig(
        displayMultipass,
    )

    val smartWalletEngine = gson.fromJson(this.smartwalletEngine, ApiSmartWalletEngine::class.java)?.toSmartWalletEngine()
    return Configuration(
        version = version,
        displayVaccination = displayVaccination,
        displayDepartmentLevel = displayDepartmentLevel,
        displaySanitaryCertificatesWallet = displaySanitaryCertificatesWallet,
        walletPublicKeys = walletPublicKeyList,
        testCertificateValidityThresholds = testCertificateValidityThresholdList,
        maxCertBeforeWarning = maxCertBeforeWarning,
        ratingsKeyFiguresOpeningThreshold = ratingsKeyFiguresOpeningThreshold,
        displayUrgentDgs = displayUrgentDgs,
        notification = notification,
        generationServerPublicKey = generationServerPublicKey,
        activityPassSkipNegTestHours = activityPassSkipNegTestHours,
        smartWalletVacc = smartWalletVacc,
        smartWalletNotif = smartWalletNotif,
        isSmartWalletOn = isSmartWalletOn,
        colorsCompareKeyFigures = colorsCompareKeyFigures,
        keyFiguresCombination = keyFiguresCombinationList,
        dccKidsEmoji = dccKidsEmoji,
        multipassConfig = multipassConfig,
        smartWalletEngine = smartWalletEngine,
        blacklistRefreshFrequency = blacklistFreqInMinutes.minutes,
        notifElgDelay = notifElgDays.days,
        updateFilesAfterDays = updateFilesAfterDays.days,
        isKeyFigureMapFrDisplayed = isKeyFigureMapFrDisplayed ?: false,
        displayedHomeKeyFigures = displayedHomeKeyFigures ?: false,
        displayHomeRecommendations = displayHomeRecommendations ?: false,
        displayHomeUsefulLinks = displayHomeUsefulLinks ?: false,
        displayHomeVaccination = displayHomeVaccination ?: false,
        displayInTheSpotlight = displayInTheSpotlight ?: false,
    )
}
