/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.certificate

import android.content.SharedPreferences
import com.lunabeestudio.common.ConfigConstant
import com.lunabeestudio.domain.model.DownloadStatus
import com.lunabeestudio.local.extension.lastCertificatesDocumentsManagerSuccessSync
import com.lunabeestudio.repository.certificate.CertificateDocumentLocalDataSource
import com.lunabeestudio.repository.certificate.ImageDocumentLocalDataSource
import com.lunabeestudio.repository.certificate.ImageDocumentRepositoryImpl
import com.lunabeestudio.repository.file.FileLocalDataSource
import com.lunabeestudio.repository.file.FileRemoteToLocalDataSource
import kotlinx.coroutines.sync.Mutex
import timber.log.Timber
import java.time.Instant
import javax.inject.Inject
import kotlin.time.Duration

class CertificateDocumentLocalDataSourceImpl @Inject constructor(
    private val imageDocumentLocalDataSource: ImageDocumentLocalDataSource,
    private val fileLocalDataSource: FileLocalDataSource,
    private val fileRemoteToLocalDataSource: FileRemoteToLocalDataSource,
    private val sharedPreferences: SharedPreferences,
) : CertificateDocumentLocalDataSource {

    private val fetchMtx: Mutex = Mutex()

    private val certificateDocumentRemoteFileManager = object : ImageDocumentRepositoryImpl(
        imageDocumentLocalDataSource,
        fileLocalDataSource,
        fileRemoteToLocalDataSource,
    ) {
        override fun getLocalFileName(): String = ConfigConstant.Wallet.TEST_CERTIFICATE_THUMBNAIL_FILE
        override val remoteFileUrlTemplate: String = ConfigConstant.Wallet.TEST_CERTIFICATE_THUMBNAIL_TEMPLATE_PATH
    }

    private val fullCertificateDocumentRemoteFileManager = object : ImageDocumentRepositoryImpl(
        imageDocumentLocalDataSource,
        fileLocalDataSource,
        fileRemoteToLocalDataSource,
    ) {
        override fun getLocalFileName(): String = ConfigConstant.Wallet.TEST_CERTIFICATE_FULL_FILE
        override val remoteFileUrlTemplate: String = ConfigConstant.Wallet.TEST_CERTIFICATE_FULL_TEMPLATE_PATH
    }

    private val vaccinDocumentRemoteFileManager = object : ImageDocumentRepositoryImpl(
        imageDocumentLocalDataSource,
        fileLocalDataSource,
        fileRemoteToLocalDataSource,
    ) {
        override fun getLocalFileName(): String = ConfigConstant.Wallet.VACCIN_CERTIFICATE_THUMBNAIL_FILE
        override val remoteFileUrlTemplate: String = ConfigConstant.Wallet.VACCIN_CERTIFICATE_THUMBNAIL_TEMPLATE_PATH
    }

    private val fullVaccinDocumentRemoteFileManager = object : ImageDocumentRepositoryImpl(
        imageDocumentLocalDataSource,
        fileLocalDataSource,
        fileRemoteToLocalDataSource,
    ) {
        override fun getLocalFileName(): String = ConfigConstant.Wallet.VACCIN_CERTIFICATE_FULL_FILE
        override val remoteFileUrlTemplate: String = ConfigConstant.Wallet.VACCIN_CERTIFICATE_FULL_TEMPLATE_PATH
    }

    private val vaccinEuropeDocumentRemoteFileManager = object : ImageDocumentRepositoryImpl(
        imageDocumentLocalDataSource,
        fileLocalDataSource,
        fileRemoteToLocalDataSource,
    ) {
        override fun getLocalFileName(): String = ConfigConstant.Wallet.VACCIN_EUROPE_CERTIFICATE_THUMBNAIL_FILE
        override val remoteFileUrlTemplate: String = ConfigConstant.Wallet.VACCIN_EUROPE_CERTIFICATE_THUMBNAIL_TEMPLATE_PATH
    }

    private val fullVaccinEuropeDocumentRemoteFileManager = object : ImageDocumentRepositoryImpl(
        imageDocumentLocalDataSource,
        fileLocalDataSource,
        fileRemoteToLocalDataSource,
    ) {
        override fun getLocalFileName(): String = ConfigConstant.Wallet.VACCIN_EUROPE_CERTIFICATE_FULL_FILE
        override val remoteFileUrlTemplate: String = ConfigConstant.Wallet.VACCIN_EUROPE_CERTIFICATE_FULL_TEMPLATE_PATH
    }

    private val certificateEuropeDocumentRemoteFileManager = object : ImageDocumentRepositoryImpl(
        imageDocumentLocalDataSource,
        fileLocalDataSource,
        fileRemoteToLocalDataSource,
    ) {
        override fun getLocalFileName(): String = ConfigConstant.Wallet.TEST_EUROPE_CERTIFICATE_THUMBNAIL_FILE
        override val remoteFileUrlTemplate: String = ConfigConstant.Wallet.TEST_EUROPE_CERTIFICATE_THUMBNAIL_TEMPLATE_PATH
    }

    private val fullCertificateEuropeDocumentRemoteFileManager = object : ImageDocumentRepositoryImpl(
        imageDocumentLocalDataSource,
        fileLocalDataSource,
        fileRemoteToLocalDataSource,
    ) {
        override fun getLocalFileName(): String = ConfigConstant.Wallet.TEST_EUROPE_CERTIFICATE_FULL_FILE
        override val remoteFileUrlTemplate: String = ConfigConstant.Wallet.TEST_EUROPE_CERTIFICATE_FULL_TEMPLATE_PATH
    }

    private val recoveryEuropeDocumentRemoteFileManager = object : ImageDocumentRepositoryImpl(
        imageDocumentLocalDataSource,
        fileLocalDataSource,
        fileRemoteToLocalDataSource,
    ) {
        override fun getLocalFileName(): String = ConfigConstant.Wallet.RECOVERY_EUROPE_CERTIFICATE_THUMBNAIL_FILE
        override val remoteFileUrlTemplate: String = ConfigConstant.Wallet.RECOVERY_EUROPE_CERTIFICATE_THUMBNAIL_TEMPLATE_PATH
    }

    private val fullRecoveryEuropeDocumentRemoteFileManager = object : ImageDocumentRepositoryImpl(
        imageDocumentLocalDataSource,
        fileLocalDataSource,
        fileRemoteToLocalDataSource,
    ) {
        override fun getLocalFileName(): String = ConfigConstant.Wallet.RECOVERY_EUROPE_CERTIFICATE_FULL_FILE
        override val remoteFileUrlTemplate: String = ConfigConstant.Wallet.RECOVERY_EUROPE_CERTIFICATE_FULL_TEMPLATE_PATH
    }

    override suspend fun fetchLastImages(delay: Duration) {
        val lastSuccessSync = sharedPreferences.lastCertificatesDocumentsManagerSuccessSync
        val now = Instant.now()
        Timber.v("[CertificatesDocumentsManager] last success sync ($lastSuccessSync) / now ($now) / delay $delay")
        if (now.toEpochMilli() - lastSuccessSync.toEpochMilli() > delay.inWholeMilliseconds) {
            if (fetchMtx.tryLock()) {
                Timber.v("[CertificatesDocumentsManager] lock mutex")
                try {
                    val success = (certificateDocumentRemoteFileManager.fetchLastImage() != DownloadStatus.Failure)
                        .and(fullCertificateDocumentRemoteFileManager.fetchLastImage() != DownloadStatus.Failure)
                        .and(vaccinDocumentRemoteFileManager.fetchLastImage() != DownloadStatus.Failure)
                        .and(fullVaccinDocumentRemoteFileManager.fetchLastImage() != DownloadStatus.Failure)
                        .and(vaccinEuropeDocumentRemoteFileManager.fetchLastImage() != DownloadStatus.Failure)
                        .and(fullVaccinEuropeDocumentRemoteFileManager.fetchLastImage() != DownloadStatus.Failure)
                        .and(certificateEuropeDocumentRemoteFileManager.fetchLastImage() != DownloadStatus.Failure)
                        .and(
                            fullCertificateEuropeDocumentRemoteFileManager.fetchLastImage()
                                != DownloadStatus.Failure,
                        )
                        .and(recoveryEuropeDocumentRemoteFileManager.fetchLastImage() != DownloadStatus.Failure)
                        .and(
                            fullRecoveryEuropeDocumentRemoteFileManager.fetchLastImage()
                                != DownloadStatus.Failure,
                        )
                    if (success) {
                        Timber.v("[CertificatesDocumentsManager] successful sync at ${Instant.now()}")
                        sharedPreferences.lastCertificatesDocumentsManagerSuccessSync = Instant.now()
                    } else {
                        Timber.v("[CertificatesDocumentsManager] failure during sync")
                    }
                } finally {
                    Timber.v("[CertificatesDocumentsManager] unlock mutex")
                    fetchMtx.unlock()
                }
            } else {
                Timber.v("[CertificatesDocumentsManager] mutex locked")
            }
        } else {
            Timber.v(
                "[CertificatesDocumentsManager] last sync ($lastSuccessSync) - " +
                    "now ($now) is less than refresh config delay ($delay)",
            )
        }
    }
}
