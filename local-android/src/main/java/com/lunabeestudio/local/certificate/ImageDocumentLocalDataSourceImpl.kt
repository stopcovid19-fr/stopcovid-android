/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.certificate

import android.content.Context
import android.graphics.drawable.Drawable
import com.lunabeestudio.common.extension.getApplicationLanguage
import com.lunabeestudio.repository.certificate.ImageDocumentLocalDataSource
import dagger.hilt.android.qualifiers.ApplicationContext
import timber.log.Timber
import java.io.File
import javax.inject.Inject

class ImageDocumentLocalDataSourceImpl @Inject constructor(
    @ApplicationContext private val context: Context,
) : ImageDocumentLocalDataSource {

    override fun getApplicationLanguage(): String {
        return context.getApplicationLanguage()
    }

    override fun fileNotCorrupted(file: File): Boolean {
        return try {
            Drawable.createFromPath(file.path) != null
        } catch (e: Exception) {
            Timber.e(e)
            false
        }
    }
}
