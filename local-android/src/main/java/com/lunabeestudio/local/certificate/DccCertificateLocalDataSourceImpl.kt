/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.certificate

import com.google.gson.reflect.TypeToken
import com.lunabeestudio.common.ConfigConstant
import com.lunabeestudio.common.EnvConstant
import com.lunabeestudio.domain.model.DccCertificates
import com.lunabeestudio.repository.certificate.DccCertificateLocalDataSource
import java.lang.reflect.Type
import javax.inject.Inject

class DccCertificateLocalDataSourceImpl @Inject constructor() : DccCertificateLocalDataSource {
    override val type: Type = object : TypeToken<DccCertificates>() {}.type

    override fun getLocalFileName(): String {
        return EnvConstant.Prod.dccCertificatesFilename
    }

    override fun getRemoteFileUrl(): String {
        return ConfigConstant.DccCertificates.URL + getLocalFileName()
    }

    override fun getAssetFilePath(): String {
        return ConfigConstant.DccCertificates.FOLDER + getLocalFileName()
    }
}
