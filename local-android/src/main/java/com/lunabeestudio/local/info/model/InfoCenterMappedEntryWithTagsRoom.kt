/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.info.model

data class InfoCenterMappedEntryWithTagsRoom(
    val id: String,
    val title: String?,
    val description: String?,
    val buttonLabel: String?,
    val url: String?,
    val timestamp: Long,
    val tagLabel: String?,
    val tagColor: String?,
)
