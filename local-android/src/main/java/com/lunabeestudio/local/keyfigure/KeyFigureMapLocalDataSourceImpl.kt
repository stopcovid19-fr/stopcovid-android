/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.keyfigure

import android.content.Context
import com.lunabeestudio.common.ConfigConstant
import com.lunabeestudio.common.extension.toMapKeyFigures
import com.lunabeestudio.domain.model.KeyFigureMap
import com.lunabeestudio.local.extension.toDepartmentKeyFigureMap
import com.lunabeestudio.local.extension.toDepartmentKeyFigureMapRoomList
import com.lunabeestudio.repository.keyfigure.KeyFigureMapLocalDataSource
import dagger.hilt.android.qualifiers.ApplicationContext
import keynumbers.Keynumbers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

class KeyFigureMapLocalDataSourceImpl @Inject constructor(
    @ApplicationContext private val context: Context,
    private val keyFiguresMapDao: KeyFiguresMapDao,
) : KeyFigureMapLocalDataSource {

    override suspend fun keyFigureMapByLabel(label: String): KeyFigureMap? {
        return keyFiguresMapDao.getDepartmentKeyFigureMapRoomByLabel(label).let {
            if (it.isEmpty()) {
                null
            } else {
                KeyFigureMap(
                    labelKey = label,
                    version = "",
                    valuesDepartments = it.map { (department, series) ->
                        department.toDepartmentKeyFigureMap(series ?: listOf())
                    },
                )
            }
        }
    }

    override fun availableKeyFigureMap(): Flow<List<String>> {
        return keyFiguresMapDao.getFlowDepartmentKeyFigureMapRoom().map {
            it.map { it.labelKey }
        }
    }

    override suspend fun count(): Int {
        return withContext(Dispatchers.IO) {
            keyFiguresMapDao.getDepartmentKeyFigureMapCount()
        }
    }

    override suspend fun loadFromAsset() {
        withContext(Dispatchers.IO) {
            Timber.v("Loading KeyFigureMap data from ${ConfigConstant.KeyFigures.ASSET_PATH}")
            context.assets.open(ConfigConstant.KeyFigures.ASSET_PATH).use {
                val keyFigureMapListProto = Keynumbers.KeyNumbersMessage.parseFrom(it)
                insertNewAndDeleteOld(keyFigureMapListProto.toMapKeyFigures())
            }
        }
    }

    override suspend fun insertNewAndDeleteOld(new: List<KeyFigureMap>) {
        withContext(Dispatchers.IO) {
            keyFiguresMapDao.insertNewAndDeleteOld(
                new = new.flatMap(KeyFigureMap::toDepartmentKeyFigureMapRoomList),
            )
        }
    }
}
