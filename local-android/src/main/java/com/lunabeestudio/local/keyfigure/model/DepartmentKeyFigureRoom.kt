/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.keyfigure.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Ignore
import androidx.room.Index
import androidx.room.PrimaryKey
import com.lunabeestudio.local.LocalConstants

@Entity(
    foreignKeys = [
        ForeignKey(
            entity = KeyFigureRoom::class,
            parentColumns = [LocalConstants.RoomColumnName.LabelKey],
            childColumns = [LocalConstants.RoomColumnName.LabelKeyFigure],
            onDelete = ForeignKey.CASCADE,
        ),
    ],
    indices = [Index(LocalConstants.RoomColumnName.LabelKeyFigure)],
)
data class DepartmentKeyFigureRoom(
    val labelKeyFigure: String,
    val dptNb: String,
    val dptLabel: String,
    val extractDateS: Long,
    val value: Double,
    val valueToDisplay: String?,
    @PrimaryKey val labelAndDptNb: String,
    @Ignore val series: List<KeyFigureSeriesItemRoom>?,
) {
    constructor(
        labelKeyFigure: String,
        dptNb: String,
        dptLabel: String,
        extractDateS: Long,
        value: Double,
        valueToDisplay: String?,
        labelAndDptNb: String,
    ) : this(labelKeyFigure, dptNb, dptLabel, extractDateS, value, valueToDisplay, labelAndDptNb, listOf())
}
