/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/01/03 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.featuredinfo

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import kotlinx.coroutines.flow.Flow

@Dao
interface FeaturedInfoDao {
    @Insert
    fun insertAll(featuredInfo: List<FeaturedInfoRoom>)

    @Query("SELECT * FROM FeaturedInfoRoom ORDER BY `index`")
    fun getAllFeaturedInfoFlow(): Flow<List<FeaturedInfoRoom>>

    @Query("SELECT * FROM FeaturedInfoRoom WHERE `index` = :index")
    suspend fun getFeaturedInfoByIndex(index: Int): FeaturedInfoRoom

    @Query("DELETE FROM FeaturedInfoRoom")
    fun deleteAll()

    @Transaction
    suspend fun deleteOldAndInsertNew(newFeaturedInfo: List<FeaturedInfoRoom>) {
        deleteAll()
        insertAll(newFeaturedInfo)
    }
}
