/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.featuredinfo

import android.content.Context
import android.content.SharedPreferences
import com.lunabeestudio.common.extension.getApplicationLanguage
import com.lunabeestudio.common.extension.getFileNameHDFromFeaturedInfo
import com.lunabeestudio.common.extension.getFileThumbnailFromFeaturedInfo
import com.lunabeestudio.common.extension.prevSyncLanguageFeaturedInfo
import com.lunabeestudio.domain.model.FeaturedInfo
import com.lunabeestudio.local.extension.toFeaturedInfo
import com.lunabeestudio.local.extension.toFeaturedInfoRoom
import com.lunabeestudio.repository.featuredinfo.FeaturedInfoLocalDataSource
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.io.File
import javax.inject.Inject

class FeaturedInfoLocalDataSourceImpl @Inject constructor(
    @ApplicationContext private val context: Context,
    private val featuredInfoDao: FeaturedInfoDao,
    private val sharedPreferences: SharedPreferences,
) : FeaturedInfoLocalDataSource {

    override fun flowAllFeaturedInfo(): Flow<List<FeaturedInfo>> {
        return featuredInfoDao.getAllFeaturedInfoFlow().map {
            it.map(FeaturedInfoRoom::toFeaturedInfo)
        }
    }

    override suspend fun getFeaturedInfoByIndex(index: Int): FeaturedInfo {
        return featuredInfoDao.getFeaturedInfoByIndex(index).let(FeaturedInfoRoom::toFeaturedInfo)
    }

    override suspend fun deleteOldAndInsertNew(newFeaturedInfo: List<FeaturedInfo>) {
        featuredInfoDao.deleteOldAndInsertNew(newFeaturedInfo.map(FeaturedInfo::toFeaturedInfoRoom))
    }

    override fun removeOldFiles(oldFeaturedInfo: List<FeaturedInfo>) {
        oldFeaturedInfo.forEach {
            File(context.filesDir, it.getFileNameHDFromFeaturedInfo()).delete()
            File(context.filesDir, it.getFileThumbnailFromFeaturedInfo()).delete()
        }
    }

    override fun getApplicationLanguage(): String {
        return context.getApplicationLanguage()
    }

    override fun getFileThumbnailFromFeaturedInfo(featuredInfo: FeaturedInfo): String {
        return featuredInfo.getFileThumbnailFromFeaturedInfo()
    }

    override fun getFileNameHDFromFeaturedInfo(featuredInfo: FeaturedInfo): String {
        return featuredInfo.getFileNameHDFromFeaturedInfo()
    }

    override var prevSyncLanguageFeaturedInfo: String?
        get() = sharedPreferences.prevSyncLanguageFeaturedInfo
        set(value) {
            sharedPreferences.prevSyncLanguageFeaturedInfo = value
        }
}
