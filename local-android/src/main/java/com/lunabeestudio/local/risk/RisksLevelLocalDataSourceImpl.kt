/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.risk

import com.google.gson.reflect.TypeToken
import com.lunabeestudio.common.ConfigConstant
import com.lunabeestudio.domain.model.RisksUILevelSection
import com.lunabeestudio.repository.risk.RisksLevelLocalDataSource
import java.lang.reflect.Type
import javax.inject.Inject

class RisksLevelLocalDataSourceImpl @Inject constructor() : RisksLevelLocalDataSource {
    override val type: Type = object : TypeToken<List<RisksUILevelSection>>() {}.type

    override fun getLocalFileName(): String {
        return ConfigConstant.Risks.FILENAME
    }

    override fun getRemoteFileUrl(): String {
        return ConfigConstant.Risks.URL
    }

    override fun getAssetFilePath(): String {
        return ConfigConstant.Risks.ASSET_FILE_PATH
    }
}
