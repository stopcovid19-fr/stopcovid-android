/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/12/22 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.extension

import com.lunabeestudio.domain.model.KeyFigureSeriesItem
import com.lunabeestudio.local.keyfigure.model.KeyFigureMapSerieItemRoom

internal fun KeyFigureSeriesItem.toKeyFigureMapSeriesItemRoom(
    labelAndDptNb: String,
) = KeyFigureMapSerieItemRoom(
    date,
    value.toDouble(),
    opacity?.toDouble() ?: 0.0,
    labelAndDptNb,
)

internal fun KeyFigureMapSerieItemRoom.toKeyFigureSeriesItem() = KeyFigureSeriesItem(
    date,
    value,
    opacity,
)
