/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.extension

import com.lunabeestudio.domain.model.KeyFigureSeriesItem
import com.lunabeestudio.local.keyfigure.model.KeyFigureSeriesItemRoom
import com.lunabeestudio.local.keyfigure.model.KeyFigureSeriesTypeRoom

internal fun KeyFigureSeriesItem.toKeyFigureSeriesItemRoom(
    labelKeyFigure: String?,
    labelAndDptNb: String?,
    seriesTypeRoom: KeyFigureSeriesTypeRoom,
) = KeyFigureSeriesItemRoom(
    date,
    value.toDouble(),
    seriesTypeRoom,
    labelKeyFigure,
    labelAndDptNb,
)

internal fun KeyFigureSeriesItemRoom.toKeyFigureSeriesItem() = KeyFigureSeriesItem(
    date,
    value,
    null,
)
