/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/14 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.extension

import android.content.SharedPreferences
import androidx.core.content.edit
import com.lunabeestudio.local.LocalConstants
import java.time.Instant

var SharedPreferences.currentVaccinationReferenceLatitude: Double?
    get() = if (contains(LocalConstants.SharedPrefs.CURRENT_VACCINATION_REFERENCE_LATITUDE)) {
        getFloat(
            LocalConstants.SharedPrefs.CURRENT_VACCINATION_REFERENCE_LATITUDE,
            0f,
        ).toDouble()
    } else {
        null
    }
    set(value) = edit {
        if (value != null) {
            putFloat(LocalConstants.SharedPrefs.CURRENT_VACCINATION_REFERENCE_LATITUDE, value.toFloat())
        } else {
            remove(LocalConstants.SharedPrefs.CURRENT_VACCINATION_REFERENCE_LATITUDE)
        }
    }

var SharedPreferences.currentVaccinationReferenceLongitude: Double?
    get() = if (contains(LocalConstants.SharedPrefs.CURRENT_VACCINATION_REFERENCE_LONGITUDE)) {
        getFloat(
            LocalConstants.SharedPrefs.CURRENT_VACCINATION_REFERENCE_LONGITUDE,
            0f,
        ).toDouble()
    } else {
        null
    }
    set(value) = edit {
        if (value != null) {
            putFloat(LocalConstants.SharedPrefs.CURRENT_VACCINATION_REFERENCE_LONGITUDE, value.toFloat())
        } else {
            remove(LocalConstants.SharedPrefs.CURRENT_VACCINATION_REFERENCE_LONGITUDE)
        }
    }

var SharedPreferences.zipGeolocVersion: Int
    get() = getInt(LocalConstants.SharedPrefs.ZIP_GEOLOC_VERSION, 0)
    set(value) = edit { putInt(LocalConstants.SharedPrefs.ZIP_GEOLOC_VERSION, value) }

var SharedPreferences.currentVaccinationReferenceDepartmentCode: String?
    get() = getString(LocalConstants.SharedPrefs.CURRENT_VACCINATION_REFERENCE_DEPARTMENT_CODE, null)
    set(value) = edit { putString(LocalConstants.SharedPrefs.CURRENT_VACCINATION_REFERENCE_DEPARTMENT_CODE, value) }

var SharedPreferences.lastCertificatesDocumentsManagerSuccessSync: Instant
    get() = Instant.ofEpochMilli(getLong(LocalConstants.SharedPrefs.LastCertificatesDocumentsManagerSuccessSync, 0L))
    set(value) = edit {
        putLong(
            LocalConstants.SharedPrefs.LastCertificatesDocumentsManagerSuccessSync,
            value.toEpochMilli(),
        )
    }
