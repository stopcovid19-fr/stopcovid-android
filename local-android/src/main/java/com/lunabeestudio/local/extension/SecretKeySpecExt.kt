/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.extension

import javax.crypto.SecretKey
import javax.security.auth.DestroyFailedException

fun SecretKey.safeDestroy() {
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
        try {
            destroy()
        } catch (e: DestroyFailedException) {
            // Destroy not implemented
        } catch (e: NoSuchMethodError) {
            // Destroy not implemented
        }
    }
}

fun <T> SecretKey.safeUse(block: (SecretKey) -> T): T {
    try {
        return block(this)
    } finally {
        safeDestroy()
    }
}
