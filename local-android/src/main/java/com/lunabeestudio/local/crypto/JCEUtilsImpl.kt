/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.crypto

import com.lunabeestudio.domain.utils.JCEUtils
import org.bouncycastle.jce.provider.BouncyCastleProvider
import timber.log.Timber
import java.security.Security
import javax.inject.Inject

class JCEUtilsImpl @Inject constructor() : JCEUtils {
    override fun updateBouncyCastle() {
        val bouncyCastleProvider = BouncyCastleProvider()
        Security.removeProvider(bouncyCastleProvider.name)
        val result = Security.addProvider(bouncyCastleProvider)
        Timber.i("$bouncyCastleProvider added at position $result")
    }
}
