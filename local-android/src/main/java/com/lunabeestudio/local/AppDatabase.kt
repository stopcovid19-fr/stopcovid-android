/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local

import android.content.Context
import androidx.room.AutoMigration
import androidx.room.Database
import androidx.room.DeleteTable
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import androidx.room.migration.AutoMigrationSpec
import com.lunabeestudio.local.blacklist.BlacklistPrefixDao
import com.lunabeestudio.local.blacklist.BlacklistPrefixRoom
import com.lunabeestudio.local.certificate.CertificateRoom
import com.lunabeestudio.local.certificate.CertificateRoomDao
import com.lunabeestudio.local.featuredinfo.FeaturedInfoDao
import com.lunabeestudio.local.featuredinfo.FeaturedInfoRoom
import com.lunabeestudio.local.info.InfoCenterDao
import com.lunabeestudio.local.info.model.InfoCenterEntryRoom
import com.lunabeestudio.local.info.model.InfoCenterEntryTagCrossRefRoom
import com.lunabeestudio.local.info.model.InfoCenterLabelRoom
import com.lunabeestudio.local.info.model.InfoCenterTagRoom
import com.lunabeestudio.local.keyfigure.KeyFiguresDao
import com.lunabeestudio.local.keyfigure.KeyFiguresMapDao
import com.lunabeestudio.local.keyfigure.model.DepartmentKeyFigureMapRoom
import com.lunabeestudio.local.keyfigure.model.DepartmentKeyFigureRoom
import com.lunabeestudio.local.keyfigure.model.KeyFigureFavoriteRoom
import com.lunabeestudio.local.keyfigure.model.KeyFigureMapSerieItemRoom
import com.lunabeestudio.local.keyfigure.model.KeyFigureRoom
import com.lunabeestudio.local.keyfigure.model.KeyFigureSeriesItemRoom
import java.util.Date

@TypeConverters(Converters::class)
@Database(
    version = 13,
    entities = [
        CertificateRoom::class,
        KeyFigureRoom::class,
        KeyFigureFavoriteRoom::class,
        DepartmentKeyFigureRoom::class,
        KeyFigureSeriesItemRoom::class,
        BlacklistPrefixRoom::class,
        InfoCenterEntryTagCrossRefRoom::class,
        InfoCenterEntryRoom::class,
        InfoCenterLabelRoom::class,
        InfoCenterTagRoom::class,
        DepartmentKeyFigureMapRoom::class,
        KeyFigureMapSerieItemRoom::class,
        FeaturedInfoRoom::class,
    ],
    autoMigrations = [
        AutoMigration(from = 1, to = 2),
        AutoMigration(from = 2, to = 3),
        AutoMigration(from = 3, to = 4),
        AutoMigration(from = 4, to = 5),
        AutoMigration(from = 5, to = 6),
        AutoMigration(from = 6, to = 7),
        AutoMigration(from = 7, to = 8),
        AutoMigration(from = 8, to = 9),
        AutoMigration(from = 9, to = 10),
        AutoMigration(from = 10, to = 11),
        AutoMigration(from = 11, to = 12),
        AutoMigration(from = 12, to = 13, spec = DeleteActivityPassAndAttestationAndVenueRoomTable::class),
    ],
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun certificateRoomDao(): CertificateRoomDao
    abstract fun keyFiguresDao(): KeyFiguresDao
    abstract fun keyFiguresMapDao(): KeyFiguresMapDao
    abstract fun blacklistPrefixDao(): BlacklistPrefixDao
    abstract fun infoCenterDao(): InfoCenterDao
    abstract fun featuredInfoDao(): FeaturedInfoDao

    companion object {
        private const val DB_NAME = "robert_db"

        fun build(context: Context): AppDatabase =
            Room.databaseBuilder(context, AppDatabase::class.java, DB_NAME)
                .build()
    }
}

@DeleteTable(tableName = "ActivityPassRoom")
@DeleteTable(tableName = "AttestationRoom")
@DeleteTable(tableName = "VenueRoom")
class DeleteActivityPassAndAttestationAndVenueRoomTable : AutoMigrationSpec

@Suppress("unused")
class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }
}
