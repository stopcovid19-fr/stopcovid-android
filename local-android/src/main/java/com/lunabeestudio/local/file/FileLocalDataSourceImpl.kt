/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.file

import android.content.Context
import com.lunabeestudio.domain.di.IODispatcher
import com.lunabeestudio.repository.file.FileLocalDataSource
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import java.io.File
import java.io.InputStream
import javax.inject.Inject

class FileLocalDataSourceImpl @Inject constructor(
    @ApplicationContext private val context: Context,
    @IODispatcher private val ioDispatcher: CoroutineDispatcher,
) : FileLocalDataSource {

    override val filesDir: File
        get() = context.filesDir

    override suspend fun getLocalFileOrAssetStream(localFileName: String, assetFilePath: String?): InputStream? {
        val localFile = File(context.filesDir, localFileName)
        return when {
            localFile.exists() -> try {
                localFile.inputStream()
            } catch (e: Exception) {
                timber.log.Timber.Forest.e(e)
                null
            }
            assetFilePath != null -> getDefaultAssetStream(assetFilePath)
            else -> {
                timber.log.Timber.Forest.v("Nothing to load")
                null
            }
        }
    }

    private suspend fun getDefaultAssetStream(assetFilePath: String?): InputStream? {
        return assetFilePath?.let { path ->
            withContext(ioDispatcher) {
                @Suppress("BlockingMethodInNonBlockingContext")
                context.assets.open(path)
            }
        }
    }

    override fun clearLocal(localFileName: String) {
        File(context.filesDir, localFileName).delete()
    }
}
