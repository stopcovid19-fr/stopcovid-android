/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.local.blacklist

import com.lunabeestudio.repository.blacklist.BlacklistLocalDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class BlacklistLocalDataSourceImpl @Inject constructor(
    private val blacklistPrefixDao: BlacklistPrefixDao,
) : BlacklistLocalDataSource {
    override suspend fun getMatchPrefix(hash: String): String? {
        return withContext(Dispatchers.IO) {
            blacklistPrefixDao.getMatchPrefix(hash)
        }
    }
}
