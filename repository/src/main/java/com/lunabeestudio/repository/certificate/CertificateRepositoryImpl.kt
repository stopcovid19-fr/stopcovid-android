/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.repository.certificate

import com.lunabeestudio.domain.model.RawWalletCertificate
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.domain.repository.CertificateRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class CertificateRepositoryImpl @Inject constructor(
    private val certificateLocalDataSource: CertificateLocalDataSource,
    private val certificateRemoteDataSource: CertificateRemoteDataSource,
) : CertificateRepository {
    override val rawWalletCertificatesFlow: Flow<TacResult<List<RawWalletCertificate>>>
        get() = certificateLocalDataSource.rawWalletCertificatesFlow
    override val certificateCountFlow: Flow<Int>
        get() = certificateLocalDataSource.certificateCountFlow

    override suspend fun insertAllRawWalletCertificates(certificates: List<RawWalletCertificate>) {
        certificateLocalDataSource.insertAllRawWalletCertificates(certificates)
    }

    override suspend fun updateAllRawWalletCertificates(certificates: List<RawWalletCertificate>) {
        certificateLocalDataSource.updateAllRawWalletCertificates(certificates)
    }

    override suspend fun deleteRawWalletCertificate(certificateId: String) {
        certificateLocalDataSource.deleteRawWalletCertificate(certificateId)
    }

    override suspend fun deleteAllRawWalletCertificates() {
        certificateLocalDataSource.deleteAllRawWalletCertificates()
    }

    override suspend fun getCertificateCount(): Int {
        return certificateLocalDataSource.getCertificateCount()
    }

    override suspend fun getCertificateById(certificateId: String): RawWalletCertificate? {
        return certificateLocalDataSource.getCertificateById(certificateId)
    }

    override suspend fun generateMultipass(serverPublicKey: String, encodedCertificateList: List<String>): TacResult<String> {
        return certificateRemoteDataSource.generateMultipass(serverPublicKey, encodedCertificateList)
    }

    override suspend fun forceRefreshCertificatesFlow() {
        certificateLocalDataSource.forceRefreshCertificatesFlow()
    }

    override suspend fun deleteLostCertificates() {
        certificateLocalDataSource.deleteLostCertificates()
    }

    override fun resetKeyGeneratedFlag() {
        certificateLocalDataSource.resetKeyGeneratedFlag()
    }
}
