/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.repository.smartwallet

import com.lunabeestudio.domain.model.DownloadStatus
import com.lunabeestudio.domain.model.smartwallet.SmartWalletEligibilityPivot
import com.lunabeestudio.domain.repository.SmartWalletEligibilityRepository
import com.lunabeestudio.repository.file.FileLocalDataSource
import com.lunabeestudio.repository.file.FileRemoteToLocalDataSource
import com.lunabeestudio.repository.file.JsonLocalDataSource
import com.lunabeestudio.repository.file.JsonRemoteDataSource
import com.lunabeestudio.repository.file.JsonRemoteRepositoryImpl
import com.lunabeestudio.repository.smartwallet.model.ApiSmartWalletEligibilityPivot
import java.lang.reflect.Type
import javax.inject.Inject

class SmartWalletEligibilityRepositoryImpl @Inject constructor(
    private val smartWalletEligibilityLocalDataSource: SmartWalletEligibilityLocalDataSource,
    jsonLocalDataSource: JsonLocalDataSource,
    jsonRemoteDataSource: JsonRemoteDataSource,
    fileLocalDataSource: FileLocalDataSource,
    fileRemoteToLocalDataSource: FileRemoteToLocalDataSource,
) : JsonRemoteRepositoryImpl<List<ApiSmartWalletEligibilityPivot>>(
    jsonLocalDataSource,
    jsonRemoteDataSource,
    fileLocalDataSource,
    fileRemoteToLocalDataSource,
),
    SmartWalletEligibilityRepository {

    override val type: Type = smartWalletEligibilityLocalDataSource.type
    override fun getLocalFileName(): String = smartWalletEligibilityLocalDataSource.getLocalFileName()
    override fun getRemoteFileUrl(): String = smartWalletEligibilityLocalDataSource.getRemoteFileUrl()
    override fun getAssetFilePath(): String = smartWalletEligibilityLocalDataSource.getAssetFilePath()

    override var smartWalletEligibilityPivot: List<SmartWalletEligibilityPivot> = emptyList()
        private set

    override suspend fun initialize() {
        smartWalletEligibilityPivot = loadLocal()?.flatMap(
            ApiSmartWalletEligibilityPivot::toSmartWalletEligibilityPivots,
        )
            ?: emptyList()
    }

    override suspend fun onAppForeground() {
        if (fetchLast() == DownloadStatus.FromServer) {
            initialize()
        }
    }
}
