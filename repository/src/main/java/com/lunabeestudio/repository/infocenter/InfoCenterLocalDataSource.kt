/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.repository.infocenter

import com.lunabeestudio.domain.model.InfoCenterData
import kotlinx.coroutines.flow.Flow

interface InfoCenterLocalDataSource {
    fun getPastInfoCenterDataWithoutTag(amount: Int, now: Long): Flow<InfoCenterData>
    fun getPastInfoCenterData(now: Long): Flow<InfoCenterData>
}
