/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.repository.infocenter

import com.lunabeestudio.domain.model.InfoCenterData
import com.lunabeestudio.domain.repository.InfoCenterRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class InfoCenterRepositoryImpl @Inject constructor(
    private val infoCenterLocalDataSource: InfoCenterLocalDataSource,
) : InfoCenterRepository {

    override fun getHomeInfoCenterData(amount: Int, nowSec: Long): Flow<InfoCenterData> =
        infoCenterLocalDataSource.getPastInfoCenterDataWithoutTag(amount, nowSec)

    override fun getDetailInfoCenterData(nowSec: Long): Flow<InfoCenterData> =
        infoCenterLocalDataSource.getPastInfoCenterData(nowSec)
}
