/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.repository.featuredinfo

import com.lunabeestudio.domain.model.FeaturedInfo
import com.lunabeestudio.domain.model.TacResult

interface FeaturedInfoRemoteDataSource {
    suspend fun fetchFeaturedInfo(localSuffix: String, forceNewData: Boolean): TacResult<List<FeaturedInfo>?>
    suspend fun saveResourceInLocal(url: String, fileName: String)
}
