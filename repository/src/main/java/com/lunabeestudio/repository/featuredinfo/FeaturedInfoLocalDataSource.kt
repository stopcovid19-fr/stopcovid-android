/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.repository.featuredinfo

import com.lunabeestudio.domain.model.FeaturedInfo
import kotlinx.coroutines.flow.Flow

interface FeaturedInfoLocalDataSource {
    fun flowAllFeaturedInfo(): Flow<List<FeaturedInfo>>
    suspend fun getFeaturedInfoByIndex(index: Int): FeaturedInfo
    suspend fun deleteOldAndInsertNew(newFeaturedInfo: List<FeaturedInfo>)
    fun removeOldFiles(oldFeaturedInfo: List<FeaturedInfo>)
    fun getApplicationLanguage(): String
    fun getFileThumbnailFromFeaturedInfo(featuredInfo: FeaturedInfo): String
    fun getFileNameHDFromFeaturedInfo(featuredInfo: FeaturedInfo): String

    var prevSyncLanguageFeaturedInfo: String?
}
