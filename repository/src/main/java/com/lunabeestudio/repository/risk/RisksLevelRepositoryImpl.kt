/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.repository.risk

import com.lunabeestudio.domain.model.DownloadStatus
import com.lunabeestudio.domain.model.RisksUILevelSection
import com.lunabeestudio.domain.repository.RisksLevelRepository
import com.lunabeestudio.repository.file.FileLocalDataSource
import com.lunabeestudio.repository.file.FileRemoteToLocalDataSource
import com.lunabeestudio.repository.file.JsonLocalDataSource
import com.lunabeestudio.repository.file.JsonRemoteDataSource
import com.lunabeestudio.repository.file.JsonRemoteRepositoryImpl
import java.lang.reflect.Type
import javax.inject.Inject

class RisksLevelRepositoryImpl @Inject constructor(
    private val risksLevelLocalDataSource: RisksLevelLocalDataSource,
    jsonLocalDataSource: JsonLocalDataSource,
    jsonRemoteDataSource: JsonRemoteDataSource,
    fileLocalDataSource: FileLocalDataSource,
    fileRemoteToLocalDataSource: FileRemoteToLocalDataSource,
) : JsonRemoteRepositoryImpl<List<RisksUILevelSection>>(
    jsonLocalDataSource,
    jsonRemoteDataSource,
    fileLocalDataSource,
    fileRemoteToLocalDataSource,
),
    RisksLevelRepository {

    override val type: Type = risksLevelLocalDataSource.type
    override fun getLocalFileName(): String = risksLevelLocalDataSource.getLocalFileName()
    override fun getRemoteFileUrl(): String = risksLevelLocalDataSource.getRemoteFileUrl()
    override fun getAssetFilePath(): String = risksLevelLocalDataSource.getAssetFilePath()

    private var risksUILevelSections: List<RisksUILevelSection>? = null
    override fun getRisksUILevelSections(): List<RisksUILevelSection>? {
        return risksUILevelSections
    }

    override suspend fun initialize() {
        loadLocal()?.let { localRisksLevels ->
            if (risksUILevelSections != localRisksLevels) {
                risksUILevelSections = localRisksLevels
            }
        }
    }

    override suspend fun onAppForeground() {
        if (fetchLast() == DownloadStatus.FromServer) {
            loadLocal()?.let { localRisksLevels ->
                if (this.risksUILevelSections != localRisksLevels) {
                    this.risksUILevelSections = localRisksLevels
                }
            }
        }
    }
}
