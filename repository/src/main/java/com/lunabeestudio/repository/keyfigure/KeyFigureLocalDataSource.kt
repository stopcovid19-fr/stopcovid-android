/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.repository.keyfigure

import com.lunabeestudio.domain.model.DepartmentKeyFigure
import com.lunabeestudio.domain.model.KeyFigure
import kotlinx.coroutines.flow.Flow

interface KeyFigureLocalDataSource {
    fun flowAllKeyFiguresWithFavorites(): Flow<List<KeyFigure>>
    fun flowKeyFigureByLabel(label: String): Flow<KeyFigure?>
    suspend fun keyFigureByLabel(label: String): KeyFigure?
    suspend fun allKeyFigures(): List<KeyFigure>
    suspend fun setFavorite(labelKey: String, isFavorite: Boolean, favoriteOrder: Float)
    suspend fun deleteFavorites()
    suspend fun insertNewAndDeleteOld(old: List<KeyFigure>, new: List<KeyFigure>)
    suspend fun getDepartment(label: String, dptNb: String): DepartmentKeyFigure?
    val chosenPostalCode: String?
    val nationalSuffix: String
}
