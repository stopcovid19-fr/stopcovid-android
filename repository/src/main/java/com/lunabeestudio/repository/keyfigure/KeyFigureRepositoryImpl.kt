/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/14 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.repository.keyfigure

import com.lunabeestudio.domain.model.KeyFigure
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.domain.repository.KeyFigureRepository
import com.lunabeestudio.error.AppError
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.combine
import javax.inject.Inject

class KeyFigureRepositoryImpl @Inject constructor(
    private val keyFigureLocalDatasource: KeyFigureLocalDataSource,
    private val keyFigureRemoteDatasource: KeyFigureRemoteDataSource,
) : KeyFigureRepository {

    private val newDataOfflineEventFlow = MutableSharedFlow<Unit>(replay = 1)

    override val keyFiguresWithDepartmentFlow: Flow<TacResult<List<KeyFigure>>>
        get() {
            newDataOfflineEventFlow.tryEmit(Unit) // init combine
            return combine(keyFigureLocalDatasource.flowAllKeyFiguresWithFavorites(), newDataOfflineEventFlow) { figures, _ ->
                if (figures.isEmpty()) {
                    TacResult.Failure(AppError(AppError.Code.KEY_FIGURES_NOT_AVAILABLE), null)
                } else {
                    figures.forEach { figure ->
                        val departmentKeyFigure = keyFigureLocalDatasource.chosenPostalCode?.let { postalCode ->
                            keyFigureLocalDatasource.getDepartment(
                                figure.labelKey,
                                KeyFigure.getDepartmentKeyFromPostalCode(postalCode),
                            )
                        }
                        departmentKeyFigure?.let { department ->
                            figure.valuesDepartments = listOf(department)
                        }
                    }
                    TacResult.Success(figures)
                }
            }
        }

    override fun keyFigureByLabelWithDepartmentFlow(label: String): Flow<TacResult<KeyFigure?>> =
        keyFigureLocalDatasource.flowKeyFigureByLabel(label).combine(newDataOfflineEventFlow) { figure, _ ->
            if (figure != null) {
                val departmentKeyFigure = keyFigureLocalDatasource.chosenPostalCode?.let { postalCode ->
                    keyFigureLocalDatasource.getDepartment(
                        figure.labelKey,
                        KeyFigure.getDepartmentKeyFromPostalCode(postalCode),
                    )
                }
                departmentKeyFigure?.let { figure.valuesDepartments = listOf(it) }
                TacResult.Success(figure)
            } else {
                TacResult.Failure(AppError(AppError.Code.KEY_FIGURES_NOT_AVAILABLE), null)
            }
        }

    override fun allKeyFiguresWithFavoritesFlow(): Flow<List<KeyFigure>> = keyFigureLocalDatasource.flowAllKeyFiguresWithFavorites()

    override suspend fun allKeyFigures(): TacResult<List<KeyFigure>> {
        val list = keyFigureLocalDatasource.allKeyFigures()
        return if (list.isEmpty()) {
            TacResult.Failure(AppError(AppError.Code.KEY_FIGURES_NOT_AVAILABLE), null)
        } else {
            TacResult.Success(list)
        }
    }

    override suspend fun keyFigureByLabel(label: String): TacResult<KeyFigure> {
        keyFigureLocalDatasource.keyFigureByLabel(label)?.let {
            return TacResult.Success(it)
        }
        return TacResult.Failure(AppError(AppError.Code.KEY_FIGURES_NOT_AVAILABLE), null)
    }

    override suspend fun keyFigureWithDepartmentByLabel(label: String): TacResult<KeyFigure> {
        keyFigureLocalDatasource.keyFigureByLabel(label)?.let {
            val departmentKeyFigure = keyFigureLocalDatasource.chosenPostalCode?.let { postalCode ->
                keyFigureLocalDatasource.getDepartment(
                    it.labelKey,
                    KeyFigure.getDepartmentKeyFromPostalCode(postalCode),
                )
            }
            departmentKeyFigure?.let { department ->
                it.valuesDepartments = listOf(department)
            }
            return TacResult.Success(it)
        }
        return TacResult.Failure(AppError(AppError.Code.KEY_FIGURES_NOT_AVAILABLE), null)
    }

    override suspend fun onAppForeground() {
        fetchNewData()
    }

    override suspend fun initialize() {
        fetchNewData()
    }

    // Restore the favorites, remove the old keyfigures and update data
    private suspend fun fetchNewData() {
        val (codePath, suffix) = getSuffixAndCodePath()
        val result = keyFigureRemoteDatasource.fetchKeyFigures(codePath, suffix)
        if (result is TacResult.Success && result.successData != null) {
            result.successData?.let {
                val listLabel = it.map { figure -> figure.labelKey }
                val oldData = keyFigureLocalDatasource.allKeyFigures().filter { figure ->
                    !listLabel.contains(figure.labelKey)
                }
                keyFigureLocalDatasource.insertNewAndDeleteOld(oldData, it)
            }
        } else {
            newDataOfflineEventFlow.emit(Unit)
        }
    }

    override suspend fun toggleFavorite(figure: KeyFigure) {
        saveFavorite(figure.copy(isFavorite = !figure.isFavorite, favoriteOrder = (getMaxFavoriteOrder() ?: -1f) + 1f))
    }

    override suspend fun saveFavorite(figure: KeyFigure) {
        keyFigureLocalDatasource.setFavorite(figure.labelKey, figure.isFavorite, figure.favoriteOrder)
    }

    private suspend fun getMaxFavoriteOrder(): Float? {
        return keyFigureLocalDatasource.allKeyFigures().filter { it.isFavorite }.maxByOrNull { it.favoriteOrder }?.favoriteOrder
    }

    override suspend fun restoreDefaultFavorites() {
        keyFigureLocalDatasource.deleteFavorites()
    }

    private fun getSuffixAndCodePath(): Pair<String, String> {
        val key = keyFigureLocalDatasource.chosenPostalCode?.let { KeyFigure.getDepartmentKeyFromPostalCode(it) }
        val suffix = key ?: keyFigureLocalDatasource.nationalSuffix
        val codePath = if (key != null) "/$key" else ""
        return Pair(codePath, suffix)
    }
}
