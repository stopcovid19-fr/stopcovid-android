/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/15 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.repository.file

import com.lunabeestudio.domain.model.DownloadStatus
import java.io.File
import java.lang.reflect.Type
import java.rmi.RemoteException

abstract class JsonRemoteRepositoryImpl<T>(
    private val jsonLocalDataSource: JsonLocalDataSource,
    private val jsonRemoteDataSource: JsonRemoteDataSource,
    fileLocalDataSource: FileLocalDataSource,
    fileRemoteToLocalDataSource: FileRemoteToLocalDataSource,
) : FileRemoteToLocalRepositoryImpl(
    fileLocalDataSource,
    fileRemoteToLocalDataSource,
) {

    protected abstract val type: Type
    override val mimeType: String = "application/json"

    protected suspend fun loadLocal(): T? {
        return jsonLocalDataSource.loadLocal<T>(getLocalFileName(), getAssetFilePath(), type)
    }

    final override suspend fun fetchLast(): DownloadStatus {
        return try {
            super.fetchLast()
        } catch (e: RemoteException) {
            jsonRemoteDataSource.throwMalformedJsonException()
            // useless as previous function throws
            DownloadStatus.Failure
        }
    }

    final override suspend fun fileNotCorrupted(file: File): Boolean {
        return jsonLocalDataSource.fileNotCorrupted<T>(file, type)
    }
}
