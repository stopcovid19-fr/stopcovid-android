/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/14 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.repository.file

import java.io.File
import java.lang.reflect.Type

interface JsonLocalDataSource {
    suspend fun <T> loadLocal(localFileName: String, assetFilePath: String?, type: Type): T?
    suspend fun <T> fileNotCorrupted(file: File, type: Type): Boolean
}
