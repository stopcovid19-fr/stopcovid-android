/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.common

object OkHttpConstants {
    const val OKHTTP_CACHE_FILENAME: String = "http_cache"
    const val OKHTTP_MAX_CACHE_SIZE_BYTES: Long = 30 * 1024 * 1024
}
