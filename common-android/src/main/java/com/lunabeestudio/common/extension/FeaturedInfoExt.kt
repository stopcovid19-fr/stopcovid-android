/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.common.extension

import android.webkit.MimeTypeMap
import com.lunabeestudio.common.CommonConstant
import com.lunabeestudio.domain.model.FeaturedInfo

fun FeaturedInfo.getFileThumbnailFromFeaturedInfo(): String {
    val extension = MimeTypeMap.getFileExtensionFromUrl(thumbnail)
    return "${CommonConstant.FeaturedInfo.FILE_PREFIX_THUMBNAIL}${thumbnail.hashCode()}.$extension"
}

fun FeaturedInfo.getFileNameHDFromFeaturedInfo(): String {
    val extension = MimeTypeMap.getFileExtensionFromUrl(hd)
    return "${CommonConstant.FeaturedInfo.FILE_PREFIX_HD}${hd.hashCode()}.$extension"
}
