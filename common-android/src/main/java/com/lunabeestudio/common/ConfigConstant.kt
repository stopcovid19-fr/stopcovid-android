/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.common

object ConfigConstant {
    private const val STATIC_BASE_URL: String = "https://app-static.tousanticovid.gouv.fr/"
    private const val VERSIONED_PATH: String = "json/version-45/"
    private const val VERSIONED_SERVER_URL: String = STATIC_BASE_URL + VERSIONED_PATH

    object Maintenance {
        private const val FOLDER: String = "maintenance/"
        private const val FILENAME: String = "info-maintenance-v3.json"
        const val URL: String = STATIC_BASE_URL + FOLDER + FILENAME
    }

    object KeyFigures {
        const val NATIONAL_SUFFIX: String = "nat"
        private const val FOLDER: String = "infos/"
        private const val FILENAME: String = "key-figures-map.pb"
        private const val ZIP_EXTENSION: String = ".gz"
        const val ASSET_PATH: String = "$FOLDER$FILENAME"
        private const val VERSION: String = "v2/"
        const val END_URL: String = "$VERSION$FILENAME$ZIP_EXTENSION"
        const val BASE_URL: String = STATIC_BASE_URL + FOLDER
    }

    object MoreKeyFigures {
        const val FOLDER: String = "MoreKeyFigures/"
        const val URL: String = VERSIONED_SERVER_URL + FOLDER
        const val FILE_PREFIX: String = "morekeyfigures-"
    }

    object InfoCenter {
        private const val FOLDER: String = "InfoCenter/"
        private const val PATH: String = "json/news/v1/"
        const val URL: String = STATIC_BASE_URL + PATH + FOLDER
        const val FEATURED_URL: String = VERSIONED_SERVER_URL + FOLDER
    }

    object Links {
        const val FOLDER: String = "Links/"
        const val URL: String = VERSIONED_SERVER_URL + FOLDER
        const val FILE_PREFIX: String = "links-"
    }

    object Privacy {
        const val FOLDER: String = "Privacy/"
        const val URL: String = VERSIONED_SERVER_URL + FOLDER
        const val FILE_PREFIX: String = "privacy-"
    }

    object Risks {
        const val FILENAME: String = "risks.json"
        private const val FOLDER: String = "Risks/"
        const val URL: String = VERSIONED_SERVER_URL + FOLDER + FILENAME
        const val ASSET_FILE_PATH: String = FOLDER + FILENAME
    }

    object EUTags {
        const val FILENAME: String = "tags-ue-list-codes.json"
        private const val FOLDER: String = "ref-data/"
        const val URL: String = STATIC_BASE_URL + "json/" + FOLDER + FILENAME
        const val ASSET_FILE_PATH: String = FOLDER + FILENAME
    }

    object Labels {
        const val FOLDER: String = "Strings/"
        const val URL: String = VERSIONED_SERVER_URL + FOLDER
        const val FILE_PREFIX: String = "strings-"
    }

    object Wallet {
        private const val FOLDER: String = "Wallet/"
        private const val URL: String = VERSIONED_SERVER_URL + FOLDER

        const val TEST_CERTIFICATE_THUMBNAIL_FILE: String = "test-certificate.png"
        private const val TEST_CERTIFICATE_THUMBNAIL_TEMPLATE_FILE: String = "test-certificate-%s.png"
        const val TEST_CERTIFICATE_THUMBNAIL_TEMPLATE_PATH: String = URL + TEST_CERTIFICATE_THUMBNAIL_TEMPLATE_FILE

        const val TEST_CERTIFICATE_FULL_FILE: String = "test-certificate-full.png"
        private const val TEST_CERTIFICATE_FULL_TEMPLATE_FILE: String = "test-certificate-full-%s.png"
        const val TEST_CERTIFICATE_FULL_TEMPLATE_PATH: String = URL + TEST_CERTIFICATE_FULL_TEMPLATE_FILE

        const val VACCIN_CERTIFICATE_THUMBNAIL_FILE: String = "vaccin-certificate.png"
        private const val VACCIN_CERTIFICATE_THUMBNAIL_TEMPLATE_FILE: String = "vaccin-certificate-%s.png"
        const val VACCIN_CERTIFICATE_THUMBNAIL_TEMPLATE_PATH: String = URL + VACCIN_CERTIFICATE_THUMBNAIL_TEMPLATE_FILE

        const val VACCIN_CERTIFICATE_FULL_FILE: String = "vaccin-certificate-full.png"
        private const val VACCIN_CERTIFICATE_FULL_TEMPLATE_FILE: String = "vaccin-certificate-full-%s.png"
        const val VACCIN_CERTIFICATE_FULL_TEMPLATE_PATH: String = URL + VACCIN_CERTIFICATE_FULL_TEMPLATE_FILE

        const val VACCIN_EUROPE_CERTIFICATE_FULL_FILE: String = "vaccin-europe-certificate-full.png"
        private const val VACCIN_EUROPE_CERTIFICATE_FULL_TEMPLATE_FILE: String = "vaccin-europe-certificate-full-%s.png"
        const val VACCIN_EUROPE_CERTIFICATE_FULL_TEMPLATE_PATH: String = URL + VACCIN_EUROPE_CERTIFICATE_FULL_TEMPLATE_FILE

        const val VACCIN_EUROPE_CERTIFICATE_THUMBNAIL_FILE: String = "vaccin-europe-certificate.png"
        private const val VACCIN_EUROPE_CERTIFICATE_THUMBNAIL_TEMPLATE_FILE: String = "vaccin-europe-certificate-%s.png"
        const val VACCIN_EUROPE_CERTIFICATE_THUMBNAIL_TEMPLATE_PATH: String = URL + VACCIN_EUROPE_CERTIFICATE_THUMBNAIL_TEMPLATE_FILE

        const val TEST_EUROPE_CERTIFICATE_THUMBNAIL_FILE: String = "test-europe-certificate.png"
        private const val TEST_EUROPE_CERTIFICATE_THUMBNAIL_TEMPLATE_FILE: String = "test-europe-certificate-%s.png"
        const val TEST_EUROPE_CERTIFICATE_THUMBNAIL_TEMPLATE_PATH: String = URL + TEST_EUROPE_CERTIFICATE_THUMBNAIL_TEMPLATE_FILE

        const val TEST_EUROPE_CERTIFICATE_FULL_FILE: String = "test-europe-certificate-full.png"
        private const val TEST_EUROPE_CERTIFICATE_FULL_TEMPLATE_FILE: String = "test-europe-certificate-full-%s.png"
        const val TEST_EUROPE_CERTIFICATE_FULL_TEMPLATE_PATH: String = URL + TEST_EUROPE_CERTIFICATE_FULL_TEMPLATE_FILE

        const val RECOVERY_EUROPE_CERTIFICATE_THUMBNAIL_FILE: String = "recovery-europe-certificate.png"
        private const val RECOVERY_EUROPE_CERTIFICATE_THUMBNAIL_TEMPLATE_FILE: String = "recovery-europe-certificate-%s.png"
        const val RECOVERY_EUROPE_CERTIFICATE_THUMBNAIL_TEMPLATE_PATH: String = URL + RECOVERY_EUROPE_CERTIFICATE_THUMBNAIL_TEMPLATE_FILE

        const val RECOVERY_EUROPE_CERTIFICATE_FULL_FILE: String = "recovery-europe-certificate-full.png"
        private const val RECOVERY_EUROPE_CERTIFICATE_FULL_TEMPLATE_FILE: String = "recovery-europe-certificate-full-%s.png"
        const val RECOVERY_EUROPE_CERTIFICATE_FULL_TEMPLATE_PATH: String = URL + RECOVERY_EUROPE_CERTIFICATE_FULL_TEMPLATE_FILE
    }

    object DccCertificates {
        const val URL: String = STATIC_BASE_URL + "json/dsc/"
        const val FOLDER: String = "dsc/"
    }

    object Config {
        const val FOLDER: String = "Config/"
        const val URL: String = VERSIONED_SERVER_URL + FOLDER
        const val LOCAL_FILENAME: String = "config.json"
        const val LOCAL_TMP_FILENAME: String = "config_tmp.json"
    }

    object Blacklist {
        private const val FOLDER: String = "json/blacklist/v5/CertList/"
        const val BASE_URL: String = STATIC_BASE_URL + FOLDER
    }

    object Store {
        const val GOOGLE: String = "market://details?id=fr.gouv.android.stopcovid"
        const val HUAWEI: String = "appmarket://details?id=fr.gouv.android.stopcovid"
        const val TAC_WEBSITE: String = "https://bonjour.tousanticovid.gouv.fr"
        const val STOPCOVID_WEBSITE: String = "https://bonjour.stopcovid.gouv.fr"
    }

    object SmartWallet {
        const val EXPIRATION_FILENAME: String = "validity.json"
        const val ELIGIBILITY_FILENAME: String = "eligibility.json"
        const val FOLDER: String = "smartwallet/"
        private const val VERSION: String = "v2/"
        const val URL: String = STATIC_BASE_URL + "json/" + FOLDER + VERSION
    }

    object Logs {
        const val DIR_NAME: String = "logs"
    }
}
