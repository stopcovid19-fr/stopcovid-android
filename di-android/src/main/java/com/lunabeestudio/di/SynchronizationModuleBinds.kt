/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.di

import com.lunabeestudio.domain.syncmanager.BlacklistSyncManager
import com.lunabeestudio.domain.syncmanager.InfoCenterSyncManager
import com.lunabeestudio.synchronization.blacklist.BlacklistSyncManagerImpl
import com.lunabeestudio.synchronization.infocenter.InfoCenterSyncManagerImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class SynchronizationModuleBinds {

    @Binds
    abstract fun bindBlacklistSyncManager(
        blacklistSyncManagerImpl: BlacklistSyncManagerImpl,
    ): BlacklistSyncManager

    @Binds
    abstract fun bindInfoCenterSyncManager(
        infoCenterSyncManagerImpl: InfoCenterSyncManagerImpl,
    ): InfoCenterSyncManager
}
