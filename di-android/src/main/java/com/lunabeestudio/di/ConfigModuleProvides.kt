/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.di

import android.content.Context
import com.lunabeestudio.common.ConfigConstant
import com.lunabeestudio.domain.di.BlacklistBaseUrl
import com.lunabeestudio.domain.di.DccBaseUrl
import com.lunabeestudio.domain.di.FeaturedInfoBaseUrl
import com.lunabeestudio.domain.di.InfoCenterBaseUrl
import com.lunabeestudio.domain.di.KeyFigureBaseUrl
import com.lunabeestudio.domain.di.LogsDir
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import java.io.File

@Module
@InstallIn(SingletonComponent::class)
object ConfigModuleProvides {

    @LogsDir
    @Provides
    fun provideLogsDir(@ApplicationContext context: Context): File = File(
        context.filesDir,
        ConfigConstant.Logs.DIR_NAME,
    )

    @BlacklistBaseUrl
    @Provides
    fun provideBlacklistBaseUrl(): String = ConfigConstant.Blacklist.BASE_URL

    @InfoCenterBaseUrl
    @Provides
    fun provideInfoCenterBaseUrl(): String = ConfigConstant.InfoCenter.URL

    @DccBaseUrl
    @Provides
    fun provideDccBaseUrl(): String = ConfigConstant.DccCertificates.URL

    @FeaturedInfoBaseUrl
    @Provides
    fun provideFeaturedInfoBaseUrl(): String = ConfigConstant.InfoCenter.FEATURED_URL

    @KeyFigureBaseUrl
    @Provides
    fun provideKeyFigureBaseUrl(): String = ConfigConstant.KeyFigures.BASE_URL
}
