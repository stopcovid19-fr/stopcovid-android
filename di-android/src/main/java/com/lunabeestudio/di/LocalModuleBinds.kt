/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.di

import com.lunabeestudio.local.blacklist.BlacklistLocalDataSourceImpl
import com.lunabeestudio.local.certificate.CertificateDocumentLocalDataSourceImpl
import com.lunabeestudio.local.certificate.CertificateLocalDataSourceImpl
import com.lunabeestudio.local.certificate.DccCertificateLocalDataSourceImpl
import com.lunabeestudio.local.certificate.ImageDocumentLocalDataSourceImpl
import com.lunabeestudio.local.crypto.JCECryptoDataSourceImpl
import com.lunabeestudio.local.eutags.EUTagsLocalDataSourceImpl
import com.lunabeestudio.local.featuredinfo.FeaturedInfoLocalDataSourceImpl
import com.lunabeestudio.local.file.FileLocalDataSourceImpl
import com.lunabeestudio.local.file.JsonLocalDataSourceImpl
import com.lunabeestudio.local.info.InfoCenterLocalDataSourceImpl
import com.lunabeestudio.local.keyfigure.KeyFigureLocalDataSourceImpl
import com.lunabeestudio.local.keyfigure.KeyFigureMapLocalDataSourceImpl
import com.lunabeestudio.local.risk.RisksLevelLocalDataSourceImpl
import com.lunabeestudio.local.smartwallet.SmartWalletEligibilityLocalDataSourceImpl
import com.lunabeestudio.local.smartwallet.SmartWalletValidityLocalDataSourceImpl
import com.lunabeestudio.repository.blacklist.BlacklistLocalDataSource
import com.lunabeestudio.repository.certificate.CertificateDocumentLocalDataSource
import com.lunabeestudio.repository.certificate.CertificateLocalDataSource
import com.lunabeestudio.repository.certificate.DccCertificateLocalDataSource
import com.lunabeestudio.repository.certificate.ImageDocumentLocalDataSource
import com.lunabeestudio.repository.crypto.SharedCryptoDataSource
import com.lunabeestudio.repository.eutags.EUTagsLocalDataSource
import com.lunabeestudio.repository.featuredinfo.FeaturedInfoLocalDataSource
import com.lunabeestudio.repository.file.FileLocalDataSource
import com.lunabeestudio.repository.file.JsonLocalDataSource
import com.lunabeestudio.repository.infocenter.InfoCenterLocalDataSource
import com.lunabeestudio.repository.keyfigure.KeyFigureLocalDataSource
import com.lunabeestudio.repository.keyfigure.KeyFigureMapLocalDataSource
import com.lunabeestudio.repository.risk.RisksLevelLocalDataSource
import com.lunabeestudio.repository.smartwallet.SmartWalletEligibilityLocalDataSource
import com.lunabeestudio.repository.smartwallet.SmartWalletValidityLocalDataSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class LocalModuleBinds {

    @Binds
    abstract fun bindLocalBlacklistDataSource(
        localBlacklistDataSourceImpl: BlacklistLocalDataSourceImpl,
    ): BlacklistLocalDataSource

    @Binds
    abstract fun bindLocalKeyFigureDataSource(
        localKeyFigureDatasourceImpl: KeyFigureLocalDataSourceImpl,
    ): KeyFigureLocalDataSource

    @Binds
    abstract fun bindLocalKeyFigureMapDataSource(
        localKeyFigureMapDatasourceImpl: KeyFigureMapLocalDataSourceImpl,
    ): KeyFigureMapLocalDataSource

    @Binds
    abstract fun bindLocalInfoCenterDataSource(
        localInfoCenterDataSourceImpl: InfoCenterLocalDataSourceImpl,
    ): InfoCenterLocalDataSource

    @Binds
    abstract fun bindLocalFeaturedInfoDataSource(
        localFeaturedInfoDataSourceImpl: FeaturedInfoLocalDataSourceImpl,
    ): FeaturedInfoLocalDataSource

    @Binds
    abstract fun bindSharedCryptoDataSource(
        jceCryptoDataSourceImpl: JCECryptoDataSourceImpl,
    ): SharedCryptoDataSource

    @Binds
    abstract fun bindLocalCertificateDataSource(
        localCertificateDataSourceImpl: CertificateLocalDataSourceImpl,
    ): CertificateLocalDataSource

    @Binds
    abstract fun bindLocalFileDataSource(
        localFileDataSourceImpl: FileLocalDataSourceImpl,
    ): FileLocalDataSource

    @Binds
    abstract fun bindJsonLocalDataSource(
        jsonLocalDataSourceImpl: JsonLocalDataSourceImpl,
    ): JsonLocalDataSource

    @Binds
    abstract fun bindImageDocumentLocalDataSource(
        imageDocumentLocalDataSourceImpl: ImageDocumentLocalDataSourceImpl,
    ): ImageDocumentLocalDataSource

    @Binds
    abstract fun bindCertificateDocumentLocalDataSource(
        certificateDocumentLocalDataSourceImpl: CertificateDocumentLocalDataSourceImpl,
    ): CertificateDocumentLocalDataSource

    @Binds
    abstract fun bindDccCertificateLocalDataSource(
        dccCertificateLocalDataSourceImpl: DccCertificateLocalDataSourceImpl,
    ): DccCertificateLocalDataSource

    @Binds
    abstract fun bindRisksLevelLocalDataSource(
        risksLevelLocalDataSourceImpl: RisksLevelLocalDataSourceImpl,
    ): RisksLevelLocalDataSource

    @Binds
    abstract fun bindEUTagsLocalDataSource(
        euTagsLocalDataSourceImpl: EUTagsLocalDataSourceImpl,
    ): EUTagsLocalDataSource

    @Binds
    abstract fun bindSmartWalletEligibilityLocalDataSource(
        smartWalletEligibilityLocalDataSourceImpl: SmartWalletEligibilityLocalDataSourceImpl,
    ): SmartWalletEligibilityLocalDataSource

    @Binds
    abstract fun bindSmartWalletValidityLocalDataSource(
        smartWalletValidityLocalDataSourceImpl: SmartWalletValidityLocalDataSourceImpl,
    ): SmartWalletValidityLocalDataSource
}
