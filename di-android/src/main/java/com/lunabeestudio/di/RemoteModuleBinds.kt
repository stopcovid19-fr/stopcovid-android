/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.di

import com.lunabeestudio.remote.blacklist.BlacklistRemoteDataSourceImpl
import com.lunabeestudio.remote.certificate.CertificateRemoteDataSourceImpl
import com.lunabeestudio.remote.file.JsonRemoteDataSourceImpl
import com.lunabeestudio.remote.info.FeaturedInfoRemoteDataSourceImpl
import com.lunabeestudio.remote.keyfigures.KeyFigureMapRemoteDataSourceImpl
import com.lunabeestudio.remote.keyfigures.KeyFigureRemoteDataSourceImpl
import com.lunabeestudio.repository.blacklist.BlacklistRemoteDataSource
import com.lunabeestudio.repository.certificate.CertificateRemoteDataSource
import com.lunabeestudio.repository.featuredinfo.FeaturedInfoRemoteDataSource
import com.lunabeestudio.repository.file.JsonRemoteDataSource
import com.lunabeestudio.repository.keyfigure.KeyFigureMapRemoteDataSource
import com.lunabeestudio.repository.keyfigure.KeyFigureRemoteDataSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RemoteModuleBinds {

    @Binds
    abstract fun bindRemoteBlacklistDataSource(
        remoteBlacklistDataSourceImpl: BlacklistRemoteDataSourceImpl,
    ): BlacklistRemoteDataSource

    @Binds
    abstract fun bindRemoteDccDataSource(
        remoteDccDataSourceImpl: CertificateRemoteDataSourceImpl,
    ): CertificateRemoteDataSource

    @Binds
    abstract fun bindRemoteFeaturedInfoDataSource(
        remoteFeaturedInfoDataSourceImpl: FeaturedInfoRemoteDataSourceImpl,
    ): FeaturedInfoRemoteDataSource

    @Binds
    abstract fun bindRemoteKeyFigureDataSource(
        remoteKeyFigureDataSourceImpl: KeyFigureRemoteDataSourceImpl,
    ): KeyFigureRemoteDataSource

    @Binds
    abstract fun bindRemoteKeyFigureMapDataSource(
        remoteKeyFigureMapDataSourceImpl: KeyFigureMapRemoteDataSourceImpl,
    ): KeyFigureMapRemoteDataSource

    @Binds
    abstract fun bindJsonRemoteDataSource(
        jsonRemoteDataSourceImpl: JsonRemoteDataSourceImpl,
    ): JsonRemoteDataSource
}
