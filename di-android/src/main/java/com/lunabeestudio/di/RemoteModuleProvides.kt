/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.di

import com.lunabeestudio.remote.server.FileRemoteToLocalDataSourceImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient

@Module
@InstallIn(SingletonComponent::class)
object RemoteModuleProvides {

    @Provides
    fun provideOKHttpClient(
        remoteToLocalFileDataSourceImpl: FileRemoteToLocalDataSourceImpl,
    ): OkHttpClient {
        return remoteToLocalFileDataSourceImpl.okHttpClient
    }
}
