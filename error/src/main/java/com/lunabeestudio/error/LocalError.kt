/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/20 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.error

data class LocalError(
    val code: Code,
    override val message: String = code.message,
    override val cause: Throwable? = null,
) : TACError(message, cause) {

    enum class Code(val message: String) {
        SECRET_KEY_ALREADY_GENERATED("Secret key was already generated but can't be found in the KeyStore"),
    }
}
