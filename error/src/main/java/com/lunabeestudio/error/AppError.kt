/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/20 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.error

data class AppError(
    val code: Code,
    override val message: String = code.message,
    override val cause: Throwable? = null,
) : TACError(message, cause) {

    enum class Code(val message: String) {
        UNKNOWN("Unknown error occurred"),
        WALLET_CERTIFICATE_MALFORMED("Invalid certificate code"),
        WALLET_CERTIFICATE_INVALID_SIGNATURE("Invalid certificate signature"),
        WALLET_CERTIFICATE_UNKNOWN_ERROR("No key to verify the certificate authenticity"),
        KEY_FIGURES_NOT_AVAILABLE("Failed to load key figures"),
    }
}
