/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.remote

import com.lunabeestudio.remote.proto.ProtoGzipConverterFactory
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.protobuf.ProtoConverterFactory

object RetrofitClient {

    fun <T> getJsonService(
        baseUrl: String,
        clazz: Class<T>,
        okhttpClient: OkHttpClient,
    ): T {
        return Retrofit.Builder()
            .baseUrl(baseUrl.toHttpUrl())
            .addConverterFactory(MoshiConverterFactory.create())
            .client(okhttpClient)
            .build().create(clazz)
    }

    fun <T> getProtoService(
        baseUrl: String,
        clazz: Class<T>,
        okhttpClient: OkHttpClient,
    ): T {
        return Retrofit.Builder()
            .baseUrl(baseUrl.toHttpUrl())
            .addConverterFactory(ProtoConverterFactory.create())
            .client(okhttpClient)
            .build().create(clazz)
    }

    fun <T> getProtoGzipService(
        baseUrl: String,
        clazz: Class<T>,
        okhttpClient: OkHttpClient,
    ): T {
        return Retrofit.Builder()
            .baseUrl(baseUrl.toHttpUrl())
            .addConverterFactory(ProtoGzipConverterFactory.create())
            .client(okhttpClient)
            .build().create(clazz)
    }
}
