/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/3/2 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.remote.blacklist

import com.lunabeestudio.domain.di.BlacklistBaseUrl
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.remote.RetrofitClient
import com.lunabeestudio.repository.blacklist.BlacklistRemoteDataSource
import okhttp3.OkHttpClient
import javax.inject.Inject

class BlacklistRemoteDataSourceImpl @Inject constructor(
    @BlacklistBaseUrl baseUrl: String,
    okHttpClient: OkHttpClient,
) : BlacklistRemoteDataSource {
    private val api: BlacklistApi = RetrofitClient.getProtoService(baseUrl, BlacklistApi::class.java, okHttpClient)

    override suspend fun getPrefixHashes(prefix: String): TacResult<List<String>> {
        return try {
            val response = api.getPrefix(prefix)
            when {
                response.isSuccessful -> TacResult.Success(response.body()?.hList.orEmpty())
                else -> TacResult.Failure()
            }
        } catch (e: Exception) {
            TacResult.Failure(e)
        }
    }
}
