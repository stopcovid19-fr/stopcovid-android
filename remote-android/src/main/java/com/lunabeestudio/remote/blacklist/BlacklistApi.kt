/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/2/3 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.remote.blacklist

import blackListedHashPrefix.Blacklistedhasprefix
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface BlacklistApi {

    @Headers(
        "Accept: application/x-protobuf",
    )
    @GET("prefix/index.pb")
    suspend fun getIndex(): Response<Blacklistedhasprefix.indexMessage>

    @Headers(
        "Accept: application/x-protobuf",
    )
    @GET("prefix/{prefix}.pb")
    suspend fun getPrefix(
        @Path("prefix") prefix: String,
    ): Response<Blacklistedhasprefix.prefixMessage>
}
