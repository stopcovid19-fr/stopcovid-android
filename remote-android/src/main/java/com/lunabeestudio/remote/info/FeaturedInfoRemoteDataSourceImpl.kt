/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/08 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.remote.info

import android.webkit.MimeTypeMap
import com.lunabeestudio.domain.di.FeaturedInfoBaseUrl
import com.lunabeestudio.domain.model.DownloadStatus
import com.lunabeestudio.domain.model.FeaturedInfo
import com.lunabeestudio.domain.model.TacResult
import com.lunabeestudio.remote.RetrofitClient
import com.lunabeestudio.remote.extension.fromCacheOrEtags
import com.lunabeestudio.remote.extension.toFeaturedInfo
import com.lunabeestudio.repository.featuredinfo.FeaturedInfoRemoteDataSource
import com.lunabeestudio.repository.file.FileLocalDataSource
import com.lunabeestudio.repository.file.FileRemoteToLocalDataSource
import com.lunabeestudio.repository.file.FileRemoteToLocalRepositoryImpl
import okhttp3.OkHttpClient
import timber.log.Timber
import java.io.File
import javax.inject.Inject

class FeaturedInfoRemoteDataSourceImpl @Inject constructor(
    @FeaturedInfoBaseUrl baseUrl: String,
    okHttpClient: OkHttpClient,
    private val fileLocalDataSource: FileLocalDataSource,
    private val fileRemoteToLocalDataSource: FileRemoteToLocalDataSource,
) : FeaturedInfoRemoteDataSource {
    private val api: FeaturedInfoApi = RetrofitClient.getJsonService(
        baseUrl,
        FeaturedInfoApi::class.java,
        okHttpClient,
    )

    override suspend fun fetchFeaturedInfo(localSuffix: String, forceNewData: Boolean): TacResult<List<FeaturedInfo>?> {
        return try {
            val response = api.getFeaturedInfo(localSuffix)
            when {
                response.fromCacheOrEtags && !forceNewData -> TacResult.Success(
                    null,
                )
                response.isSuccessful -> TacResult.Success(
                    response.body()?.mapIndexed { index, item ->
                        item.toFeaturedInfo(index)
                    },
                )
                else -> TacResult.Failure()
            }
        } catch (e: Exception) {
            TacResult.Failure()
        }
    }

    override suspend fun saveResourceInLocal(url: String, fileName: String) {
        val extension = MimeTypeMap.getFileExtensionFromUrl(url)
        val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)

        val remoteFileRepository = object : FileRemoteToLocalRepositoryImpl(
            fileLocalDataSource,
            fileRemoteToLocalDataSource,
        ) {
            override fun getAssetFilePath(): String? = null
            override fun getLocalFileName(): String = fileName
            override val mimeType: String = mimeType.orEmpty()
            override fun getRemoteFileUrl(): String = url
            override suspend fun fileNotCorrupted(file: File): Boolean = true
            override val ignoreCache: Boolean = true

            suspend fun fetchDocument(): DownloadStatus {
                return super.fetchLast()
            }
        }
        try {
            remoteFileRepository.fetchDocument()
        } catch (e: Exception) {
            Timber.e("Error during save $url")
        }
    }
}
