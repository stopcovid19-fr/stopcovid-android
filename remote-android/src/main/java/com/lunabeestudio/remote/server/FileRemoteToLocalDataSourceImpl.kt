/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2021/27/8 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.remote.server

import android.content.Context
import android.os.Build
import androidx.annotation.RawRes
import androidx.core.util.AtomicFile
import com.lunabeestudio.common.OkHttpConstants
import com.lunabeestudio.remote.BuildConfig
import com.lunabeestudio.remote.R
import com.lunabeestudio.remote.extension.fromCacheOrEtags
import com.lunabeestudio.repository.file.FileRemoteToLocalDataSource
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.ConnectionPool
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.TlsVersion
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.tls.HandshakeCertificates
import retrofit2.HttpException
import retrofit2.Response
import timber.log.Timber
import java.io.File
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class FileRemoteToLocalDataSourceImpl @Inject constructor(
    @ApplicationContext context: Context,
) : FileRemoteToLocalDataSource {
    val okHttpClient: OkHttpClient = getDefaultOKHttpClient(context)

    private fun getDefaultOKHttpClient(context: Context): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .addInterceptor(getLogInterceptor())
            .callTimeout(30L, TimeUnit.SECONDS)
            .connectTimeout(30L, TimeUnit.SECONDS)
            .readTimeout(30L, TimeUnit.SECONDS)
            .writeTimeout(30L, TimeUnit.SECONDS)
            .connectionPool(getDefaultConnectionPool())
            .cache(
                Cache(
                    File(context.cacheDir, OkHttpConstants.OKHTTP_CACHE_FILENAME),
                    OkHttpConstants.OKHTTP_MAX_CACHE_SIZE_BYTES,
                ),
            )

        val requireTls12 = ConnectionSpec.Builder(ConnectionSpec.RESTRICTED_TLS)
            .tlsVersions(TlsVersion.TLS_1_2)
            .build()
        builder.connectionSpecs(listOf(requireTls12))

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.N) {
            val certificates: HandshakeCertificates = HandshakeCertificates.Builder()
                .addTrustedCertificate(certificateFromRes(context, R.raw.certigna_services))
                .build()
            builder.sslSocketFactory(certificates.sslSocketFactory(), certificates.trustManager)
        }

        return builder.build()
    }

    private fun certificateFromRes(context: Context, @RawRes rawRes: Int): X509Certificate {
        return CertificateFactory.getInstance("X.509").generateCertificate(
            context.resources.openRawResource(rawRes),
        ) as X509Certificate
    }

    private fun getLogInterceptor(): HttpLoggingInterceptor = HttpLoggingInterceptor { message -> Timber.v(message) }.apply {
        level = when {
            BuildConfig.DEBUG -> HttpLoggingInterceptor.Level.BODY
            else -> HttpLoggingInterceptor.Level.NONE
        }
    }

    @Suppress("BlockingMethodInNonBlockingContext")
    override suspend fun saveTo(url: String, filePath: String, acceptMimeType: String, ignoreCache: Boolean): Boolean {
        return withContext(Dispatchers.IO) {
            val file = File(filePath)
            val request: Request = Request.Builder().apply {
                cacheControl(CacheControl.Builder().maxAge(10, TimeUnit.MINUTES).build())
                addHeader("Accept", acceptMimeType)
                url(url)
            }.build()

            val response = okHttpClient.newCall(request).execute()
            val body = response.body

            if (!ignoreCache && response.fromCacheOrEtags) {
                false
            } else if (response.isSuccessful && body != null) {
                body.byteStream().use { input ->
                    file.outputStream().use { output ->
                        input.copyTo(output, 4 * 1024)
                    }
                }
                true
            } else {
                throw HttpException(Response.error<Any>(body!!, response))
            }
        }
    }

    @Suppress("BlockingMethodInNonBlockingContext")
    override suspend fun saveTo(url: String, atomicFilePath: String, validData: suspend (data: ByteArray) -> Boolean): Boolean {
        return withContext(Dispatchers.IO) {
            val atomicFile = AtomicFile(File(atomicFilePath))
            val request: Request = Request.Builder().apply {
                cacheControl(CacheControl.Builder().maxAge(10, TimeUnit.MINUTES).build())
                url(url)
            }.build()

            val response = okHttpClient.newCall(request).execute()
            val body = response.body
            if (response.fromCacheOrEtags) {
                false
            } else if (response.isSuccessful && body != null) {
                body.use {
                    val bodyBytes = body.bytes()
                    val isValid = validData(bodyBytes)
                    if (isValid) {
                        val fileOutputStream = atomicFile.startWrite()
                        try {
                            bodyBytes.inputStream().use { input ->
                                input.copyTo(fileOutputStream, 4 * 1024)
                            }
                            atomicFile.finishWrite(fileOutputStream)
                        } catch (e: Exception) {
                            atomicFile.failWrite(fileOutputStream)
                            throw e
                        }
                    }
                }
                true
            } else {
                throw HttpException(Response.error<Any>(body!!, response))
            }
        }
    }

    companion object {
        private fun getDefaultConnectionPool(): ConnectionPool = ConnectionPool(5, 30, TimeUnit.SECONDS)
    }
}
