/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2023/03/07 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.remote.proto;

import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.Parser;

import java.io.IOException;
import java.util.zip.GZIPInputStream;

import androidx.annotation.Nullable;
import okhttp3.ResponseBody;
import retrofit2.Converter;

public final class ProtoGzipResponseConverterFactory<T extends MessageLite> implements Converter<ResponseBody, T> {
    private final Parser<T> parser;
    private final @Nullable
    ExtensionRegistryLite registry;

    ProtoGzipResponseConverterFactory(Parser<T> parser, @Nullable ExtensionRegistryLite registry) {
        this.parser = parser;
        this.registry = registry;
    }

    @Override
    public T convert(ResponseBody value) throws IOException {
        try {
            return registry == null
                    ? parser.parseFrom(new GZIPInputStream(value.byteStream()))
                    : parser.parseFrom(new GZIPInputStream(value.byteStream()), registry);
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException(e); // Despite extending IOException, this is data mismatch.
        } finally {
            value.close();
        }
    }
}
