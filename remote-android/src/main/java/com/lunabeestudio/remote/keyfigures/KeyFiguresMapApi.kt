/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Authors
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Created by Lunabee Studio / Date - 2022/12/21 - for the TOUS-ANTI-COVID project
 */

package com.lunabeestudio.remote.keyfigures

import com.lunabeestudio.common.ConfigConstant
import keynumbers.Keynumbers
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers

internal interface KeyFiguresMapApi {
    @Headers(
        "Accept: application/x-protobuf",
    )
    @GET(ConfigConstant.KeyFigures.END_URL)
    suspend fun getKeyFigureMap(): Response<Keynumbers.KeyNumbersMessage>
}
